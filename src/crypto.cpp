#include "crypto.hpp"
#include "intrinsics.h"
#include <algorithm>
#include <vector>
#include <cstring>

namespace crypto
{
    std::mt19937_64 seeded_rng()
    {
        using rres_t = std::random_device::result_type;

        static constexpr size_t N = std::mt19937_64::state_size *
                                    sizeof(std::mt19937_64::result_type);

        std::random_device source;
        std::array<rres_t, (N - 1) / sizeof(rres_t) + 1> random_data;

        std::ranges::generate(random_data, std::ref(source));
        std::seed_seq seeds(random_data.begin(), random_data.end());

        return std::mt19937_64{seeds};
    }

    static constexpr const char *alphabeth()
    {
        if constexpr (BITS_PER_CHAR == 4)
            return "0123456789ABCDEF";
        if constexpr (BITS_PER_CHAR == 5)
            return "abcdefghijklmnopqrstuvwxyz01234";
        if constexpr (BITS_PER_CHAR == 6)
            return "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    }

    size_t u64_to_str(uint64_t x, char *str)
    {
        size_t len = ((63 + BITS_PER_CHAR) - _lzcnt_u64(x)) / BITS_PER_CHAR;

        if constexpr (BITS_PER_CHAR < 7)
        {
            static constexpr const char *char_map = alphabeth();

            for (uint64_t i = 0, t = x; i < len; ++i, x = t, t /= BITS_PER_CHAR_S)
                str[i] = char_map[t % BITS_PER_CHAR_S];

            str[len - 1] = char_map[x - 1];
        }
        else
        {
            for (uint64_t i = 0, t = x; i < len; ++i, t /= BITS_PER_CHAR_S)
                str[i] = t % BITS_PER_CHAR_S;
        }
        str[len] = '\0';

        return len;
    }

    void crack_hash(HashFunction hash, size_t digest_sz, void *res, const void *digest,
                    uint64_t limit, uint64_t off, uint64_t watch)
    {
        using clk = std::chrono::high_resolution_clock;
        volatile bool found = false;

#pragma omp parallel shared(found)
        {
            int th_n = omp_get_num_threads();
            int th_i = omp_get_thread_num();
            char guess[16]{};
            std::vector<uint8_t> my_digest(digest_sz);

            auto start = clk::now();
            for (uint64_t i = th_i + off * th_n; !found && i < limit; i += th_n)
            {
                size_t len = u64_to_str(i, guess);

                hash(my_digest.data(), guess, len);
                if (!memcmp(my_digest.data(), digest, digest_sz))
                {
                    memcpy(res, guess, sizeof(guess));
                    found = true;
                }
                if (watch && !th_i && !(i % watch))
                {
                    std::cout << guess << ' ' << len << ' '
                              << (clk::now() - start).count() / 1'000'000'000. << " sec. \r";
                    std::cout.flush();
                }
            }
        }
        if (watch)
            std::cout << '\n';
    }
} // namespace crypto
