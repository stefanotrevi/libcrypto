#include "cu_crypto.cuh"
#include "tc01/cu_tc01.hpp"

namespace cu::crypto::tc01::device
{
    __device__ static uint64_t sbox(uint64_t m)
    {
        static constexpr uint64_t sbox = 0xDC8970EB3FA16542;

        uint2 mv{(uint32_t)(m >> 32), (uint32_t)m};
        uint2 p{};

#pragma unroll(8)
        for (uint32_t i = 0; i < 8; ++i)
        {
            p.x |= ((uint32_t)(sbox >> (mv.x >> i * 4 & 0xF) * 4) & 0xF) << i * 4;
            p.y |= ((uint32_t)(sbox >> (mv.y >> i * 4 & 0xF) * 4) & 0xF) << i * 4;
        }

        return (uint64_t)p.x << 32 | p.y;
    }

    __device__ static uint64_t ell(uint64_t x) { return x ^ rotl(x, 15) ^ rotl(x, 32); }

    __device__ static uint64_t round(uint64_t m, uint64_t k) { return ell(sbox(m ^ k)); }

    __device__ uint64_t enc(uint64_t m, uint64_t k, int rounds)
    {
#pragma unroll(ROUNDS)
        for (uint32_t i = 0; i < ROUNDS; ++i)
        {
            m = round(m, k);
            k = ell(k) ^ 3;
        }

        return m;
    }
} // namespace cu::crypto::tc01::device

namespace cu::crypto::tc01
{
    CU_DEFINE_TESTENC(test_enc, device::enc)
    CU_DEFINE_CRACKENC(crack, device::enc)
} // namespace cu::crypto::tc01
