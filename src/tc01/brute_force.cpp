#include <iomanip>
#include <iostream>

#include "crypto.hpp"
#include "tc01/cu_tc01.hpp"
#include "tc01/tc01.hpp"

int main()
{
    static constexpr uint64_t off = 5468567109632;

    uint64_t m = 0xF427F3E883941EC3;
    uint64_t c = 0xF1D5414F4CA7FFE8;
    uint64_t k = cu::crypto::tc01::crack(m, c, off, 1 << 8);

    std::cout << std::hex << std::uppercase;
    if (k)
        std::cout << std::setw(16) << std::setfill('0') << k << '\n';
    else
        std::cout << "Not found\n";

    return 0;
}
