#include "tc01/tc01.hpp"
#include "avxlib.hpp"

namespace crypto::tc01
{

    static uint64_t sbox(uint64_t m)
    {
        __m128i sbox = _mm_setr_epi8(0x2, 0x4, 0x5, 0x6, 0x1, 0xA, 0xF, 0x3, //
                                     0xB, 0xE, 0x0, 0x7, 0x9, 0x8, 0xC, 0xD);

        return avx::sub_nib(m, sbox);
    }

    static uint64_t ell(uint64_t x) { return x ^ _rotl64(x, 15) ^ _rotl64(x, 32); }

    static uint64_t round(uint64_t m, uint64_t k) { return ell(sbox(m ^ k)); }

    static uint64_t next_key(uint64_t k) { return ell(k) ^ 3; }

    uint64_t enc(uint64_t m, uint64_t k, int rounds)
    {
        for (int i = 0; i < rounds; ++i)
        {
            m = round(m, k);
            k = next_key(k);
        }

        return m;
    }
} // namespace tc01
