#include "tc02/tc02.hpp"
#include "avxlib.hpp"

namespace crypto::tc02
{
    static constexpr inline uint16_t rotl16(uint16_t x, uint16_t s)
    {
        return (uint16_t)(x << s | x >> (16 - s));
    }

    static constexpr inline uint16_t rotr16(uint16_t x, uint16_t s)
    {
        return (uint16_t)(x >> s | x << (16 - s));
    }

    static uint64_t sbox(uint64_t m)
    {
        __m128i sbox = _mm_setr_epi8(0x2, 0x4, 0x5, 0x6, 0x1, 0xA, 0xF, 0x3, //
                                     0xB, 0xE, 0x0, 0x7, 0x9, 0x8, 0xC, 0xD);

        return avx::sub_nib(m, sbox);
    }

    static uint64_t sbox_inv(uint64_t m)
    {
        __m128i sbox = _mm_setr_epi8(0xA, 0x4, 0x0, 0x7, 0x1, 0x2, 0x3, 0xB, //
                                     0xD, 0xC, 0x5, 0x8, 0xE, 0xF, 0x9, 0x6);

        return avx::sub_nib(m, sbox);
    }

    static uint64_t rows(uint64_t m)
    {
        uint64_t p = m & 0xFFFF000000000000;

        p |= (uint64_t)rotl16(m >> 32, 4) << 32;
        p |= (uint64_t)rotl16(m >> 16, 8) << 16;
        p |= (uint64_t)rotl16(m >> 0, 12) << 0;

        return p;
    }

    static uint64_t rows_inv(uint64_t m)
    {
        uint64_t p = m & 0xFFFF000000000000;

        p |= (uint64_t)rotr16(m >> 32, 4) << 32;
        p |= (uint64_t)rotr16(m >> 16, 8) << 16;
        p |= (uint64_t)rotr16(m >> 0, 12) << 0;

        return p;
    }

    static uint64_t columns(uint64_t m)
    {
        uint16_t a = m >> 48, b = m >> 32, c = m >> 16, d = m >> 0;

        m = c;
        m |= (uint64_t)(a ^ d) << 16;
        m |= (uint64_t)(b ^ c) << 32;
        m |= (uint64_t)(a ^ c) << 48;

        return m;
    }

    static uint64_t columns_inv(uint64_t m)
    {
        uint16_t c = m, a = m >> 48 ^ c, b = m >> 32 ^ c, d = m >> 16 ^ a;

        m = d;
        m |= (uint64_t)c << 16;
        m |= (uint64_t)b << 32;
        m |= (uint64_t)a << 48;

        return m;
    }

    static uint64_t round(uint64_t m, uint64_t k)
    {
        m ^= k & 0xFFFFFFFF00000000;
        m = sbox(m);
        m = rows(m);
        m = columns(m);

        return m;
    }

    static uint64_t round_inv(uint64_t m, uint64_t k)
    {
        m = columns_inv(m);
        m = rows_inv(m);
        m = sbox_inv(m);
        m ^= k & 0xFFFFFFFF00000000;

        return m;
    }

    static uint64_t next_key(uint64_t k) { return _rotr64(k ^ 3, 16); }

    static uint64_t prev_key(uint64_t k) { return _rotl64(k, 16) ^ 3; }

    uint64_t sched(uint64_t k, int rounds)
    {
        // key is repeated every 8 rounds
        rounds &= 7;
        for (int i = 0; i < rounds; ++i)
            k = next_key(k);

        return k;
    }

    uint64_t enc(uint64_t m, uint64_t k, int rounds)
    {
        for (int i = 0; i < rounds; ++i)
        {
            m = round(m, k);
            k = next_key(k);
        }

        return m;
    }

    uint64_t dec(uint64_t m, uint64_t k, int rounds)
    {
        k = sched(k, rounds);

        for (int i = 0; i < rounds; ++i)
        {
            k = prev_key(k);
            m = round_inv(m, k);
        }

        return m;
    }

} // namespace crypto::tc02
