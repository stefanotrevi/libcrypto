#include "cu_crypto.cuh"
#include "tc02/cu_tc02.hpp"

namespace cu::crypto::tc02::device
{

    __device__ static uint2 sbox(uint2 m)
    {
        static constexpr uint64_t sbox = 0xDC8970EB3FA16542;

        uint2 p{};

        for (uint32_t i = 0; i < 8; ++i)
        {
            p.x |= ((uint32_t)(sbox >> (m.x >> i * 4 & 0xF) * 4) & 0xF) << i * 4;
            p.y |= ((uint32_t)(sbox >> (m.y >> i * 4 & 0xF) * 4) & 0xF) << i * 4;
        }

        return m;
    }

    __device__ static uint2 rows(uint2 m)
    {
        uint16_t b = m.x, c = m.y >> 16, d = m.y;

        m.x &= 0xFFFF0000;
        m.x |= rotl(b, 4);
        m.y |= (uint32_t)rotl(c, 8) << 16;
        m.y |= rotl(d, 12);

        return m;
    }

    __device__ static uint2 columns(uint2 m)
    {
        uint16_t a = m.x >> 16, b = m.x, c = m.y >> 16, d = m.y;

        m.x = (uint32_t)(a ^ c) << 16 | (b ^ c);
        m.y = (uint32_t)(a ^ d) << 16 | c;

        return m;
    }

    __device__ static uint2 round(uint2 m, uint64_t k)
    {
        m.x ^= (uint32_t)(k >> 32);
        m = sbox(m);
        m = rows(m);
        m = columns(m);

        return m;
    }


    __device__ static uint64_t next_key(uint64_t k) { return rotr(k ^ 3, 16); }

    __device__ static uint64_t enc(uint64_t m, uint64_t k, int rounds)
    {
        uint2 vm{(uint32_t)(m >> 32), (uint32_t)m};

        for (uint32_t i = 0; i < rounds; ++i)
        {
            vm = round(vm, k);
            k = next_key(k);
        }

        return (uint64_t)vm.x << 32 | vm.y;
    }
} // namespace cu::crypto::tc02::device

namespace cu::crypto::tc02
{
    CU_DEFINE_TESTENC(test_enc, device::enc)
    CU_DEFINE_CRACKENC(crack, device::enc)
} // namespace cu::crypto::tc02
