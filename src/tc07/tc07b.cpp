#include "tc07/tc07b.hpp"
#include "avxlib.hpp"
#include "common.hpp"

namespace crypto::tc07b
{
    uint64_t sbox(uint64_t m)
    {
        __m128i sbox = _mm_setr_epi8(0xA, 0x5, 0x4, 0x2, 0x6, 0x1, 0xF, 0x3, //
                                     0xB, 0xE, 0x7, 0x0, 0x8, 0xD, 0xC, 0x9);

        return avx::sub_nib(m, sbox);
    }

    uint64_t sbox_inv(uint64_t m)
    {
        __m128i sbox = _mm_setr_epi8(0xB, 0x5, 0x3, 0x7, 0x2, 0x1, 0x4, 0xA, //
                                     0xC, 0xF, 0x0, 0x8, 0xE, 0xD, 0x9, 0x6);

        return avx::sub_nib(m, sbox);
    }

    uint64_t rows(uint64_t m)
    {
        uint64_t p = m & 0xFFFF000000000000;

        p |= (uint64_t)_rotwl(m >> 32, 4) << 32;
        p |= (uint64_t)_rotwl(m >> 16, 8) << 16;
        p |= (uint64_t)_rotwl(m >> 0, 12) << 0;

        return p;
    }

    uint64_t rows_inv(uint64_t m)
    {
        uint64_t p = m & 0xFFFF000000000000;

        p |= (uint64_t)_rotwr(m >> 32, 4) << 32;
        p |= (uint64_t)_rotwr(m >> 16, 8) << 16;
        p |= (uint64_t)_rotwr(m >> 0, 12) << 0;

        return p;
    }

    uint64_t columns(uint64_t m)
    {
        uint16_t a = m >> 48;
        uint16_t b = m >> 32;
        uint16_t c = m >> 16;
        uint16_t d = m >> 0;

        m = c ^ d;
        m |= (uint64_t)d << 16;
        m |= (uint64_t)(b ^ c) << 32;
        m |= (uint64_t)(a ^ c) << 48;

        return m;
    }

    uint64_t columns_inv(uint64_t m)
    {
        uint16_t d = m >> 16;
        uint16_t c = m ^ d;
        uint16_t b = (m >> 32) ^ c;
        uint16_t a = (m >> 48) ^ c;

        m = d;
        m |= (uint64_t)c << 16;
        m |= (uint64_t)b << 32;
        m |= (uint64_t)a << 48;

        return m;
    }

    static uint64_t round(uint64_t m, uint64_t k)
    {
        m ^= k & 0x00000000FFFFFFFF;
        m = sbox(m);
        m = rows(m);
        m = columns(m);

        return m;
    }

    static uint64_t round_inv(uint64_t m, uint64_t k)
    {
        m = columns_inv(m);
        m = rows_inv(m);
        m = sbox_inv(m);
        m ^= k & 0x00000000FFFFFFFF;

        return m;
    }

    uint64_t next_key(uint64_t k) { return _rotr64(k ^ 0xF3F3, 16); }
    uint64_t prev_key(uint64_t k) { return _rotl64(k, 16) ^ 0xF3F3; }

    uint64_t sched(uint64_t k, int rounds)
    {
        rounds %= 8; // after 8 rounds the key is reset
        for (int i = 0; i < rounds; ++i)
            k = next_key(k);

        return k;
    }

    uint64_t enc(uint64_t m, uint64_t k, int rounds)
    {
        for (int i = 0; i < rounds; ++i)
        {
            m = round(m, k);
            k = next_key(k);
        }

        return m;
    }

    uint64_t dec(uint64_t m, uint64_t k, int rounds)
    {
        k = sched(k, rounds);
        for (int i = 0; i < rounds; ++i)
        {
            k = prev_key(k);
            m = round_inv(m, k);
        }

        return m;
    }

    static uint16_t sbox(uint16_t x) { return sbox256[x >> 8] << 8 | sbox256[x & 0xFF]; }
    static uint16_t sbox_i(uint16_t x) { return sbox256_i[x >> 8] << 8 | sbox256_i[x & 0xFF]; }

    uint64_t enc2(uint64_t m, uint64_t k, int rounds)
    {
        uint16_t *p = (uint16_t *)&m;

        for (int i = 0; i < rounds; ++i)
        {
            // AddKey
            p[0] ^= k;
            p[1] ^= k >> 16;

            // Sbox
            p[0] = sbox(p[0]);
            p[1] = sbox(p[1]);
            p[2] = sbox(p[2]);
            p[3] = sbox(p[3]);

            // ShiftRows
            p[0] = _rotwl(p[0], 12);
            p[1] = _rotwl(p[1], 8);
            p[2] = _rotwl(p[2], 4);

            // MixColumns
            p[0] ^= p[1];
            p[2] ^= p[1];
            p[3] ^= p[1];
            p[1] ^= p[0];

            // KeySchedule
            k = next_key(k);
        }

        return m;
    }

    uint64_t dec2_nosched(uint64_t m, uint64_t k, int rounds)
    {
        uint16_t *p = (uint16_t *)&m;

        for (int i = 0; i < rounds; ++i)
        {
            // Invert KeySchedule
            k = prev_key(k);

            // Invert MixColumns
            p[1] ^= p[0];
            p[3] ^= p[1];
            p[2] ^= p[1];
            p[0] ^= p[1];

            // Invert ShiftRows
            p[2] = _rotwr(p[2], 4);
            p[1] = _rotwr(p[1], 8);
            p[0] = _rotwr(p[0], 12);

            // Invert Sbox
            p[3] = sbox_i(p[3]);
            p[2] = sbox_i(p[2]);
            p[1] = sbox_i(p[1]);
            p[0] = sbox_i(p[0]);

            // Invert AddKey
            p[1] ^= k >> 16;
            p[0] ^= k;
        }

        return m;
    }

    uint64_t dec2(uint64_t m, uint64_t k, int rounds)
    {
        k = sched(k, rounds);
        return dec2_nosched(m, k, rounds);
    }

    uint32_t pdec(uint32_t m)
    {
        uint16_t *p = (uint16_t *)&m;
        
        // Invert MixColumns
        p[1] ^= p[0];
        p[0] ^= p[1];

        // Invert ShiftRows
        p[1] = _rotwr(p[1], 8);
        p[0] = _rotwr(p[0], 12);

        // Invert Sbox
        p[1] = sbox_i(p[1]);
        p[0] = sbox_i(p[0]);

        return m;
    }

} // namespace tc07b
