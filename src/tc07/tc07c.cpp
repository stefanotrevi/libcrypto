#include "tc07/tc07c.hpp"
#include "avxlib.hpp"
#include "common.hpp"

namespace crypto::tc07c
{
    static uint64_t next_key(uint64_t k) { return _rotr64(k ^ 0xF3F3, 16); }
    static uint64_t prev_key(uint64_t k) { return _rotl64(k, 16) ^ 0xF3F3; }

    uint64_t sched(uint64_t k, int rounds)
    {
        rounds %= 8;
        for (int i = 0; i < rounds; ++i)
            k = next_key(k);

        return k;
    }

    static uint16_t sbox_fast(uint16_t x) { return sbox256[x >> 8] << 8 | sbox256[x & 0xFF]; };
    static uint16_t sbox_fast_i(uint16_t x)
    {
        return sbox256_i[x >> 8] << 8 | sbox256_i[x & 0xFF];
    };

    uint64_t enc(uint64_t m, uint64_t k, int rounds)
    {
        uint16_t *p = (uint16_t *)&m;
        uint16_t t[4];

        for (int i = 0; i < rounds; ++i)
        {
            // AddKey
            p[0] ^= k;
            p[1] ^= k >> 16;

            // Sbox
            p[0] = sbox_fast(p[0]);
            p[1] = sbox_fast(p[1]);
            p[2] = sbox_fast(p[2]);
            p[3] = sbox_fast(p[3]);

            // ShiftRows
            p[0] = _rotwl(p[0], 12);
            p[1] = _rotwl(p[1], 8);
            p[2] = _rotwl(p[2], 4);

            // MixColumns
            t[0] = p[0];
            t[1] = p[1];
            t[2] = p[2];
            t[3] = p[3];

            p[0] = t[1] ^ t[3];
            p[1] = t[0] ^ t[1];
            p[2] = t[0] ^ t[1] ^ t[2] ^ t[3];
            p[3] = t[2] ^ t[3];

            // KeySchedule
            k = next_key(k);
        }

        return m;
    }

    uint64_t enc_nomix(uint64_t m, uint64_t k, int rounds)
    {
        uint16_t *p = (uint16_t *)&m;

        for (int i = 0; i < rounds; ++i)
        {
            // AddKey
            p[0] ^= k;
            p[1] ^= k >> 16;

            // Sbox
            p[0] = sbox_fast(p[0]);
            p[1] = sbox_fast(p[1]);
            p[2] = sbox_fast(p[2]);
            p[3] = sbox_fast(p[3]);

            // ShiftRows
            p[0] = _rotwl(p[0], 12);
            p[1] = _rotwl(p[1], 8);
            p[2] = _rotwl(p[2], 4);

            // KeySchedule
            k = next_key(k);
        }

        return m;
    }

    uint64_t dec_nomix(uint64_t m, uint64_t k, int rounds)
    {
        uint16_t *p = (uint16_t *)&m;

        k = sched(k, rounds);
        for (int i = 0; i < rounds; ++i)
        {
            // KeySchedule
            k = prev_key(k);

            // ShiftRows
            p[2] = _rotwr(p[2], 4);
            p[1] = _rotwr(p[1], 8);
            p[0] = _rotwr(p[0], 12);

            // Sbox
            p[3] = sbox_fast_i(p[3]);
            p[2] = sbox_fast_i(p[2]);
            p[1] = sbox_fast_i(p[1]);
            p[0] = sbox_fast_i(p[0]);

            // AddKey
            p[1] ^= k >> 16;
            p[0] ^= k;
        }

        return m;
    }
} // namespace crypto::tc07c
