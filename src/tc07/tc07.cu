#include "cu_crypto.cuh"
#include "intrinsics.h"
#include "tc07/cu_tc07.hpp"

namespace cu::crypto::tc07::device
{
    __device__ static uint2 sbox(uint2 m)
    {
        static constexpr uint64_t sbox = 0x9CD807EB3F16245A;

        uint2 p{};

        for (uint32_t i = 0; i < 8; ++i)
        {
            p.x |= ((uint32_t)(sbox >> (m.x >> i * 4 & 0xF) * 4) & 0xF) << i * 4;
            p.y |= ((uint32_t)(sbox >> (m.y >> i * 4 & 0xF) * 4) & 0xF) << i * 4;
        }

        return p;
    }

    __device__ static uint2 rows(uint2 m)
    {
        uint16_t b = m.x, c = m.y >> 16, d = m.y;

        m.x &= 0xFFFF0000;
        m.x |= rotl(b, 4);
        m.y = (uint32_t)rotl(c, 8) << 16;
        m.y |= rotl(d, 12);

        return m;
    }

    __device__ static uint2 columns(uint2 m)
    {
        uint16_t a = m.x >> 16, b = m.x, c = m.y >> 16, d = m.y;

        m.x = (uint32_t)(a ^ c) << 16 | (b ^ c);
        m.y = (uint32_t)(a ^ d) << 16 | (c ^ d);

        return m;
    }

    __device__ static uint2 round(uint2 m, uint64_t k)
    {
        m.y ^= (uint32_t)k;
        m = sbox(m);
        m = rows(m);
        m = columns(m);

        return m;
    }


    __device__ static uint64_t next_key(uint64_t k) { return rotr(k ^ 0xF3F3, 16); }

    __device__ static uint64_t enc(uint64_t m, uint64_t k, int rounds)
    {
        uint2 vm{(uint32_t)(m >> 32), (uint32_t)m};

        for (uint32_t i = 0; i < rounds; ++i)
        {
            vm = round(vm, k);
            k = next_key(k);
        }

        return (uint64_t)vm.x << 32 | vm.y;
    }

    static __global__ void candidates_ker(uint64_t *can, uint32_t *can_sz, const uint64_t *msg,
                                          const uint64_t *cyp, int rounds, uint32_t msg_sz,
                                          uint32_t nib, uint64_t dep, uint64_t off)
    {
        uint32_t id = blockIdx.x * blockDim.x + threadIdx.x;
        uint64_t i = off + id;
        uint64_t key = pdep_4(i, dep);
        bool valid = true;

        for (uint32_t k = 0; k < msg_sz; ++k)
        {
            uint32_t c = cyp[k] >> 4 * nib & 0xF;
            uint32_t m_c = enc(msg[k], key, rounds) >> 4 * nib & 0xF;

            valid &= (c == m_c);
        }

        if (valid)
            can[(*can_sz)++] = key;
    }
} // namespace cu::crypto::tc07::device

namespace cu::crypto::tc07
{
    Vector candidates(const Vector &msg, const Vector &cyp, int rounds,
                      std::pair<uint32_t, uint64_t> dep)
    {
        static constexpr uint32_t GRDSZ = 1 << 12;
        static constexpr uint32_t BLKSZ = MAX_BLKSZ;
        static constexpr uint64_t check = 1ULL << 28;

        uint64_t n = 1ULL << _popcnt64(dep.second);
        uint64_t *d_cand, *d_msg, *d_cyp;
        uint32_t *d_cand_sz;
        uint32_t cand_sz = 0;

        cudaMalloc(&d_cand, (1 << 24) * sizeof(*d_cand));

        cudaMalloc(&d_msg, msg.size() * sizeof(*d_msg));
        cudaMemcpy(d_msg, msg.data(), msg.size() * sizeof(*d_msg), cudaMemcpyHostToDevice);

        cudaMalloc(&d_cyp, cyp.size() * sizeof(*d_cyp));
        cudaMemcpy(d_cyp, cyp.data(), cyp.size() * sizeof(*d_cyp), cudaMemcpyHostToDevice);

        cudaMalloc(&d_cand_sz, sizeof(*d_cand_sz));
        cudaMemset(d_cand_sz, 0, sizeof(*d_cand_sz));

        for (uint64_t off = 0; off < n; off += GRDSZ * BLKSZ)
        {
            device::candidates_ker<<<GRDSZ, BLKSZ>>>(d_cand, d_cand_sz, d_msg, d_cyp, rounds,
                                                     msg.size(), dep.first, dep.second, off);
            if (!(off % check))
                (std::cout << off << '/' << n << '\r').flush();
        }
        cudaDeviceSynchronize();
        cudaMemcpy(&cand_sz, d_cand_sz, sizeof(cand_sz), cudaMemcpyDeviceToHost);
        std::cout << "\nCandiates found: " << cand_sz << '\n';

        Vector cand(cand_sz);

        cudaMemcpy(cand.data(), d_cand, cand_sz * sizeof(*cand.data()), cudaMemcpyDeviceToHost);

        cudaFree(d_cand);
        cudaFree(d_msg);
        cudaFree(d_cyp);

        return cand;
    }

    CU_DEFINE_TESTENC(test_enc, device::enc)
    CU_DEFINE_CRACKENC(crack, device::enc)
} // namespace cu::crypto::tc07
