#include "ntlm/ntlm.hpp"
#include "des/des.hpp"
#include "intrinsics.h"
#include "md4/md4.hpp"

namespace crypto::ntlm1
{
    void hash(void *digest, const void *msg, size_t sz)
    {
        static constexpr uint64_t fix = "KGS!@#$%"_u64;

        const uint8_t *m = (uint8_t *)msg;
        uint8_t pad[16] = {0};
        uint64_t key0 = 0, key1 = 0;
        uint64_t *dig = (uint64_t *)digest;

        for (size_t i = 0; i < 14 && i < sz; ++i)
            pad[i] = (uint8_t)toupper(m[i]);

        for (size_t i = 0; i < 7; ++i)
        {
            key0 |= (uint64_t)pad[i] << (6 - i) * 8;
            key1 |= (uint64_t)pad[i + 7] << (6 - i) * 8;
        }

        key0 = _pdep_u64(key0, 0xFEFEFEFEFEFEFEFE);
        key1 = _pdep_u64(key1, 0xFEFEFEFEFEFEFEFE);

        dig[0] = des::enc2(fix, key0);
        dig[1] = des::enc2(fix, key1);
    }
} // namespace ntlm1

namespace crypto::ntlm2
{
    void hash(void *digest, const void *msg, size_t sz)
    {
        const char *m = (const char *)msg;

        std::string msg_utf16(sz * 2, 0);

        for (size_t i = 0; i < sz; ++i)
            msg_utf16[i * 2] = m[i];

        md4::hash(digest, msg_utf16.data(), msg_utf16.size());
    }
} // namespace ntlm2
