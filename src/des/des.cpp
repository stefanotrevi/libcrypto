#include "des/des.hpp"
#include "avxlib.hpp"

#include <omp.h>

namespace crypto::des // Helper functions
{
    static constexpr uint64_t reverse(uint64_t x) // swaps bits in bytes
    {
        x = (x & 0x5555555555555555) << 1 | (x & ~0x5555555555555555) >> 1;
        x = (x & 0x3333333333333333) << 2 | (x & ~0x3333333333333333) >> 2;
        x = (x & 0x0F0F0F0F0F0F0F0F) << 4 | (x & ~0x0F0F0F0F0F0F0F0F) >> 4;

        return x;
    }

    static constexpr uint64_t rotl(uint64_t x, uint8_t s, uint8_t msb)
    {
        return (x << s & ((1 << msb) - 1)) | (x >> (msb - s) & ((1 << s) - 1));
    }

    static constexpr uint64_t permute(uint64_t x, const uint8_t *ptable, size_t siz)
    {
        uint64_t p = 0;

        for (size_t i = 0; i < siz; ++i)
            p |= (x >> ptable[i] & 1) << i;

        return p;
    }
}; // namespace crypto::des

namespace crypto::des // "Naive" implementation
{
    uint64_t reduce_key(uint64_t k)
    {
        static constexpr uint8_t ptable[56] = {
            60, 52, 44, 36,                //
            59, 51, 43, 35, 27, 19, 11, 3, //
            58, 50, 42, 34, 26, 18, 10, 2, //
            57, 49, 41, 33, 25, 17, 9,  1, //
            28, 20, 12, 4,                 //
            61, 53, 45, 37, 29, 21, 13, 5, //
            62, 54, 46, 38, 30, 22, 14, 6, //
            63, 55, 47, 39, 31, 23, 15, 7,
        };

        return permute(k, ptable, sizeof(ptable));
    }

    uint64_t augment_key(uint64_t k)
    {
        static constexpr uint8_t ptable[64] = {
            0, 27, 19, 11, 31, 39, 47, 55, //
            0, 26, 18, 10, 30, 38, 46, 54, //
            0, 25, 17, 9,  29, 37, 45, 53, //
            0, 24, 16, 8,  28, 36, 44, 52, //
            0, 23, 15, 7,  3,  35, 43, 51, //
            0, 22, 14, 6,  2,  34, 42, 50, //
            0, 21, 13, 5,  1,  33, 41, 49, //
            0, 20, 12, 4,  0,  32, 40, 48,
        };

        return permute(k, ptable, sizeof(ptable));
    }

    uint64_t initial_permute(uint64_t m)
    {
        static constexpr uint8_t ptable[64] = {
            57, 49, 41, 33, 25, 17, 9,  1, //
            59, 51, 43, 35, 27, 19, 11, 3, //
            61, 53, 45, 37, 29, 21, 13, 5, //
            63, 55, 47, 39, 31, 23, 15, 7, //
            56, 48, 40, 32, 24, 16, 8,  0, //
            58, 50, 42, 34, 26, 18, 10, 2, //
            60, 52, 44, 36, 28, 20, 12, 4, //
            62, 54, 46, 38, 30, 22, 14, 6, //
        };

        return permute(m, ptable, sizeof(ptable));
    }

    uint64_t rotate_key(uint64_t k, uint8_t s)
    {
        return rotl(k >> 28, s, 28) << 28 | rotl(k & 0x0FFFFFFF, s, 28);
    }

    uint64_t compress_key(uint64_t k)
    {
        static constexpr uint8_t ptable[48] = {
            24, 27, 20, 6,  14, 10, 3,  22, //
            0,  17, 7,  12, 8,  23, 11, 5,  //
            16, 26, 1,  9,  19, 25, 4,  15, //
            54, 43, 36, 29, 49, 40, 48, 30, //
            52, 44, 37, 33, 46, 35, 50, 41, //
            28, 53, 51, 55, 32, 45, 39, 42, //
        };

        return permute(k, ptable, sizeof(ptable));
    }

    void build_round_keys(uint64_t k, uint64_t sk[ROUNDS])
    {
        static constexpr uint8_t shift[ROUNDS] = {1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};

        for (size_t i = 0; i < ROUNDS; ++i)
        {
            k = rotate_key(k, shift[i]);
            sk[i] = compress_key(k);
        }
    }

    uint64_t expand(uint32_t m)
    {
        static constexpr uint8_t ptable[48] = {
            31, 0,  1,  2,  3,  4,  3,  4,  //
            5,  6,  7,  8,  7,  8,  9,  10, //
            11, 12, 11, 12, 13, 14, 15, 16, //
            15, 16, 17, 18, 19, 20, 19, 20, //
            21, 22, 23, 24, 23, 24, 25, 26, //
            27, 28, 27, 28, 29, 30, 31, 0,  //
        };

        return permute(m, ptable, sizeof(ptable));
    }

    uint32_t sbox(uint64_t m)
    {
        static constexpr uint8_t box[8][4][16] = {
            {
                {13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7},
                {1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2},
                {7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8},
                {2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11},
            },
            {
                {4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1},
                {13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6},
                {1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2},
                {6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12},
            },
            {
                {12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11},
                {10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8},
                {9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6},
                {4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13},
            },
            {
                {2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9},
                {14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6},
                {4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14},
                {11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3},
            },
            {
                {7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15},
                {13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9},
                {10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4},
                {3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14},
            },
            {
                {10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8},
                {13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1},
                {13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7},
                {1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12},
            },
            {
                {15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10},
                {3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5},
                {0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15},
                {13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9},
            },
            {
                {14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7},
                {0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8},
                {4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0},
                {15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13},
            },

        };

        uint32_t new_m = 0;

        for (size_t i = 0; i < 8; ++i)
        {
            size_t j = (m & 1) | ((m >> 4) & 2);
            size_t k = (m & 0b011110) >> 1;

            new_m |= box[i][j][k] << (4 * i);
            m >>= 6;
        }

        return new_m;
    }

    uint32_t perm(uint32_t m)
    {
        static constexpr uint8_t ptable[32] = {
            7,  28, 21, 10, 26, 2,  19, 13, //
            23, 29, 5,  0,  18, 8,  24, 30, //
            22, 1,  14, 27, 6,  9,  17, 31, //
            15, 4,  20, 3,  11, 12, 25, 16, //
        };

        return (uint32_t)permute(m, ptable, sizeof(ptable));
    }

    uint32_t round(uint32_t m, uint64_t k)
    {
        return perm(sbox(expand(m) ^ k));
    }

    uint64_t final_permute(uint64_t m)
    {
        static constexpr uint8_t ptable[64] = {
            39, 7, 47, 15, 55, 23, 63, 31, //
            38, 6, 46, 14, 54, 22, 62, 30, //
            37, 5, 45, 13, 53, 21, 61, 29, //
            36, 4, 44, 12, 52, 20, 60, 28, //
            35, 3, 43, 11, 51, 19, 59, 27, //
            34, 2, 42, 10, 50, 18, 58, 26, //
            33, 1, 41, 9,  49, 17, 57, 25, //
            32, 0, 40, 8,  48, 16, 56, 24, //
        };

        return permute(m, ptable, sizeof(ptable));
    }

    uint64_t enc(uint64_t m, uint64_t k, int rounds)
    {
        k = reduce_key(k);
        m = initial_permute(m);
        uint32_t ml = m >> 32;
        uint32_t mr = (uint32_t)m;
        uint64_t sk[ROUNDS];

        build_round_keys(k, sk);
        for (int i = 0; i < rounds; ++i)
        {
            uint32_t ml_old = ml;
            ml = mr;
            mr = ml_old ^ round(mr, sk[i]);
        }

        m = (uint64_t)mr << 32 | ml; // reverse order
        m = final_permute(m);

        return m;
    }

    uint64_t dec(uint64_t m, uint64_t k, int rounds)
    {
        k = reduce_key(k);

        m = initial_permute(m);
        uint32_t ml = m >> 32;
        uint32_t mr = (uint32_t)m;
        uint64_t sk[ROUNDS];

        build_round_keys(k, sk);
        for (int i = 0; i < rounds; ++i)
        {
            uint32_t ml_old = ml;
            ml = mr;
            mr = ml_old ^ round(mr, sk[(rounds - 1) - i]);
        }

        m = (uint64_t)mr << 32 | ml; // reverse order
        m = final_permute(m);

        return m;
    }
}; // namespace crypto::des

namespace crypto::des // Optimized implementation
{
    uint64_t reduce_key2(uint64_t k)
    {
        uint64_t p = 0;

        // insert consecutive bytes
        p |= _pext_u64(k, 0x01010101ULL << 35 | 0x01010101ULL << 3) << 4;
        p |= _pext_u64(k, 0x01010101ULL << 34 | 0x01010101ULL << 2) << 12;
        p |= _pext_u64(k, 0x01010101ULL << 33 | 0x01010101ULL << 1) << 20;
        p |= _pext_u64(k, 0x01010101ULL << 37 | 0x01010101ULL << 5) << 32;
        p |= _pext_u64(k, 0x01010101ULL << 38 | 0x01010101ULL << 6) << 40;
        p |= _pext_u64(k, 0x01010101ULL << 39 | 0x01010101ULL << 7) << 48;

        // reorder nibbles. we ignore the first and eigth nibbles
        p = (p & 0x0F0F0F0F'00F0F0F0) << 4 | (p & 0xF0F0F0F0'0F0F0F00) >> 4;

        // insert consecutive nibbles
        p |= _pext_u64(k, 0x01010101ULL << 36) << 0;
        p |= _pext_u64(k, 0x01010101ULL << 4) << 28;

        // reorder bits in nibbles
        p = (p & 0x3333333333333333) << 2 | (p & ~0x3333333333333333) >> 2;
        p = (p & 0x5555555555555555) << 1 | (p & ~0x5555555555555555) >> 1;

        return p & 0x00FFFFFFFFFFFFFF;
    }

    uint64_t augment_key2(uint64_t k)
    {
        uint64_t p = 0;

        // reorder bits in nibbles
        k = (k & 0x5555555555555555) << 1 | (k & ~0x5555555555555555) >> 1;
        k = (k & 0x3333333333333333) << 2 | (k & ~0x3333333333333333) >> 2;

        // insert consecutive nibbles
        p |= _pdep_u64(k >> 0, 0x01010101ULL << 36);
        p |= _pdep_u64(k >> 28, 0x01010101ULL << 4);

        k = (k & 0x0F0F0F0F'00F0F0F0) << 4 | (k & 0xF0F0F0F0'0F0F0F00) >> 4;

        // insert consecutive bytes
        p |= _pdep_u64(k >> 48, 0x01010101ULL << 39 | 0x01010101ULL << 7);
        p |= _pdep_u64(k >> 40, 0x01010101ULL << 38 | 0x01010101ULL << 6);
        p |= _pdep_u64(k >> 32, 0x01010101ULL << 37 | 0x01010101ULL << 5);
        p |= _pdep_u64(k >> 20, 0x01010101ULL << 33 | 0x01010101ULL << 1);
        p |= _pdep_u64(k >> 12, 0x01010101ULL << 34 | 0x01010101ULL << 2);
        p |= _pdep_u64(k >> 4, 0x01010101ULL << 35 | 0x01010101ULL << 3);


        return p;
    }

    uint64_t initial_permute2(uint64_t m)
    {
        uint64_t p = 0;

        p |= _pext_u64(m, 0x0101010101010101ULL << 1) << 0;
        p |= _pext_u64(m, 0x0101010101010101ULL << 3) << 8;
        p |= _pext_u64(m, 0x0101010101010101ULL << 5) << 16;
        p |= _pext_u64(m, 0x0101010101010101ULL << 7) << 24;

        p |= _pext_u64(m, 0x0101010101010101ULL << 0) << 32;
        p |= _pext_u64(m, 0x0101010101010101ULL << 2) << 40;
        p |= _pext_u64(m, 0x0101010101010101ULL << 4) << 48;
        p |= _pext_u64(m, 0x0101010101010101ULL << 6) << 56;

        return reverse(p);
    }

    uint64_t compress_key2(uint64_t k)
    {
        static constexpr uint8_t ptabler[32] = {
            24,   27,   20,   6,    14,   10,   3,    22,   //
            0,    17,   7,    12,   8,    23,   11,   5,    //
            16,   26,   1,    9,    19,   25,   4,    15,   //
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, //
        };
        static constexpr uint8_t ptablel[32] = {
            26,   15,   8,    1,    21,   12,   20,   2,    //
            24,   16,   9,    5,    18,   7,    22,   13,   //
            0,    25,   23,   27,   4,    17,   11,   14,   //
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, //
        };

        const __m256i pbitsl = mm256_ptable_to_pbits(ptablel);
        const __m256i pbytesl = mm256_ptable_to_pbytes(ptablel);
        const __m256i pbitsr = mm256_ptable_to_pbits(ptabler);
        const __m256i pbytesr = mm256_ptable_to_pbytes(ptabler);

        uint32_t kl = avx::permute((uint32_t)(k >> 28), pbitsl, pbytesl);
        uint32_t kr = avx::permute((uint32_t)k, pbitsr, pbytesr);

        return (uint64_t)kl << 24 | kr;
    }

    void build_round_keys2(uint64_t k, uint64_t *sk)
    {
        static constexpr uint8_t shift[ROUNDS] = {1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};

        for (size_t i = 0; i < ROUNDS; ++i)
        {
            k = rotate_key(k, shift[i]);
            sk[i] = compress_key2(k);
        }
    }

    uint64_t expand2(uint32_t m)
    {
        uint32_t sel = _pext_u32(m, 0x19999998U);
        uint64_t exp1 = _pdep_u64(m, 0x000039E79E79E79FU);
        uint64_t exp2 = _pdep_u64(sel, ~0x000039E79E79E79FU);

        return (exp1 | exp2) << 1 | m >> 31 | (uint64_t)(m & 1) << 47;
    }

    uint32_t sbox2(uint64_t m)
    {
        static constexpr uint64_t box[8][4] = {
            {0x7C05E39A1BF6482D, 0x29E0B65C473A8DF1, 0x853FDA602EC914B7, 0xB65309CFD8A47E12},
            {0x16A579C3D80FE2B4, 0x68F2C53EA1947B0D, 0x295086FAE73CDB41, 0xC32EF0597A418DB6},
            {0xB57E43D08629FA1C, 0x83B0ED1659C724FA, 0x6BD1A4073C825FE9, 0xD80671EBAF59C234},
            {0x9E0DF3586BA714C2, 0x6893AF051D74C2BE, 0xE0365C9F87DAB124, 0x354A90F6D2E17C8B},
            {0xF4CB5821A9603ED7, 0x9EA1C27430F65B8D, 0x4825E31FD7BC096A, 0xE27CB5498D1A60F3},
            {0x824B7CD15F36E90A, 0x1FBCE582A643907D, 0x7EA5C21B03F8946D, 0xC25B3EF478960DA1},
            {0xA50CD27943B6E81F, 0x5B96A10CE82F74D3, 0xF2396C851D4AB7E0, 0x9E50C76B24F31A8D},
            {0x7095C6A38BF21D4E, 0x8359BC6A1D2E47F0, 0x05A379CFB26D8E14, 0xD60AE3B5719428CF},
        };

        uint32_t new_m = 0;
        // select the first and last bits in every 6-bit "nibble", speeding up the index retrieval.
        uint16_t js = (uint16_t)_pext_u64(m, 0x0000861861861861);
        uint32_t ks = (uint32_t)_pext_u64(m, ~0x0000861861861861);


        new_m |= (box[0][js >> 0 & 0x3] >> (ks >> 0 & 0xF) * 4 & 0xF) << 0;
        new_m |= (box[1][js >> 2 & 0x3] >> (ks >> 4 & 0xF) * 4 & 0xF) << 4;
        new_m |= (box[2][js >> 4 & 0x3] >> (ks >> 8 & 0xF) * 4 & 0xF) << 8;
        new_m |= (box[3][js >> 6 & 0x3] >> (ks >> 12 & 0xF) * 4 & 0xF) << 12;
        new_m |= (box[4][js >> 8 & 0x3] >> (ks >> 16 & 0xF) * 4 & 0xF) << 16;
        new_m |= (box[5][js >> 10 & 0x3] >> (ks >> 20 & 0xF) * 4 & 0xF) << 20;
        new_m |= (box[6][js >> 12 & 0x3] >> (ks >> 24 & 0xF) * 4 & 0xF) << 24;
        new_m |= (box[7][js >> 14 & 0x3] >> (ks >> 28 & 0xF) * 4 & 0xF) << 28;

        return new_m;
    }

    uint32_t perm2(uint32_t m)
    {
        static constexpr uint8_t ptable[32] = {
            7,  28, 21, 10, 26, 2,  19, 13, //
            23, 29, 5,  0,  18, 8,  24, 30, //
            22, 1,  14, 27, 6,  9,  17, 31, //
            15, 4,  20, 3,  11, 12, 25, 16, //
        };
        static const __m256i pbits = mm256_ptable_to_pbits(ptable);
        static const __m256i pbytes = mm256_ptable_to_pbytes(ptable);

        return avx::permute(m, pbits, pbytes);
    }

    uint32_t round2(uint32_t m, uint64_t k)
    {
        uint64_t m_exp = expand2(m) ^ k;

        m = sbox2(m_exp);
        m = perm2(m);

        return m;
    }

    uint64_t final_permute2(uint64_t m)
    {
        uint64_t p = 0;

        p |= _pdep_u64(m >> 0, 0x0101010101010101ULL << 1);
        p |= _pdep_u64(m >> 8, 0x0101010101010101ULL << 3);
        p |= _pdep_u64(m >> 16, 0x0101010101010101ULL << 5);
        p |= _pdep_u64(m >> 24, 0x0101010101010101ULL << 7);
        p |= _pdep_u64(m >> 32, 0x0101010101010101ULL << 0);
        p |= _pdep_u64(m >> 40, 0x0101010101010101ULL << 2);
        p |= _pdep_u64(m >> 48, 0x0101010101010101ULL << 4);
        p |= _pdep_u64(m >> 56, 0x0101010101010101ULL << 6);

        return _bswap64(p);
    }

    uint64_t enc2(uint64_t m, uint64_t k, int rounds)
    {
        k = reduce_key2(k);

        m = initial_permute2(m);
        uint32_t ml = m >> 32;
        uint32_t mr = (uint32_t)m;

        for (int i = 0; i < rounds; ++i)
        {
            static constexpr uint16_t shift = 0b0111111011111100;

            uint32_t ml_old = ml;
            ml = mr;
            k = rotate_key(k, (uint8_t)((shift >> i & 1) + 1));
            mr = ml_old ^ round2(mr, compress_key2(k));
        }

        m = (uint64_t)mr << 32 | ml;
        m = final_permute2(m);

        return m;
    }

    uint64_t dec2(uint64_t m, uint64_t k, int rounds)
    {
        k = reduce_key2(k);

        m = initial_permute2(m);
        uint32_t ml = m >> 32;
        uint32_t mr = (uint32_t)m;
        uint64_t sk[ROUNDS];

        build_round_keys2(k, sk);
        for (int i = 0; i < rounds; ++i)
        {
            uint32_t ml_old = ml;
            ml = mr;
            mr = ml_old ^ round2(mr, sk[(rounds - 1) - i]);
        }

        m = (uint64_t)mr << 32 | ml; // reverse order
        m = final_permute2(m);

        return m;
    }

    static uint64_t enc_short(uint64_t m, uint64_t k)
    {
        // we skip the initial and final permutation
        uint32_t ml = m >> 32;
        uint32_t mr = (uint32_t)m;

        for (size_t i = 0; i < ROUNDS; ++i)
        {
            constexpr uint16_t shift = 0b0111111011111100;

            uint32_t ml_old = ml;
            ml = mr;
            k = rotate_key(k, (uint8_t)((shift >> i & 1) + 1));
            mr = ml_old ^ round2(mr, compress_key2(k));
        }

        return (uint64_t)mr << 32 | ml;
    }

    uint64_t crack(uint64_t m, uint64_t c, uint64_t off)
    {
        m = initial_permute2(m);
        c = initial_permute2(c);
        for (uint64_t k = off;; ++k)
            if (enc_short(m, k) == c)
                return augment_key2(k);
    }

    uint64_t crack_parallel(uint64_t m, uint64_t c, uint64_t off)
    {
        uint64_t real_k = 0;
        bool found = false;

        m = initial_permute2(m);
        c = initial_permute2(c);
#pragma omp parallel shared(real_k, found)
        {
            int n = omp_get_num_threads();
            int i = omp_get_thread_num();

            for (uint64_t k = off + i; !found; k += n)
                if (enc_short(m, k) == c)
                {
                    found = true;
                    real_k = k;
                    break;
                }
        }

        return augment_key2(real_k);
    }

} // namespace crypto::des
