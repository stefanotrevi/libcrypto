#include "cu_crypto.cuh"
#include "des/cu_des.hpp"
#include "des/des.hpp"

#include <iostream>

namespace cu::crypto::des::device
{
    __device__ static uint32_t rotl(uint32_t x, uint32_t s, uint32_t msb)
    {
        return (x << s & ((1 << msb) - 1)) | (x >> (msb - s) & ((1 << s) - 1));
    }

    __device__ static uint64_t permute(uint64_t x, const uint8_t *ptable, uint32_t msb)
    {
        uint64_t p = 0;

        for (uint32_t i = 0; i < msb; ++i)
            p |= ((x >> ptable[i]) & 1) << i;

        return p;
    }

    __device__ static uint32_t permute(uint32_t x, const uint8_t *ptable, uint32_t msb)
    {
        uint32_t p = 0;

        for (uint32_t i = 0; i < msb; ++i)
            p |= ((x >> ptable[i]) & 1) << i;

        return p;
    }

    __device__ static uint64_t reduce_key(uint64_t k)
    {
        static constexpr uint8_t ptable[56] = {
            60, 52, 44, 36,                //
            59, 51, 43, 35, 27, 19, 11, 3, //
            58, 50, 42, 34, 26, 18, 10, 2, //
            57, 49, 41, 33, 25, 17, 9,  1, //
            28, 20, 12, 4,                 //
            61, 53, 45, 37, 29, 21, 13, 5, //
            62, 54, 46, 38, 30, 22, 14, 6, //
            63, 55, 47, 39, 31, 23, 15, 7,
        };

        return permute(k, ptable, sizeof(ptable));
    }

    __device__ static uint64_t initial_permute(uint64_t m)
    {
        static constexpr uint8_t ptable[64] = {
            57, 49, 41, 33, 25, 17, 9,  1, //
            59, 51, 43, 35, 27, 19, 11, 3, //
            61, 53, 45, 37, 29, 21, 13, 5, //
            63, 55, 47, 39, 31, 23, 15, 7, //
            56, 48, 40, 32, 24, 16, 8,  0, //
            58, 50, 42, 34, 26, 18, 10, 2, //
            60, 52, 44, 36, 28, 20, 12, 4, //
            62, 54, 46, 38, 30, 22, 14, 6,
        };
        return permute(m, ptable, sizeof(ptable));
    }

    __device__ static uint2 rotate_key(uint2 k, uint8_t s)
    {
        k.x = rotl(k.x, s, 28);
        k.y = rotl(k.y, s, 28);

        return k;
    }

    __device__ static uint2 compress_key(uint2 k)
    {
        static constexpr uint8_t rtable[24] = {
            24, 27, 20, 6,  14, 10, 3,  22, //
            0,  17, 7,  12, 8,  23, 11, 5,  //
            16, 26, 1,  9,  19, 25, 4,  15, //
        };
        static constexpr uint8_t ltable[24] = {
            26, 15, 8,  1,  21, 12, 20, 2,  //
            24, 16, 9,  5,  18, 7,  22, 13, //
            0,  25, 23, 27, 4,  17, 11, 14, //
        };

        k.x = permute(k.x, ltable, sizeof(ltable));
        k.y = permute(k.y, rtable, sizeof(rtable));

        return k;
    }

    __device__ static void build_round_keys(uint64_t k, uint2 sk[ROUNDS])
    {
        static constexpr uint8_t shift[ROUNDS] = {1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};

        uint2 kv{(uint32_t)(k >> 28), (uint32_t)k & 0x0FFFFFFF};

        for (size_t i = 0; i < ROUNDS; ++i)
        {
            kv = rotate_key(kv, shift[i]);
            sk[i] = compress_key(kv);
        }
    }

    __device__ static uint2 expand(uint32_t m)
    {
        static constexpr uint8_t rtable[24] = {
            31, 0,  1,  2,  3,  4,  3,  4,  //
            5,  6,  7,  8,  7,  8,  9,  10, //
            11, 12, 11, 12, 13, 14, 15, 16, //
        };
        static constexpr uint8_t ltable[24] = {
            15, 16, 17, 18, 19, 20, 19, 20, //
            21, 22, 23, 24, 23, 24, 25, 26, //
            27, 28, 27, 28, 29, 30, 31, 0,  //
        };
        uint2 p{permute(m, ltable, sizeof(ltable)), permute(m, rtable, sizeof(rtable))};

        return p;
    }

    __device__ static uint32_t sbox(uint2 m)
    {
        static constexpr uint64_t box[8][4] = {
            {0x7C05E39A1BF6482D, 0x29E0B65C473A8DF1, 0x853FDA602EC914B7, 0xB65309CFD8A47E12},
            {0x16A579C3D80FE2B4, 0x68F2C53EA1947B0D, 0x295086FAE73CDB41, 0xC32EF0597A418DB6},
            {0xB57E43D08629FA1C, 0x83B0ED1659C724FA, 0x6BD1A4073C825FE9, 0xD80671EBAF59C234},
            {0x9E0DF3586BA714C2, 0x6893AF051D74C2BE, 0xE0365C9F87DAB124, 0x354A90F6D2E17C8B},
            {0xF4CB5821A9603ED7, 0x9EA1C27430F65B8D, 0x4825E31FD7BC096A, 0xE27CB5498D1A60F3},
            {0x824B7CD15F36E90A, 0x1FBCE582A643907D, 0x7EA5C21B03F8946D, 0xC25B3EF478960DA1},
            {0xA50CD27943B6E81F, 0x5B96A10CE82F74D3, 0xF2396C851D4AB7E0, 0x9E50C76B24F31A8D},
            {0x7095C6A38BF21D4E, 0x8359BC6A1D2E47F0, 0x05A379CFB26D8E14, 0xD60AE3B5719428CF},
        };

        uint32_t new_m = 0;

        for (uint8_t i = 0; i < 4; ++i)
        {
            uint8_t j = (m.y & 1) | (m.y >> 4 & 2);
            uint8_t k = (m.y & 0b011110) >> 1;

            new_m |= ((uint8_t)(box[i][j] >> (k * 4)) & 0xF) << (i * 4);
            m.y >>= 6;
        }
        for (uint8_t i = 4; i < 8; ++i)
        {
            uint8_t j = (m.x & 1) | (m.x >> 4 & 2);
            uint8_t k = (m.x & 0b011110) >> 1;

            new_m |= ((uint8_t)(box[i][j] >> (k * 4)) & 0xF) << (i * 4);
            m.x >>= 6;
        }

        return new_m;
    }

    __device__ uint32_t perm(uint32_t m)
    {
        static constexpr uint8_t ptable[32] = {
            7,  28, 21, 10, 26, 2,  19, 13, //
            23, 29, 5,  0,  18, 8,  24, 30, //
            22, 1,  14, 27, 6,  9,  17, 31, //
            15, 4,  20, 3,  11, 12, 25, 16, //
        };

        return permute(m, ptable, sizeof(ptable));
    }

    __device__ static uint32_t round(uint32_t m, uint2 k)
    {
        uint2 e{expand(m)};

        e.x ^= k.x;
        e.y ^= k.y;

        m = sbox(e);
        m = perm(m);

        return m;
    }

    __device__ static uint64_t final_permute(uint64_t m)
    {
        static constexpr uint8_t ptable[64] = {
            39, 7, 47, 15, 55, 23, 63, 31, //
            38, 6, 46, 14, 54, 22, 62, 30, //
            37, 5, 45, 13, 53, 21, 61, 29, //
            36, 4, 44, 12, 52, 20, 60, 28, //
            35, 3, 43, 11, 51, 19, 59, 27, //
            34, 2, 42, 10, 50, 18, 58, 26, //
            33, 1, 41, 9,  49, 17, 57, 25, //
            32, 0, 40, 8,  48, 16, 56, 24,
        };

        return permute(m, ptable, 64);
    }

    __device__ static uint64_t enc(uint64_t m, uint64_t k, int rounds [[maybe_unused]])
    {
        k = reduce_key(k);
        m = initial_permute(m);
        uint2 mv{(uint32_t)(m >> 32), (uint32_t)m};
        uint2 kv{(uint32_t)(k >> 28), (uint32_t)k & 0x0FFFFFFF};

        for (int i = 0; i < rounds; ++i)
        {
            static constexpr uint16_t shift = 0b0111111011111100;

            uint32_t ml_old = mv.x;
            mv.x = mv.y;
            kv = rotate_key(kv, (uint8_t)((shift >> i & 1) + 1));
            mv.y = ml_old ^ round(mv.y, compress_key(kv));
        }
        m = (uint64_t)mv.y << 32 | mv.x; // reverse order
        m = final_permute(m);

        return m;
    }

    __device__ static uint64_t dec(uint64_t m, uint64_t k, int rounds)
    {
        k = reduce_key(k);

        m = initial_permute(m);
        uint32_t ml = m >> 32;
        uint32_t mr = (uint32_t)m;
        uint2 sk[ROUNDS];

        build_round_keys(k, sk);
        for (int i = 0; i < rounds; ++i)
        {
            uint32_t ml_old = ml;
            ml = mr;
            mr = ml_old ^ round(mr, sk[(ROUNDS - 1) - i]);
        }

        m = (uint64_t)mr << 32 | ml; // reverse order
        m = final_permute(m);

        return m;
    }

    __device__ static uint64_t enc_short(uint64_t m, uint64_t k)
    {
        uint2 mv{(uint32_t)(m >> 32), (uint32_t)m};
        uint2 kv{(uint32_t)(k >> 28), (uint32_t)k & 0x0FFFFFFF};

        for (size_t i = 0; i < ROUNDS; ++i)
        {
            static constexpr uint16_t shift = 0b0111111011111100;

            uint32_t ml_old = mv.x;
            mv.x = mv.y;
            kv = rotate_key(kv, (uint8_t)((shift >> i & 1) + 1));
            mv.y = ml_old ^ round(mv.y, compress_key(kv));
        }
        return (uint64_t)mv.y << 32 | mv.x;
    }

    __global__ static void enc_short_ker(uint64_t plain, size_t crypt, uint64_t off, uint64_t *dk)
    {
        uint64_t key = blockIdx.x * blockDim.x + threadIdx.x + off;

        if (enc_short(plain, key) == crypt)
            *dk = key;
    }
} // namespace cu::crypto::des::device

namespace cu::crypto::des
{
    static constexpr size_t BLKSZ = MAX_BLKSZ / 2;

    uint64_t crack(uint64_t m, uint64_t c, uint64_t off, uint64_t limit)
    {
        static constexpr uint32_t GRDSZ = 1 << 14;

        m = ::crypto::des::initial_permute2(m);
        c = ::crypto::des::initial_permute2(c);

        uint64_t k = 0, *dk;

        cudaMalloc(&dk, sizeof(*dk));
        cudaMemcpy(dk, &k, sizeof(k), cudaMemcpyHostToDevice);

        for (; off < limit && !k; off += GRDSZ * BLKSZ)
        {
            device::enc_short_ker<<<GRDSZ, BLKSZ>>>(m, c, off, dk);
            cudaMemcpyAsync(&k, dk, sizeof(k), cudaMemcpyDeviceToHost);
        }

        cudaDeviceSynchronize();
        cudaFree(dk);

        return ::crypto::des::augment_key2(k);
    }

    uint64_t test_enc(uint64_t msg, uint64_t key, int rounds)
    {
        return crypto::test_enc<uint64_t, uint64_t, device::enc>(msg, key, rounds);
    }

    uint64_t test_dec(uint64_t msg, uint64_t key, int rounds)
    {
        return crypto::test_enc<uint64_t, uint64_t, device::dec>(msg, key, rounds);
    }

} // namespace cu::crypto::des
