#include "rc4/rc4.hpp"

#include <cstdint>

namespace crypto::rc4
{
    static constexpr size_t S_SIZE = 256;

    void enc(void *cyp, const void *msg, size_t msg_sz, const void *key, size_t key_sz, size_t off)
    {
        uint8_t *c = (uint8_t *)cyp;
        const uint8_t *m = (const uint8_t *)msg;
        const uint8_t *k = (const uint8_t *)key;
        uint8_t s[S_SIZE];
        uint8_t i = 0, j = 0, t = 0;

        do
            s[i] = i;
        while (++i);

        do
        {
            j += s[i] + k[i % key_sz];
            t = s[i];
            s[i] = s[j];
            s[j] = t;
        } while (++i);

        j = 0;
        for (size_t l = 0; l < off; ++l)
        {
            j += s[++i];
            t = s[i];
            s[i] = s[j];
            s[j] = t;
            t += s[i];
        }
        for (size_t l = 0; l < msg_sz; ++l)
        {
            j += s[++i];
            t = s[i];
            s[i] = s[j];
            s[j] = t;
            t += s[i];
            c[l] = s[t] ^ m[l];
        }
    }

    void dec(void *msg, const void *cyp, size_t cyp_sz, const void *key, size_t key_sz, size_t off)
    {
        enc(msg, cyp, cyp_sz, key, key_sz, off);
    }

} // namespace crypto::rc4
