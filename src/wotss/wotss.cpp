#include "wotss/wotss.hpp"

#include <random>
#include <cstring>

namespace crypto::wotss
{
    static constexpr size_t N = (size_t)std::numeric_limits<uint8_t>::max() + 1;

    void keygen(void *sk, void *pk)
    {
        using base_t = std::random_device::result_type;

        std::random_device rng;
        base_t *bsk = (base_t *)sk;

        for (size_t i = 0, n = SK_SIZE / sizeof(base_t); i < n; ++i)
            bsk[i] = rng();

        for (size_t i = 0, n = SK_SIZE / sha256::DIGEST_SIZE; i < n; ++i)
        {
            uint8_t dig[sha256::DIGEST_SIZE]{};
            uint8_t blk[sha256::BLOCK_SIZE]{};

            memcpy(blk, (const uint8_t *)sk + i * sha256::DIGEST_SIZE, sha256::DIGEST_SIZE);
            for (size_t j = 0; j < N; ++j)
            {
                sha256::hash_oneblock(dig, blk, sha256::BLOCK_SIZE);
                memcpy(blk, dig, sha256::DIGEST_SIZE);
            }
            memcpy((uint8_t *)pk + i * sha256::DIGEST_SIZE, dig, sha256::DIGEST_SIZE);
        }
    }

    void sign(void *sig, const void *sk, const void *msg, size_t msg_sz)
    {
        uint8_t msg_dig[sha256::DIGEST_SIZE]{};

        sha256::hash(msg_dig, msg, msg_sz);

        for (size_t i = 0; i < sha256::DIGEST_SIZE; ++i)
        {
            uint8_t dig[sha256::DIGEST_SIZE]{};
            uint8_t blk[sha256::BLOCK_SIZE]{};

            memcpy(blk, (const uint8_t *)sk + i * sha256::DIGEST_SIZE, sha256::DIGEST_SIZE);
            for (size_t j = 0; j < msg_dig[i]; ++j)
            {
                sha256::hash_oneblock(dig, blk, sha256::BLOCK_SIZE);
                memcpy(blk, dig, sha256::DIGEST_SIZE);
            }

            memcpy((uint8_t *)sig + i * sha256::DIGEST_SIZE, dig, sha256::DIGEST_SIZE);
        }
    }

    bool check(const void *sig, const void *pk, const void *msg, size_t msg_sz)
    {
        uint8_t msg_dig[sha256::DIGEST_SIZE]{};

        sha256::hash(msg_dig, msg, msg_sz);

        for (size_t i = 0; i < sha256::DIGEST_SIZE; ++i)
        {
            uint8_t dig[sha256::DIGEST_SIZE]{};
            uint8_t blk[sha256::BLOCK_SIZE]{};

            memcpy(blk, (const uint8_t *)sig + i * sha256::DIGEST_SIZE, sha256::DIGEST_SIZE);
            for (size_t j = 0; j < N - msg_dig[i]; ++j)
            {
                sha256::hash_oneblock(dig, blk, sha256::BLOCK_SIZE);
                memcpy(blk, dig, sha256::DIGEST_SIZE);
            }

            if (memcmp((const uint8_t *)pk + i * sha256::DIGEST_SIZE, dig, sha256::DIGEST_SIZE))
                return false;
        }

        return true;
    }
} // namespace crypto::wotss
