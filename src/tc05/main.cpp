#include "tc05.hpp"
#include <iostream>
#include <random>

int main()
{
    std::mt19937_64 rng{std::random_device{}()};
    uint64_t key = rng();
    uint32_t diff = 5;
    uint16_t dtable[16][16] = {0};

    std::cout << std::hex << std::uppercase;

    for (uint16_t m0 = 0; m0 < 16; ++m0)
        for (uint16_t m1 = 0; m1 < 16; ++m1)
        {
            uint16_t c0 = tc05::sbox(m0);
            uint16_t c1 = tc05::sbox(m1);
            ++dtable[m0 ^ m1][c0 ^ c1];
        }
    for (size_t i = 0; i < 16; ++i)
    {
        std::cout << i << ": ";
        for (auto &&j : dtable[i])
            std::cout << j << ' ';
        std::cout << '\n';
    }
    for (int i = 0; i < 100; ++i)
    {
        uint32_t m0 = rng();
        uint32_t m1 = m0 ^ diff;
        uint32_t c0 = tc05::enc(m0, key, 2);
        uint32_t c1 = tc05::enc(m1, key, 2);
        uint32_t d_in = m0 ^ m1;
        uint32_t d_out = c0 ^ c1;


        //        std::cout << m0 << ", " << c0 << "; " << m1 << ", " << c1 << "; " << d_in << ", " << d_out
        //                  << '\n';
        std::cout << d_out << ' ';
    }
    std::cout << '\n';

    return 0;
}
