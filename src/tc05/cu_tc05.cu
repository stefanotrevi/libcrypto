#include "cu_crypto.cuh"
#include "intrinsics.h"
#include "tc05/cu_tc05.hpp"

namespace cu::crypto::tc05
{
    namespace device
    {
        __device__ static inline uint16_t sigma(uint16_t word)
        {
            uint16_t new_word = 0;

            new_word |= (word & 0xC00C) >> 1;
            new_word |= (word & 0x0020) >> 2;
            new_word |= (word & 0x0010) >> 4;
            new_word |= (word & 0x0C00) >> 5;
            new_word |= (word & 0x2000) >> 6;
            new_word |= (word & 0x1000) >> 8;

            new_word |= (word & 0x00C0) << 3;
            new_word |= (word & 0x0100) << 4;
            new_word |= (word & 0x0200) << 6;
            new_word |= (word & 0x0001) << 8;
            new_word |= (word & 0x0002) << 10;

            return new_word;
        }

        __device__ static inline uint16_t sbox(uint16_t m)
        {
            static constexpr uint8_t sbox[16] = {0xE, 0xB, 0x4, 0x6, 0xA, 0xD, 0x7, 0x0,
                                                 0x3, 0x8, 0xF, 0xC, 0x5, 0x9, 0x1, 0x2};
            uint16_t p = sbox[m & 0xF];

            p |= (uint16_t)sbox[m >> 4 & 0xF] << 4;
            p |= (uint16_t)sbox[m >> 8 & 0xF] << 8;
            p |= (uint16_t)sbox[m >> 12] << 12;

            return p;
        }

        __device__ static inline void next_key(uint16_t keys[4], uint32_t i)
        {
            keys[i & 3] = (uint16_t)(keys[i & 3] ^ keys[(i - 1) & 3] ^ sigma(keys[(i - 2) & 3]) ^
                                     0xC);
        }

        template<uint32_t rounds>
        __device__ static inline uint32_t enc(uint32_t m, uint64_t k)
        {
            uint16_t sk[4] = {k >> 48, k >> 32, k >> 16, k};
            uint16_t l = m >> 16;
            uint16_t r = m;

#pragma unroll(rounds)
            for (uint32_t i = 0; i < rounds; ++i)
            {
                uint16_t t = l;
                l = sigma(sbox(l)) ^ r ^ sk[i & 3];
                r = l;

                sk[i & 3] ^= sk[(i - 1) & 3];
                sk[i & 3] ^= sigma(sk[(i - 2) & 3]);
                sk[i & 3] ^= 0xC;
            }

            return (uint32_t)l << 16 | r;
        }

        template<uint32_t rounds>
        __device__ static inline uint32_t dec(uint32_t m, uint64_t k)
        {
            uint16_t sk[4];
            uint16_t l = m >> 16;
            uint16_t r = m;

            sk[(rounds - 1) & 3] = k;
            sk[(rounds - 2) & 3] = k >> 16;
            sk[(rounds - 3) & 3] = k >> 32;
            sk[(rounds - 4) & 3] = k >> 48;


#pragma unroll(rounds)
            for (uint32_t i = rounds - 1; i != ~0U; --i)
            {
                uint16_t t = r;
                r = sigma(sbox(r)) ^ l ^ sk[i & 3];
                l = t;

                sk[i & 3] ^= sk[(i - 1) & 3];
                sk[i & 3] ^= sigma(sk[(i - 2) & 3]);
                sk[i & 3] ^= 0xC;
            }

            return (uint32_t)l << 16 | r;
        }

        template<uint32_t rounds>
        __global__ void test_enc(uint32_t msg, uint64_t key, uint32_t *cip)
        {
            *cip = enc<rounds>(msg, key);
        }

        template<uint32_t rounds>
        __global__ void test_dec(uint32_t cip, uint64_t key, uint32_t *msg)
        {
            *msg = enc<rounds>(cip, key);
        }

        template<uint32_t rounds>
        __global__ void crack_enc(uint32_t msg, uint32_t cip, uint16_t skn, uint64_t *d_key,
                                  uint64_t off)
        {
            // concatenation of the 4 last subkeys, must be de-scheduled
            uint64_t key = (off + blockIdx.x * blockDim.x + threadIdx.x) << 16 | skn;

            if (dec<rounds>(cip, key) == msg)
                *d_key = key;
        }
    } // namespace device

    template<uint32_t rounds>
    uint32_t test_enc(uint32_t msg, uint64_t key)
    {
        uint32_t cip;
        uint32_t *d_cip;

        cudaMalloc(&d_cip, sizeof(*d_cip));
        device::test_enc<rounds><<<1, 1>>>(msg, key, d_cip);
        cudaDeviceSynchronize();
        cudaMemcpy(&cip, d_cip, sizeof(cip), cudaMemcpyDeviceToHost);
        cudaFree(d_cip);

        return cip;
    }

    template<uint32_t rounds>
    uint64_t crack(uint32_t msg, uint32_t cip, uint16_t skn, size_t watch)
    {
        using clk = std::chrono::high_resolution_clock;

        static constexpr uint32_t GRDSZ = 1 << 16;
        static constexpr uint32_t BLKSZ = MAX_BLKSZ;

        if (test_enc<rounds>(msg, 0) == cyp)
            return 0;

        uint64_t key = 0;
        uint64_t *d_key;
        cudaStream_t copy_stream;

        cudaStreamCreate(&copy_stream);
        cudaMalloc(&d_key, sizeof(*d_key));
        cudaMemcpy(d_key, &key, sizeof(key), cudaMemcpyHostToDevice);

        std::cout << std::left;

        auto start = clk::now();
        for (size_t i = 0, off = 0; !key; ++i, off += GRDSZ * BLKSZ)
        {
            device::crack_enc<rounds><<<GRDSZ, BLKSZ>>>(msg, cip, skn, d_key, off);
            cudaMemcpyAsync(&key, d_key, sizeof(key), cudaMemcpyDeviceToHost, 0);
            if (watch && !(i % watch))
            {
                std::cout
                    << std::setw(20) << off << '\t'
                    << std::chrono::duration_cast<std::chrono::seconds>(clk::now() - start).count()
                    << " sec. \r";
                std::cout.flush();
            }
        }
        if (watch)
            std::cout << '\n';

        std::cout << std::right;
        cudaDeviceSynchronize();
        cudaStreamDestroy(copy_stream);
        cudaFree(d_key);

        return key;
    }
} // namespace cu::crypto::tc05
