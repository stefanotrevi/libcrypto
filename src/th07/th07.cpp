#include "th07/th07.hpp"
#include "tc07/tc07.hpp"
#include <cstring>

namespace crypto::th07
{
    void hash(void *digest, const void *message, size_t sz)
    {
        if (!sz)
        {
            *(uint64_t *)digest = tc07::enc2(0, 0);
            return;
        }

        uint64_t *msg = (uint64_t *)message;
        uint64_t dig = 0;
        size_t sz_div = sz / sizeof(uint64_t);
        size_t sz_rem = sz % sizeof(uint64_t);
        uint64_t pad = 0;

        for (size_t i = 0; i < sz_div; ++i)
            dig = tc07::enc2(msg[i], dig);

        if (sz_rem)
        {
            memcpy(&pad, (const char *)message + sz - sz_rem, sz_rem);
            dig = tc07::enc2(pad, dig);
        }

        *(uint64_t *)digest = dig;
    }

    void hash2(void *digest, const void *message, size_t sz)
    {
        uint64_t dig = 0;

        if (!sz)
        {
            *(uint64_t *)digest = tc07::enc2(dig, 0);
            return;
        }

        uint64_t *msg = (uint64_t *)message;
        size_t sz_div = sz / sizeof(uint64_t);
        size_t sz_rem = sz % sizeof(uint64_t);
        uint64_t pad = 0;

        for (size_t i = 0; i < sz_div; ++i)
            dig = tc07::enc2(msg[i] ^ dig, 0);

        if (sz_rem)
        {
            memcpy(&pad, (const char *)message + sz - sz_rem, sz_rem);
            dig = tc07::enc2(pad ^ dig, 0);
        }

        *(uint64_t *)digest = dig;
    }
} // namespace th07
