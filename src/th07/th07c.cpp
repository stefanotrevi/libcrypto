#include "th07/th07c.hpp"
#include "tc07/tc07c.hpp"
#include <cstring>

namespace crypto::th07c
{
    void hash(void *digest, const void *message, size_t sz)
    {
        uint64_t dig = DIGEST_INIT;

        if (!sz)
        {
            *(uint64_t *)digest = tc07c::enc(dig, 0);
            return;
        }

        uint64_t *msg = (uint64_t *)message;
        size_t sz_div = sz / sizeof(uint64_t);
        size_t sz_rem = sz % sizeof(uint64_t);
        uint64_t pad = 0;

        for (size_t i = 0; i < sz_div; ++i)
            dig = tc07c::enc(msg[i], dig);

        if (sz_rem)
        {
            memcpy(&pad, (const char *)message + sz - sz_rem, sz_rem);
            dig = tc07c::enc(pad, dig);
        }

        *(uint64_t *)digest = dig;
    }
} // namespace crypto::th07c
