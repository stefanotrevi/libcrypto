#include "cu_crypto.cuh"
#include "tc09/cu_tc09.hpp"

namespace cu::crypto::tc09::device
{
    static __device__ uint16_t sbox(uint16_t m)
    {
        static constexpr uint64_t box = 0x9CD807EB3F16245A;
        uint16_t p = 0;

        for (uint32_t i = 0; i < 4; ++i)
            p |= ((uint16_t)(box >> (m >> i * 4 & 0xF) * 4) & 0xF) << i * 4;

        return p;
    }

    static __device__ uint16_t sbox_i(uint16_t m)
    {
        static constexpr uint64_t box = 0x69DE80FCA412735B;
        uint16_t p = 0;
        for (uint32_t i = 0; i < 4; ++i)
            p |= ((uint16_t)(box >> (m >> i * 4 & 0xF) * 4) & 0xF) << i * 4;

        return p;
    }

    template<int fix_rounds = 0> static __device__ uint64_t enc(uint64_t m, uint64_t k, int rounds)
    {
        uint16_t *p = (uint16_t *)&m;
        uint16_t *rk = (uint16_t *)&k;
        uint16_t t[4];

        for (int i = 0, lim = fix_rounds ? fix_rounds : rounds; i < lim; ++i)
        {
            // AddKey
            p[0] ^= rk[0];
            p[1] ^= rk[1];
            p[2] ^= rk[2];
            p[3] ^= rk[3];

            // Sbox
            p[0] = sbox(p[0]);
            p[1] = sbox(p[1]);
            p[2] = sbox(p[2]);
            p[3] = sbox(p[3]);

            // ShiftRows
            p[0] = rotl(p[0], 12);
            p[1] = rotl(p[1], 8);
            p[2] = rotl(p[2], 4);

            // MixColumns
            t[0] = p[0];
            t[1] = p[1];
            t[2] = p[2];
            t[3] = p[3];

            p[0] = t[1] ^ t[0];
            p[1] = t[3] ^ t[1] ^ t[0];
            p[2] = t[2] ^ t[1];
            p[3] = t[3] ^ t[2];
        }

        return m;
    }

    __device__ uint64_t pdec(uint64_t m)
    {
        uint16_t *p = (uint16_t *)&m;

        // Invert MixColumns
        p[1] ^= p[3] ^ p[2] ^ p[0];
        p[0] ^= p[1];
        p[2] ^= p[1];
        p[3] ^= p[2];

        // Invert ShiftRows
        p[2] = rotr(p[2], 4);
        p[1] = rotr(p[1], 8);
        p[0] = rotr(p[0], 12);

        // Invert Sbox
        p[3] = sbox_i(p[3]);
        p[2] = sbox_i(p[2]);
        p[1] = sbox_i(p[1]);
        p[0] = sbox_i(p[0]);

        return m;
    }

    static __global__ void compute_freq_ker(uint16_t *freq, uint64_t c0, uint64_t c1, uint64_t d_in,
                                            uint64_t last, uint64_t msk, uint64_t prev_key)
    {
        uint32_t i = blockIdx.x * blockDim.x + threadIdx.x;
        uint64_t key = pdep_4((uint64_t)i, msk) | prev_key;
        uint64_t d = pdec(c0 ^ key) ^ pdec(c1 ^ key);

        if (i < last && d == d_in)
            ++freq[i];
    }
} // namespace cu::crypto::tc09::device

namespace cu::crypto::tc09
{
    CU_DEFINE_TESTENC(test_enc, device::enc<0>)

    CU_DEFINE_CRACKENC(crack, device::enc<0>)

    void compute_freq(uint16_t *d_freq, uint64_t c0, uint64_t c1, uint64_t d_in, uint64_t last,
                      uint64_t msk, uint64_t prev_key)
    {
        uint32_t blksz = MAX_BLKSZ;
        uint32_t grdsz = (last / blksz) + 1;

        device::compute_freq_ker<<<grdsz, blksz>>>(d_freq, c0, c1, d_in, last, msk, prev_key);
        cudaDeviceSynchronize();
    }

} // namespace cu::crypto::tc09
