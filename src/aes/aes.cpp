#include "aes/aes.hpp"
#include "avxlib.hpp"
#include <bit>
#include <cstring>
#include <utility>

namespace crypto::aes
{
    struct Sbox
    {
        uint8_t data[256];
    };

    class Poly8
    {
    private:
        uint8_t p;

    public:
        static constexpr uint8_t R = 0x1B;

        Poly8() = default;
        constexpr Poly8(uint8_t p) : p{p} {}

        constexpr operator bool() const { return p; }
        constexpr operator uint8_t() const { return p; }

        auto operator<=>(const Poly8 &other) const = default;

        constexpr Poly8 operator+=(Poly8 other) { return p ^= other.p; }
        constexpr Poly8 operator+(Poly8 other) const { return Poly8{*this} += other; }
        constexpr Poly8 operator-=(Poly8 other) { return *this += other; }
        constexpr Poly8 operator-(Poly8 other) const { return *this + other; }

        constexpr Poly8 operator*=(Poly8 other)
        {
            uint8_t z = p; // x^0

            p = p * (other.p & 1);

            // shift and reduce if overflow occours, then add if coefficient is 1
            while (other.p)
            {
                z = (z << 1) ^ (R * (z >= 128));
                other.p >>= 1;
                p ^= z * (other.p & 1);
            }

            return *this;
        }
        constexpr Poly8 operator*(Poly8 other) const { return Poly8{*this} *= other; }

        constexpr std::pair<Poly8, Poly8> div_rem(Poly8 other)
        {
            Poly8 z{};

            while (p >= other.p)
            {
                // find the distance between the highest degree in the divisor and in the dividend
                uint8_t t = 1U << (std::countl_zero(other.p) - std::countl_zero(p));

                z.p |= t;
                *this -= Poly8{t} * other;
            }

            return {z, *this};
        }
        // *this will store the remainder, we have to swap it!
        constexpr Poly8 operator/=(Poly8 other) { return *this = div_rem(other).first; }
        constexpr Poly8 operator/(Poly8 other) const { return Poly8{*this} /= other; }
        constexpr Poly8 operator%=(Poly8 other) { return div_rem(other).second; }
        constexpr Poly8 operator%(Poly8 other) const { return Poly8{*this} %= other; }

        constexpr Poly8 operator~() const
        {
            // we find the multiplicative inverse using the extended GCD algorithm
            if (p == 0)
                return Poly8{};

            // explicit first division to avoid overflow
            uint8_t t1 = (uint8_t)(std::countl_zero(p) + 1);
            Poly8 q{(uint8_t)(R ^ (uint8_t)(p << t1))};

            q /= *this;
            q -= Poly8{(uint8_t)(1U << t1)};

            Poly8 r{*this};
            Poly8 new_r{q * r};
            Poly8 t{1};
            Poly8 new_t{q};


            while (new_r)
            {
                Poly8 temp;

                q = r / new_r;

                temp = new_r;
                new_r *= q;
                new_r -= r;
                r = temp;

                temp = new_t;
                new_t *= q;
                new_t -= t;
                t = temp;
            }

            return t;
        }

        /*
        friend std::ostream &operator<<(std::ostream &os, Poly8 x)
        {
            static constexpr size_t BITS = 8 * sizeof(x.p);

            bool first = true;
            os << '(';

            for (int i = BITS - 1; i > 0; --i)
            {
                if (x.p & (1 << (BITS - 1)))
                {
                    if (first)
                        first = false;
                    else
                        os << " + ";
                    if (i > 1)
                        os << "x^" << i;
                    else
                        os << 'x';
                }
                x.p <<= 1;
            }
            if (x.p)
                if (first)
                    os << '1';
                else
                    os << " + 1";
            else if (first)
                os << '0';


            return os << ')';
        }*/
    };

    constexpr Sbox init_sbox()
    {
        Sbox box;

        for (size_t i = 0; i < 256; ++i)
        {
            uint8_t n = ~Poly8{(uint8_t)i};
            /*
            //uint64_t r = 0b11110001'11100011'11000111'10001111'00011111'00111110'01111100'11111000;
            //uint64_t r = 0b11111000'01111100'00111110'00011111'10001111'11000111'11100011'11110001;
            uint64_t r = 0b10001111'11000111'11100011'11110001'11111000'01111100'00111110'00011111;
            
            uint8_t c = 0b0110'0011;

            n = n << 4 | n >> 4;
            n = n << 2 | n >> 2;
            n = n << 1 | n >> 1;

            uint64_t x = n;
            x |= x << 8;
            x |= x << 16;
            x |= x << 32;
            x &= r;
            x |= (std::popcount(x & (0xFFULL << 0)) & 1) << 7;
            x |= (std::popcount(x & (0xFFULL << 8)) & 1) << 6;
            x |= (std::popcount(x & (0xFFULL << 16)) & 1) << 5;
            x |= (std::popcount(x & (0xFFULL << 24)) & 1) << 4;
            x |= (std::popcount(x & (0xFFULL << 32)) & 1) << 3;
            x |= (std::popcount(x & (0xFFULL << 40)) & 1) << 2;
            x |= (std::popcount(x & (0xFFULL << 48)) & 1) << 1;
            x |= (std::popcount(x & (0xFFULL << 56)) & 1) << 0;
            */
            box.data[i] = n;
            box.data[i] ^= std::rotl(n, 1);
            box.data[i] ^= std::rotl(n, 2);
            box.data[i] ^= std::rotl(n, 3);
            box.data[i] ^= std::rotl(n, 4);
            box.data[i] ^= 0x63;
        }

        return box;
    }

    static inline void add_key(uint8_t *m, const uint32_t *rk)
    {
        uint8_t *k = (uint8_t *)rk;

        for (size_t i = 0; i < 4; ++i)
            for (size_t j = 0; j < 4; ++j)
                m[i * 4 + j] ^= k[i * 4 + 3 - j];
    }

    inline uint8_t sbox(uint8_t x)
    {
        static constexpr Sbox box{init_sbox()};

        return box.data[x];
    }

    static inline void sbox(uint8_t *x)
    {
        for (size_t i = 0; i < 16; ++i)
            x[i] = sbox(x[i]);
    }

    static inline void shift_rows(uint8_t *x)
    {
        uint8_t t;

        t = x[1];
        x[1] = x[5];
        x[5] = x[9];
        x[9] = x[13];
        x[13] = t;

        t = x[2];
        x[2] = x[10];
        x[10] = t;
        t = x[6];
        x[6] = x[14];
        x[14] = t;

        t = x[15];
        x[15] = x[11];
        x[11] = x[7];
        x[7] = x[3];
        x[3] = t;
    }

    static inline void mix_cols(uint8_t *x)
    {
        // There is likely a much faster way to do this, provided just for reference
        static constexpr Poly8 M[16] = {2, 3, 1, 1, //
                                        1, 2, 3, 1, //
                                        1, 1, 2, 3, //
                                        3, 1, 1, 2};
        Poly8 r[16] = {0};

        for (size_t i = 0; i < 4; ++i)
            for (size_t j = 0; j < 4; ++j)
                for (size_t k = 0; k < 4; ++k)
                    r[i * 4 + j] += M[j * 4 + k] * Poly8{x[i * 4 + k]};

        memcpy(x, r, 16);
    }

    static inline void expand(uint32_t *rk, size_t i)
    {
        static constexpr uint32_t C[10] = {
            0x0100'0000, 0x0200'0000, 0x0400'0000, 0x0800'0000, 0x1000'0000,
            0x2000'0000, 0x4000'0000, 0x8000'0000, 0x1B00'0000, 0x3600'0000,
        };

        uint32_t r = std::rotl(rk[3], 8);

        ((uint8_t *)&r)[0] = sbox(((uint8_t *)&r)[0]);
        ((uint8_t *)&r)[1] = sbox(((uint8_t *)&r)[1]);
        ((uint8_t *)&r)[2] = sbox(((uint8_t *)&r)[2]);
        ((uint8_t *)&r)[3] = sbox(((uint8_t *)&r)[3]);
        rk[0] ^= r;
        rk[0] ^= C[i];
        rk[1] ^= rk[0];
        rk[2] ^= rk[1];
        rk[3] ^= rk[2];
    }

    void enc(void *cip, const void *msg, const void *key)
    {
        uint8_t *c = (uint8_t *)cip;
        uint8_t *m = (uint8_t *)msg;
        uint32_t *k = (uint32_t *)key;

        uint32_t rk[4] = {(uint32_t)_bswap(k[0]), (uint32_t)_bswap(k[1]), (uint32_t)_bswap(k[2]),
                          (uint32_t)_bswap(k[3])};

        memcpy(c, m, 16);
        add_key(c, rk);

        for (size_t i = 0; i < 9; ++i)
        {
            expand(rk, i);
            sbox(c);
            shift_rows(c);
            mix_cols(c);
            add_key(c, rk);
        }

        expand(rk, 9);
        sbox(c);
        shift_rows(c);
        add_key(c, rk);
    }

    template<int c>
    static inline __m128i expand_key2(__m128i k)
    {
        __m128i temp2 = _mm_aeskeygenassist_si128(k, c);
        __m128i temp3;

        temp2 = _mm_shuffle_epi32(temp2, 0xff);

        temp3 = _mm_slli_si128(k, 0x4);
        k = _mm_xor_si128(k, temp3);

        temp3 = _mm_slli_si128(temp3, 0x4);
        k = _mm_xor_si128(k, temp3);

        temp3 = _mm_slli_si128(temp3, 0x4);
        k = _mm_xor_si128(k, temp3);

        k = _mm_xor_si128(k, temp2);

        return k;
    }

    void enc2(void *cip, const void *msg, const void *key)
    {
        __m128i m = _mm_loadu_si128((__m128i *)msg);
        __m128i k = _mm_loadu_si128((__m128i *)key);

        m = _mm_xor_si128(m, k);

        k = expand_key2<0x01>(k);
        m = _mm_aesenc_si128(m, k);

        k = expand_key2<0x02>(k);
        m = _mm_aesenc_si128(m, k);

        k = expand_key2<0x04>(k);
        m = _mm_aesenc_si128(m, k);


        k = expand_key2<0x08>(k);
        m = _mm_aesenc_si128(m, k);

        k = expand_key2<0x10>(k);
        m = _mm_aesenc_si128(m, k);

        k = expand_key2<0x20>(k);
        m = _mm_aesenc_si128(m, k);

        k = expand_key2<0x40>(k);
        m = _mm_aesenc_si128(m, k);

        k = expand_key2<0x80>(k);
        m = _mm_aesenc_si128(m, k);

        k = expand_key2<0x1B>(k);
        m = _mm_aesenc_si128(m, k);

        k = expand_key2<0x36>(k);
        m = _mm_aesenclast_si128(m, k);

        _mm_storeu_si128((__m128i *)cip, m);
    }

    static inline void precompute_keys(__m128i *keys, const void *key)
    {
        __m128i k = _mm_loadu_si128((const __m128i *)key);

        _mm_storeu_si128(keys + 0, k);

        k = expand_key2<0x01>(k);
        _mm_storeu_si128(keys + 1, k);

        k = expand_key2<0x02>(k);
        _mm_storeu_si128(keys + 2, k);

        k = expand_key2<0x04>(k);
        _mm_storeu_si128(keys + 3, k);

        k = expand_key2<0x08>(k);
        _mm_storeu_si128(keys + 4, k);

        k = expand_key2<0x10>(k);
        _mm_storeu_si128(keys + 5, k);

        k = expand_key2<0x20>(k);
        _mm_storeu_si128(keys + 6, k);

        k = expand_key2<0x40>(k);
        _mm_storeu_si128(keys + 7, k);

        k = expand_key2<0x80>(k);
        _mm_storeu_si128(keys + 8, k);
        k = expand_key2<0x1B>(k);
        _mm_storeu_si128(keys + 9, k);

        k = expand_key2<0x36>(k);
        _mm_storeu_si128(keys + 10, k);
    }

    static inline __m128i enc3_inner(__m128i m, const __m128i *ks)
    {
        m = _mm_xor_si128(m, ks[0]);
        m = _mm_aesenc_si128(m, ks[1]);
        m = _mm_aesenc_si128(m, ks[2]);
        m = _mm_aesenc_si128(m, ks[3]);
        m = _mm_aesenc_si128(m, ks[4]);
        m = _mm_aesenc_si128(m, ks[5]);
        m = _mm_aesenc_si128(m, ks[6]);
        m = _mm_aesenc_si128(m, ks[7]);
        m = _mm_aesenc_si128(m, ks[8]);
        m = _mm_aesenc_si128(m, ks[9]);
        m = _mm_aesenclast_si128(m, ks[10]);

        return m;
    }

    void enc3(void *cip, const void *msg, const void *key)
    {
        __m128i m = _mm_loadu_si128((__m128i *)msg);
        __m128i k[11];

        precompute_keys(k, key);
        m = enc3_inner(m, k);

        _mm_storeu_si128((__m128i *)cip, m);
    }

    static inline void precompute_keys256(__m256i *keys, const void *key)
    {
        __m128i k = _mm_loadu_si128((const __m128i *)key);
        __m256i ek = _mm256_broadcastsi128_si256(k);

        _mm256_storeu_si256(keys + 0, ek);

        k = expand_key2<0x1>(k);
        ek = _mm256_broadcastsi128_si256(k);
        _mm256_storeu_si256(keys + 1, ek);

        k = expand_key2<0x02>(k);
        ek = _mm256_broadcastsi128_si256(k);
        _mm256_storeu_si256(keys + 2, ek);

        k = expand_key2<0x04>(k);
        ek = _mm256_broadcastsi128_si256(k);
        _mm256_storeu_si256(keys + 3, ek);

        k = expand_key2<0x08>(k);
        ek = _mm256_broadcastsi128_si256(k);
        _mm256_storeu_si256(keys + 4, ek);

        k = expand_key2<0x10>(k);
        ek = _mm256_broadcastsi128_si256(k);
        _mm256_storeu_si256(keys + 5, ek);

        k = expand_key2<0x20>(k);
        ek = _mm256_broadcastsi128_si256(k);
        _mm256_storeu_si256(keys + 6, ek);

        k = expand_key2<0x40>(k);
        ek = _mm256_broadcastsi128_si256(k);
        _mm256_storeu_si256(keys + 7, ek);

        k = expand_key2<0x80>(k);
        ek = _mm256_broadcastsi128_si256(k);
        _mm256_storeu_si256(keys + 8, ek);

        k = expand_key2<0x1B>(k);
        ek = _mm256_broadcastsi128_si256(k);
        _mm256_storeu_si256(keys + 9, ek);

        k = expand_key2<0x36>(k);
        ek = _mm256_broadcastsi128_si256(k);
        _mm256_storeu_si256(keys + 10, ek);
    }

    static inline __m256i enc3_inner256(__m256i m, const __m256i *ks)
    {
        m = _mm256_xor_si256(m, ks[0]);
        m = _mm256_aesenc_epi128(m, ks[1]);
        m = _mm256_aesenc_epi128(m, ks[2]);
        m = _mm256_aesenc_epi128(m, ks[3]);
        m = _mm256_aesenc_epi128(m, ks[4]);
        m = _mm256_aesenc_epi128(m, ks[5]);
        m = _mm256_aesenc_epi128(m, ks[6]);
        m = _mm256_aesenc_epi128(m, ks[7]);
        m = _mm256_aesenc_epi128(m, ks[8]);
        m = _mm256_aesenc_epi128(m, ks[9]);
        m = _mm256_aesenclast_epi128(m, ks[10]);

        return m;
    }

#ifdef __AVX512__
    static inline void precompute_keys512(__m512i *keys, const void *key)
    {
        __m128i k = _mm_loadu_si128((const __m128i *)key);
        __m512i ek = _mm512_broadcast_i64x2(k);

        _mm512_storeu_si512(keys + 0, ek);

        k = expand_key2<0x1>(k);
        ek = _mm512_broadcast_i64x2(k);
        _mm512_storeu_si512(keys + 1, ek);

        k = expand_key2<0x02>(k);
        ek = _mm512_broadcast_i64x2(k);
        _mm512_storeu_si512(keys + 2, ek);

        k = expand_key2<0x04>(k);
        ek = _mm512_broadcast_i64x2(k);
        _mm512_storeu_si512(keys + 3, ek);

        k = expand_key2<0x08>(k);
        ek = _mm512_broadcast_i64x2(k);
        _mm512_storeu_si512(keys + 4, ek);

        k = expand_key2<0x10>(k);
        ek = _mm512_broadcast_i64x2(k);
        _mm512_storeu_si512(keys + 5, ek);

        k = expand_key2<0x20>(k);
        ek = _mm512_broadcast_i64x2(k);
        _mm512_storeu_si512(keys + 6, ek);

        k = expand_key2<0x40>(k);
        ek = _mm512_broadcast_i64x2(k);
        _mm512_storeu_si512(keys + 7, ek);

        k = expand_key2<0x80>(k);
        ek = _mm512_broadcast_i64x2(k);
        _mm512_storeu_si512(keys + 8, ek);

        k = expand_key2<0x1B>(k);
        ek = _mm512_broadcast_i64x2(k);
        _mm512_storeu_si512(keys + 9, ek);

        k = expand_key2<0x36>(k);
        ek = _mm512_broadcast_i64x2(k);
        _mm512_storeu_si512(keys + 10, ek);
    }

    static inline __m512i enc3_inner512(__m512i m, const __m512i *ks)
    {
        m = _mm512_xor_si512(m, ks[0]);
        m = _mm512_aesenc_epi128(m, ks[1]);
        m = _mm512_aesenc_epi128(m, ks[2]);
        m = _mm512_aesenc_epi128(m, ks[3]);
        m = _mm512_aesenc_epi128(m, ks[4]);
        m = _mm512_aesenc_epi128(m, ks[5]);
        m = _mm512_aesenc_epi128(m, ks[6]);
        m = _mm512_aesenc_epi128(m, ks[7]);
        m = _mm512_aesenc_epi128(m, ks[8]);
        m = _mm512_aesenc_epi128(m, ks[9]);
        m = _mm512_aesenclast_epi128(m, ks[10]);

        return m;
    }
#endif

    void enc3_cbc(void *cip, const void *msg, size_t n, const void *key)
    {
        __m128i ks[11];
        __m128i iv = _mm_setzero_si128();
        size_t blocks = n / sizeof(__m128i);

        precompute_keys(ks, key);

        for (size_t i = 0; i < blocks; ++i)
        {
            __m128i m = _mm_loadu_si128((const __m128i *)msg + i);

            iv = _mm_xor_si128(m, iv);
            iv = enc3_inner(iv, ks);

            _mm_storeu_si128((__m128i *)cip + i, iv);
        }
    }

    void enc3_ctr(void *cip, const void *msg, size_t n, const void *key, const void *nn)
    {
        uint64_t nonce_high = ((uint64_t *)nn)[0];
        uint64_t nonce_low = ((uint64_t *)nn)[1];
        __m128i ks[11];
        __m128i nonce = _mm_set_epi64x(nonce_low, nonce_high);
        __m128i pp = _mm_set_epi64x(1, 0);
        size_t blocks = n / sizeof(__m128i);

        precompute_keys(ks, key);
        for (size_t i = 0; i < blocks; ++i)
        {
            __m128i c = enc3_inner(nonce, ks);
            __m128i m = _mm_loadu_si128((const __m128i *)msg + i);

            c = _mm_xor_si128(c, m);
            _mm_storeu_si128((__m128i *)cip + i, c);

            nonce = _mm_add_epi64(nonce, pp);
        }
    }

    void enc3_ctr256(void *cip, const void *msg, size_t n, const void *key, const void *nn)
    {
        __m256i ks[11];
        uint64_t nonce_high = ((uint64_t *)nn)[0];
        uint64_t nonce_low = ((uint64_t *)nn)[1];
        __m256i nonce = _mm256_set_epi64x(nonce_low + 1, nonce_high, //
                                         nonce_low + 0, nonce_high);
        __m256i pp = _mm256_set_epi64x(4, 0, 4, 0);
        size_t blocks = n / sizeof(__m256i);


        precompute_keys256(ks, key);

        for (size_t i = 0; i < blocks; ++i)
        {
            __m256i c = enc3_inner256(nonce, ks);


            __m256i m = _mm256_loadu_si256((const __m256i *)msg + i);
            c = _mm256_xor_si256(c, m);
            _mm256_storeu_si256((__m256i *)cip + i, c);

            nonce = _mm256_add_epi64(nonce, pp);
        }
    }

#ifdef __AVX512__
    void enc3_ctr512(void *cip, const void *msg, size_t n, const void *key, const void *nn)
    {
        __m512i ks[11];
        uint64_t nonce_high = ((uint64_t *)nn)[0];
        uint64_t nonce_low = ((uint64_t *)nn)[1];
        __m512i nonce = _mm512_set_epi64(nonce_low + 3, nonce_high, //
                                         nonce_low + 2, nonce_high, //
                                         nonce_low + 1, nonce_high, //
                                         nonce_low + 0, nonce_high);
        __m512i pp = _mm512_set_epi64(4, 0, 4, 0, 4, 0, 4, 0);
        size_t blocks = n / sizeof(__m512i);


        precompute_keys512(ks, key);

        for (size_t i = 0; i < blocks; ++i)
        {
            __m512i c = enc3_inner512(nonce, ks);


            __m512i m = _mm512_loadu_si512((const __m512i *)msg + i);
            c = _mm512_xor_si512(c, m);
            _mm512_storeu_si512((__m512i *)cip + i, c);

            nonce = _mm512_add_epi64(nonce, pp);
        }
    }
#endif

} // namespace crypto::aes
