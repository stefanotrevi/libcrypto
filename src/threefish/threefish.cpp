#include "threefish/threefish.hpp"
#include "intrinsics.h"

#include <cstdint>

namespace crypto::threefish
{
    void enc(void *cyp_void, const void *msg_void, const void *key_void, const void *tweak_void,
             unsigned rounds)
    {
        // Constants
        static constexpr unsigned N_W = 4;
        static constexpr uint64_t C_240 = 0x1BD11BDAA9FC1A22;
        static constexpr uint8_t ROT[8][2] = {{14, 16}, {52, 57}, {23, 40}, {5, 37},
                                              {25, 33}, {46, 12}, {58, 22}, {32, 32}};

        // Cast to word size
        uint64_t *cyp = (uint64_t *)cyp_void;
        const uint64_t *msg = (const uint64_t *)msg_void;
        const uint64_t *key = (const uint64_t *)key_void;
        const uint64_t *tweak = (const uint64_t *)tweak_void;

        // Initialize additional data
        uint64_t skey[N_W] = {key[0], key[1], key[2], key[3]};

        uint64_t t[3] = {tweak[0], tweak[1], tweak[0] ^ tweak[1]};

        uint64_t k[N_W + 1] = {key[0], key[1], key[2], key[3], C_240 ^ key[0] ^ key[1] ^ key[2] ^ key[3]};

        cyp[0] = msg[0];
        cyp[1] = msg[1];
        cyp[2] = msg[2];
        cyp[3] = msg[3];

        for (unsigned i = 0; i < rounds; ++i)
        {
            // AddKey
            if (i % 4 == 0)
            {
                unsigned s = i / 4;

                cyp[0] += skey[0];
                cyp[1] += skey[1];
                cyp[2] += skey[2];
                cyp[3] += skey[3];

                cyp[1] += t[(s + 0) % 3];
                cyp[2] += t[(s + 1) % 3];
                cyp[3] += s;
            }

            // MIX
            cyp[0] += cyp[1];
            cyp[1] = _rotl64(cyp[1], ROT[i % 8][0]) ^ cyp[0];

            cyp[2] += cyp[3];
            cyp[3] = _rotl64(cyp[3], ROT[i % 8][1]) ^ cyp[2];

            // Permute
            cyp[0] = cyp[0];
            cyp[1] = cyp[3];
            cyp[2] = cyp[2];
            cyp[3] = cyp[1];
        }

        // Final step
        cyp[0] += skey[0];
        cyp[1] += skey[1];
        cyp[2] += skey[2];
        cyp[3] += skey[3];
    }
} // namespace crypto::threefish
