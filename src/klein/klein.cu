#include "cu_crypto.cuh"
#include "klein/cu_klein.hpp"

namespace cu::crypto::klein::device
{
    __device__ static uint8_t sbox(uint8_t m)
    {
        static constexpr uint64_t sbox = 0x5DE8623C0BF19A47;

        return (sbox >> ((m >> 4 & 0xF) * 4) & 0xF) << 4 | (sbox >> ((m & 0xF) * 4) & 0xF);
    }

    __device__ static uint8_t multiply(uint8_t x) { return (x << 1) ^ (0x1B * (x >> 7)); }

    __device__ static uint64_t enc(uint64_t m, uint64_t k, int rounds)
    {

        uint8_t t[8];
        uint8_t *vm = (uint8_t *)&m;
        uint8_t *vk = (uint8_t *)&k;
        uint8_t u, v;

        for (int i = 1; i <= rounds; ++i)
        {

            // xor key
            vm[7] ^= vk[7];
            vm[6] ^= vk[6];
            vm[5] ^= vk[5];
            vm[4] ^= vk[4];
            vm[3] ^= vk[3];
            vm[2] ^= vk[2];
            vm[1] ^= vk[1];
            vm[0] ^= vk[0];

            // schedule
            t[7] = vk[7];
            t[6] = vk[6];
            t[5] = vk[5];
            t[4] = vk[4];
            t[3] = vk[3];

            vk[7] = vk[2];
            vk[6] = vk[1];
            vk[5] = vk[0] ^ i;
            vk[4] = vk[3];

            vk[3] = t[6] ^ vk[2];
            vk[2] = sbox(t[5] ^ vk[1]);
            vk[1] = sbox(t[4] ^ vk[0]);
            vk[0] = t[7] ^ t[3];

            // sbox
            vm[7] = sbox(vm[7]);
            vm[6] = sbox(vm[6]);
            vm[5] = sbox(vm[5]);
            vm[4] = sbox(vm[4]);
            vm[3] = sbox(vm[3]);
            vm[2] = sbox(vm[2]);
            vm[1] = sbox(vm[1]);
            vm[0] = sbox(vm[0]);

            // rotate
            t[7] = vm[5];
            t[6] = vm[4];
            t[5] = vm[3];
            t[4] = vm[2];
            t[3] = vm[1];
            t[2] = vm[0];
            t[1] = vm[7];
            t[0] = vm[6];

            //an efficient MixNibbles implementation for AES, Book Page 54;/*
            u = t[7] ^ t[6] ^ t[5] ^ t[4];
            v = t[7] ^ t[6];
            v = multiply(v);
            vm[7] = t[7] ^ v ^ u;

            v = t[6] ^ t[5];
            v = multiply(v);
            vm[6] = t[6] ^ v ^ u;

            v = t[5] ^ t[4];
            v = multiply(v);
            vm[5] = t[5] ^ v ^ u;

            v = t[4] ^ t[7];
            v = multiply(v);
            vm[4] = t[4] ^ v ^ u;

            u = t[3] ^ t[2] ^ t[1] ^ t[0];
            v = t[3] ^ t[2];
            v = multiply(v);
            vm[3] = t[3] ^ v ^ u;

            v = t[2] ^ t[1];
            v = multiply(v);
            vm[2] = t[2] ^ v ^ u;

            v = t[1] ^ t[0];
            v = multiply(v);
            vm[1] = t[1] ^ v ^ u;

            v = t[0] ^ t[3];
            v = multiply(v);
            vm[0] = t[0] ^ v ^ u;
        }
        // xor key
        m ^= k;

        return m;
    }

    // decrypt assuming that the key is already scheduled
    __device__ static uint64_t dec_nosched(uint64_t m, uint64_t k, int rounds)
    {

        uint8_t t[8];
        uint8_t *vm = (uint8_t *)&m;
        uint8_t *vk = (uint8_t *)&k;
        uint8_t u, v;

        m ^= k;
        for (int i = rounds; i > 0; --i)
        {
            //inverse the MixNibbles and the Rotate steps;
            t[7] = vm[7];
            t[6] = vm[6];
            t[5] = vm[5];
            t[4] = vm[4];
            t[3] = vm[3];
            t[2] = vm[2];
            t[1] = vm[1];
            t[0] = vm[0];

            u = multiply(multiply(t[7] ^ t[5]));
            v = multiply(multiply(t[6] ^ t[4]));

            t[7] ^= u;
            t[6] ^= v;
            t[5] ^= u;
            t[4] ^= v;

            u = multiply(multiply(t[3] ^ t[1]));
            v = multiply(multiply(t[2] ^ t[0]));

            t[3] ^= u;
            t[2] ^= v;
            t[1] ^= u;
            t[0] ^= v;

            u = t[7] ^ t[6] ^ t[5] ^ t[4];
            v = t[7] ^ t[6];
            v = multiply(v);
            vm[5] = t[7] ^ v ^ u;

            v = t[6] ^ t[5];
            v = multiply(v);
            vm[4] = t[6] ^ v ^ u;

            v = t[5] ^ t[4];
            v = multiply(v);
            vm[3] = t[5] ^ v ^ u;

            v = t[4] ^ t[7];
            v = multiply(v);
            vm[2] = t[4] ^ v ^ u;

            u = t[3] ^ t[2] ^ t[1] ^ t[0];
            v = t[3] ^ t[2];
            v = multiply(v);
            vm[1] = t[3] ^ v ^ u;

            v = t[2] ^ t[1];
            v = multiply(v);
            vm[0] = t[2] ^ v ^ u;

            v = t[1] ^ t[0];
            v = multiply(v);
            vm[7] = t[1] ^ v ^ u;

            v = t[0] ^ t[3];
            v = multiply(v);
            vm[6] = t[0] ^ v ^ u;

            //inverse the SubNibbles step;
            vm[7] = sbox(vm[7]);
            vm[6] = sbox(vm[6]);
            vm[5] = sbox(vm[5]);
            vm[4] = sbox(vm[4]);
            vm[3] = sbox(vm[3]);
            vm[2] = sbox(vm[2]);
            vm[1] = sbox(vm[1]);
            vm[0] = sbox(vm[0]);

            //reverse the round key;
            t[3] = vk[3];
            t[2] = vk[2];
            t[1] = vk[1];
            t[0] = vk[0];

            vk[3] = vk[4];
            vk[2] = vk[7];
            vk[1] = vk[6];
            vk[0] = vk[5] ^ i;

            vk[7] = t[0] ^ vk[3];
            vk[6] = t[3] ^ vk[2];
            vk[5] = sbox(t[2]) ^ vk[1];
            vk[4] = sbox(t[1]) ^ vk[0];

            //add round key with the intermediate vm;
            vm[7] ^= vk[7];
            vm[6] ^= vk[6];
            vm[5] ^= vk[5];
            vm[4] ^= vk[4];
            vm[3] ^= vk[3];
            vm[2] ^= vk[2];
            vm[1] ^= vk[1];
            vm[0] ^= vk[0];
        }

        return m;
    }
} // namespace cu::crypto::klein::device

namespace cu::crypto::klein
{
    CU_DEFINE_TESTENC(test_enc, device::enc)
    CU_DEFINE_TESTENC(test_dec_nosched, device::dec_nosched)

    CU_DEFINE_CRACKENC(crack, device::enc)
    CU_DEFINE_CRACKENC(crack_dec_nosched, device::dec_nosched)
} // namespace cu::crypto::klein
