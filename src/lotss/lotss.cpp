#include "lotss/lotss.hpp"
#include "sha256/sha256.hpp"

#include <random>
#include <cstring>

namespace crypto::lotss
{
    size_t compute_sksize(size_t msg_sz)
    {
        return (2 * sha256::BLOCK_SIZE * CHAR_BIT) * msg_sz;
    }

    size_t compute_pksize(size_t msg_sz)
    {
        return (2 * sha256::DIGEST_SIZE * CHAR_BIT) * msg_sz;
    }

    size_t compute_sigsize(size_t msg_sz)
    {
        return (sha256::BLOCK_SIZE * CHAR_BIT) * msg_sz;
    }

    void keygen(void *sk, size_t sk_sz, void *pk, size_t pk_sz)
    {
        using base_t = std::random_device::result_type;

        std::random_device rng;
        base_t *bsk = (base_t *)sk;

        for (size_t i = 0, n = sk_sz / sizeof(base_t); i < n; ++i)
            bsk[i] = rng();

        for (size_t i = 0, n = sk_sz / sha256::BLOCK_SIZE; i < n; ++i)
            sha256::hash_oneblock((uint8_t *)pk + i * sha256::DIGEST_SIZE,
                                  (uint8_t *)sk + i * sha256::BLOCK_SIZE, sha256::BLOCK_SIZE);
    }

    void sign(void *sig, const void *sk, const void *msg, size_t msg_sz)
    {
        const uint8_t *u8msg = (const uint8_t *)msg;

        for (size_t i = 0; i < msg_sz; ++i)
        {
            for (size_t j = 0; j < 8; ++j)
            {
                uint8_t b = (u8msg[i] >> j) & 1;
                size_t k = i * CHAR_BIT + j;

                memcpy((uint8_t *)sig + k * sha256::BLOCK_SIZE,
                       (const uint8_t *)sk + (2 * k + b) * sha256::BLOCK_SIZE, sha256::BLOCK_SIZE);
            }
        }
    }

    bool check(const void *sig, const void *pk, const void *msg, size_t msg_sz)
    {
        const uint8_t *u8msg = (const uint8_t *)msg;

        for (size_t i = 0; i < msg_sz; ++i)
        {
            for (size_t j = 0; j < 8; ++j)
            {
                uint8_t b = (u8msg[i] >> j) & 1;
                size_t k = i * CHAR_BIT + j;
                uint8_t dig[sha256::DIGEST_SIZE];

                sha256::hash_oneblock(dig, (const uint8_t *)sig + k * sha256::BLOCK_SIZE,
                                      sha256::BLOCK_SIZE);

                if (memcmp(dig, (const uint8_t *)pk + (2 * k + b) * sha256::DIGEST_SIZE,
                           sha256::DIGEST_SIZE))
                    return false;
            }
        }

        return true;
    }
} // namespace crypto::lotss
