#include "rainbow/rainbow.hpp"
#include "crypto.hpp"
#include <boost/sort/pdqsort/pdqsort.hpp>
#include <cstring>
#include <filesystem>
#include <fstream>
#include <vector>

namespace crypto::rainbow
{
    Table::Table(HashFunction hash, size_t digest_sz, size_t tab_sz, size_t chain_sz,
                 uint64_t watch) :
        chain(chain_sz),
        tab(tab_sz)
    {
        std::mt19937_64 rng{seeded_rng()};
        std::vector<uint8_t> dig(digest_sz);
        char msg[16];

        std::ranges::generate(chain, std::ref(rng));

        if (watch)
            std::cout << "Building rainbow table (" << tab.size() << " entries)\n";
#pragma omp parallel
        {
            int th_id = omp_get_thread_num();
            int th_n = omp_get_num_threads();

            for (size_t i = th_id; i < tab.size(); i += th_n)
            {
                tab[i].first = rng() & BITMASK;
                tab[i].second = tab[i].first;

                for (size_t j = 0; j < chain_sz; ++j)
                {
                    // Apply hash function and reduction
                    size_t len = u64_to_str(tab[i].second, msg);

                    hash(dig.data(), msg, len);
                    hash(msg, dig.data(), digest_sz);
                    tab[i].second = (*(uint64_t *)msg ^ chain[j]) & BITMASK;
                }

                if (!th_id && watch && i % watch == 0)
                {
                    std::cout << i << '/' << tab.size() << '\r';
                    std::cout.flush();
                }
            }
        }
        // sort so we can do binary search later
        std::ranges::sort(tab, [](auto &&x, auto &&y) { return x.second < y.second; });

        if (watch)
            std::cout << tab.size() << '/' << tab.size() << '\n';
    }

    Table::Table(const char *fname)
    {
        std::ifstream file{fname, std::ios::binary};
        size_t chain_sz = 0, tab_sz = 0;

        file.read((char *)&chain_sz, sizeof(chain_sz));
        chain.resize(chain_sz);
        file.read((char *)chain.data(), chain_sz * sizeof(chain[0]));

        file.read((char *)&tab_sz, sizeof(tab_sz));
        tab.resize(tab_sz);
        file.read((char *)tab.data(), tab_sz * sizeof(tab[0]));
    }

    void Table::save(const char *fname) const
    {
        std::ofstream file{fname, std::ios::binary};
        size_t chain_sz = chain.size(), tab_sz = tab.size();

        file.write((const char *)&chain_sz, sizeof(chain_sz));
        file.write((const char *)chain.data(), chain_sz * sizeof(chain[0]));

        file.write((const char *)&tab_sz, sizeof(tab_sz));
        file.write((const char *)tab.data(), tab.size() * sizeof(tab[0]));
    }

    void crack(crypto::HashFunction hash_foo, size_t digest_sz, const Table &table, void *res,
               const void *digest)
    {
        uint64_t msg = 0;
        char guess[16]{};
        std::vector<uint8_t> dig(digest_sz);
        std::pair<uint64_t, uint64_t> red;

        for (size_t i = 0, chain_sz = table.chain.size(); i < chain_sz; ++i)
        {
            memcpy(dig.data(), digest, digest_sz);
            for (size_t j = chain_sz - i - 1; j < chain_sz; ++j)
            {
                hash_foo(guess, dig.data(), digest_sz);
                red.second = (*(uint64_t *)guess ^ table.chain[j]) & Table::BITMASK;

                size_t len = u64_to_str(red.second, guess);

                hash_foo(dig.data(), guess, len);
            }

            auto match = std::ranges::lower_bound(
                table.tab, red, [](auto &&x, auto &&y) { return x.second < y.second; });

            if (match != table.tab.end() && match->second == red.second)
            {
                uint64_t next = match->first;

                for (size_t j = 0; memcmp(dig.data(), digest, digest_sz) && j < (chain_sz - i); ++j)
                {
                    size_t len = u64_to_str(next, guess);

                    hash_foo(dig.data(), guess, len);
                    msg = next;
                    hash_foo(guess, dig.data(), digest_sz);
                    next = (*(uint64_t *)guess ^ table.chain[j]) & Table::BITMASK;
                }
                if (!memcmp(dig.data(), digest, digest_sz))
                {
                    std::cout << "Collision found!\n";
                    u64_to_str(msg, (char *)res);
                    return;
                }
            }
        }

        std::cout << "No collision was found!\n";
    }
} // namespace crypto::rainbow
