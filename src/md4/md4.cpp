#include "md4/md4.hpp"
#include "intrinsics.h"
#include <cstring>

namespace crypto::md4
{
    static constexpr size_t BLOCK_SIZE = 512 / CHAR_BIT;
    static constexpr size_t BLOCK_MOD = 448 / CHAR_BIT;


    using Block = uint32_t[BLOCK_SIZE / sizeof(uint32_t)];

    static constexpr inline uint32_t F(uint32_t x, uint32_t y, uint32_t z)
    {
        return (x & y) | (~x & z);
    }

    static constexpr inline uint32_t G(uint32_t x, uint32_t y, uint32_t z)
    {
        return (x & y) | (x & z) | (y & z);
    }

    static constexpr inline uint32_t H(uint32_t x, uint32_t y, uint32_t z) { return x ^ y ^ z; }

    static inline void round1(uint32_t &a, uint32_t b, uint32_t c, uint32_t d, uint32_t x,
                              uint32_t s)
    {
        a = _rotl(a + F(b, c, d) + x, s);
    }

    static inline void round2(uint32_t &a, uint32_t b, uint32_t c, uint32_t d, uint32_t x,
                              uint32_t s)
    {
        a = _rotl(a + G(b, c, d) + x + 0x5A827999, s);
    }

    static inline void round3(uint32_t &a, uint32_t b, uint32_t c, uint32_t d, uint32_t x,
                              uint32_t s)
    {
        a = _rotl(a + H(b, c, d) + x + 0x6ED9EBA1, s);
    }

    static void hash_padded(uint32_t h[4], const Block *m, uint64_t sz)
    {
        for (uint64_t i = 0; i < sz; ++i)
        {
            uint32_t old[4] = {h[0], h[1], h[2], h[3]};
            const uint32_t *x = m[i];

            // ROUND 1
            round1(h[0], h[1], h[2], h[3], x[0], 3);
            round1(h[3], h[0], h[1], h[2], x[1], 7);
            round1(h[2], h[3], h[0], h[1], x[2], 11);
            round1(h[1], h[2], h[3], h[0], x[3], 19);

            round1(h[0], h[1], h[2], h[3], x[4], 3);
            round1(h[3], h[0], h[1], h[2], x[5], 7);
            round1(h[2], h[3], h[0], h[1], x[6], 11);
            round1(h[1], h[2], h[3], h[0], x[7], 19);

            round1(h[0], h[1], h[2], h[3], x[8], 3);
            round1(h[3], h[0], h[1], h[2], x[9], 7);
            round1(h[2], h[3], h[0], h[1], x[10], 11);
            round1(h[1], h[2], h[3], h[0], x[11], 19);

            round1(h[0], h[1], h[2], h[3], x[12], 3);
            round1(h[3], h[0], h[1], h[2], x[13], 7);
            round1(h[2], h[3], h[0], h[1], x[14], 11);
            round1(h[1], h[2], h[3], h[0], x[15], 19);


            // ROUND 2
            round2(h[0], h[1], h[2], h[3], x[0], 3);
            round2(h[3], h[0], h[1], h[2], x[4], 5);
            round2(h[2], h[3], h[0], h[1], x[8], 9);
            round2(h[1], h[2], h[3], h[0], x[12], 13);

            round2(h[0], h[1], h[2], h[3], x[1], 3);
            round2(h[3], h[0], h[1], h[2], x[5], 5);
            round2(h[2], h[3], h[0], h[1], x[9], 9);
            round2(h[1], h[2], h[3], h[0], x[13], 13);

            round2(h[0], h[1], h[2], h[3], x[2], 3);
            round2(h[3], h[0], h[1], h[2], x[6], 5);
            round2(h[2], h[3], h[0], h[1], x[10], 9);
            round2(h[1], h[2], h[3], h[0], x[14], 13);

            round2(h[0], h[1], h[2], h[3], x[3], 3);
            round2(h[3], h[0], h[1], h[2], x[7], 5);
            round2(h[2], h[3], h[0], h[1], x[11], 9);
            round2(h[1], h[2], h[3], h[0], x[15], 13);


            // ROUND 3
            round3(h[0], h[1], h[2], h[3], x[0], 3);
            round3(h[3], h[0], h[1], h[2], x[8], 9);
            round3(h[2], h[3], h[0], h[1], x[4], 11);
            round3(h[1], h[2], h[3], h[0], x[12], 15);

            round3(h[0], h[1], h[2], h[3], x[2], 3);
            round3(h[3], h[0], h[1], h[2], x[10], 9);
            round3(h[2], h[3], h[0], h[1], x[6], 11);
            round3(h[1], h[2], h[3], h[0], x[14], 15);

            round3(h[0], h[1], h[2], h[3], x[1], 3);
            round3(h[3], h[0], h[1], h[2], x[9], 9);
            round3(h[2], h[3], h[0], h[1], x[5], 11);
            round3(h[1], h[2], h[3], h[0], x[13], 15);

            round3(h[0], h[1], h[2], h[3], x[3], 3);
            round3(h[3], h[0], h[1], h[2], x[11], 9);
            round3(h[2], h[3], h[0], h[1], x[7], 11);
            round3(h[1], h[2], h[3], h[0], x[15], 15);


            h[0] += old[0];
            h[1] += old[1];
            h[2] += old[2];
            h[3] += old[3];
        }
    }

    void hash(void *digest, const void *msg, size_t sz)
    {
        Block pad[2]{};
        uint32_t *dig = (uint32_t *)digest;
        uint64_t rem_sz = sz % sizeof(Block);
        uint64_t blk_sz = sz / sizeof(Block) - (sz >= sizeof(Block));
        uint64_t off = ((rem_sz == 0) & (blk_sz != 0)) * sizeof(Block);
        uint64_t pad_sz = sizeof(Block) + ((rem_sz >= BLOCK_MOD) | (off != 0)) * sizeof(Block);

        memcpy(pad, (Block *)msg + blk_sz, rem_sz + off);

        ((uint8_t *)pad)[rem_sz + off] = 0x80;
        ((uint64_t *)pad)[pad_sz / sizeof(uint64_t) - 1] = sz * CHAR_BIT;

        dig[0] = 0x67452301;
        dig[1] = 0xEFCDAB89;
        dig[2] = 0x98BADCFE;
        dig[3] = 0x10325476;
        hash_padded(dig, (Block *)msg, blk_sz);
        hash_padded(dig, &pad[0], pad_sz / sizeof(Block));
    }
} // namespace crypto::md4
