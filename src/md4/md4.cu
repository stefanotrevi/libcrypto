#include "rainbow/cu_rainbow.cuh"
#include "md4/cu_md4.hpp"
#include <cstring>

namespace cu::crypto::md4::device
{
    static constexpr size_t BLOCK_SIZE = 512 / CHAR_BIT;
    static constexpr size_t BLOCK_MOD = 448 / CHAR_BIT;

    using Block = uint32_t[BLOCK_SIZE / sizeof(uint32_t)];

    __device__ static uint32_t F(uint32_t x, uint32_t y, uint32_t z)
    {
        return z ^ (x & (y ^ z));
        //return (x & y) | (~x & z);
    }

    __device__ static uint32_t G(uint32_t x, uint32_t y, uint32_t z)
    {
        return x ^ ((x ^ z) & (y ^ x));
        //        return (x & y) | (x & z) | (y & z);
    }

    __device__ static uint32_t H(uint32_t x, uint32_t y, uint32_t z) { return x ^ y ^ z; }

    __device__ static void round1(uint32_t &a, uint32_t b, uint32_t c, uint32_t d, uint32_t x,
                                  uint32_t s)
    {
        a = rotl(a + F(b, c, d) + x, s);
    }

    __device__ static void round2(uint32_t &a, uint32_t b, uint32_t c, uint32_t d, uint32_t x,
                                  uint32_t s)
    {
        a = rotl(a + G(b, c, d) + x + 0x5A827999, s);
    }

    __device__ static void round3(uint32_t &a, uint32_t b, uint32_t c, uint32_t d, uint32_t x,
                                  uint32_t s)
    {
        a = rotl(a + H(b, c, d) + x + 0x6ED9EBA1, s);
    }

    __device__ static void hash_block(uint32_t *h, const uint32_t *x)
    {
        uint32_t a = h[0];
        uint32_t b = h[1];
        uint32_t c = h[2];
        uint32_t d = h[3];

        // ROUND 1
        round1(a, b, c, d, x[0], 3);
        round1(d, a, b, c, x[1], 7);
        round1(c, d, a, b, x[2], 11);
        round1(b, c, d, a, x[3], 19);

        round1(a, b, c, d, x[4], 3);
        round1(d, a, b, c, x[5], 7);
        round1(c, d, a, b, x[6], 11);
        round1(b, c, d, a, x[7], 19);

        round1(a, b, c, d, x[8], 3);
        round1(d, a, b, c, x[9], 7);
        round1(c, d, a, b, x[10], 11);
        round1(b, c, d, a, x[11], 19);

        round1(a, b, c, d, x[12], 3);
        round1(d, a, b, c, x[13], 7);
        round1(c, d, a, b, x[14], 11);
        round1(b, c, d, a, x[15], 19);


        // ROUND 2
        round2(a, b, c, d, x[0], 3);
        round2(d, a, b, c, x[4], 5);
        round2(c, d, a, b, x[8], 9);
        round2(b, c, d, a, x[12], 13);

        round2(a, b, c, d, x[1], 3);
        round2(d, a, b, c, x[5], 5);
        round2(c, d, a, b, x[9], 9);
        round2(b, c, d, a, x[13], 13);

        round2(a, b, c, d, x[2], 3);
        round2(d, a, b, c, x[6], 5);
        round2(c, d, a, b, x[10], 9);
        round2(b, c, d, a, x[14], 13);

        round2(a, b, c, d, x[3], 3);
        round2(d, a, b, c, x[7], 5);
        round2(c, d, a, b, x[11], 9);
        round2(b, c, d, a, x[15], 13);


        // ROUND 3
        round3(a, b, c, d, x[0], 3);
        round3(d, a, b, c, x[8], 9);
        round3(c, d, a, b, x[4], 11);
        round3(b, c, d, a, x[12], 15);

        round3(a, b, c, d, x[2], 3);
        round3(d, a, b, c, x[10], 9);
        round3(c, d, a, b, x[6], 11);
        round3(b, c, d, a, x[14], 15);

        round3(a, b, c, d, x[1], 3);
        round3(d, a, b, c, x[9], 9);
        round3(c, d, a, b, x[5], 11);
        round3(b, c, d, a, x[13], 15);

        round3(a, b, c, d, x[3], 3);
        round3(d, a, b, c, x[11], 9);
        round3(c, d, a, b, x[7], 11);
        round3(b, c, d, a, x[15], 15);

        h[0] += a;
        h[1] += b;
        h[2] += c;
        h[3] += d;
    }

    __device__ static void hash_padded(uint32_t *h, const Block *m, uint64_t sz)
    {
        for (uint64_t i = 0; i < sz; ++i)
        {
            const uint32_t *x = m[i];

            hash_block(h, x);
        }
    }

    __device__ void hash(void *digest, const void *message, uint32_t sz)
    {
        Block pad[2]{};
        uint32_t rem_sz = sz % sizeof(Block);
        uint32_t blk_sz = sz / sizeof(Block) - (sz >= sizeof(Block));
        uint32_t off = ((rem_sz == 0) & (blk_sz != 0)) * sizeof(Block);
        uint32_t pad_sz = sizeof(Block) + ((rem_sz >= BLOCK_MOD) | (off != 0)) * sizeof(Block);
        uint32_t *dig = (uint32_t *)digest;

        memcpy(pad, (const Block *)message + blk_sz, rem_sz + off);

        ((uint8_t *)pad)[rem_sz + off] = 0x80;
        ((uint64_t *)pad)[pad_sz / sizeof(uint64_t) - 1] = sz * CHAR_BIT;

        dig[0] = 0x67452301;
        dig[1] = 0xEFCDAB89;
        dig[2] = 0x98BADCFE;
        dig[3] = 0x10325476;
        hash_padded(dig, (Block *)message, blk_sz);
        hash_padded(dig, pad, pad_sz / sizeof(Block));
    }

    // guarantee that message is shorter than one block
    __device__ static void hash_oneblock(void *digest, const void *message, uint32_t sz)
    {
        Block x{};
        uint32_t *dig = (uint32_t *)digest;
        uint32_t a = 0x67452301;
        uint32_t b = 0xEFCDAB89;
        uint32_t c = 0x98BADCFE;
        uint32_t d = 0x10325476;

        memcpy(x, message, sz);
        ((uint8_t *)x)[sz] = 0x80;
        ((uint64_t *)x)[sizeof(Block) / sizeof(uint64_t) - 1] = sz * CHAR_BIT;


        // ROUND 1
        round1(a, b, c, d, x[0], 3);
        round1(d, a, b, c, x[1], 7);
        round1(c, d, a, b, x[2], 11);
        round1(b, c, d, a, x[3], 19);

        round1(a, b, c, d, x[4], 3);
        round1(d, a, b, c, x[5], 7);
        round1(c, d, a, b, x[6], 11);
        round1(b, c, d, a, x[7], 19);

        round1(a, b, c, d, x[8], 3);
        round1(d, a, b, c, x[9], 7);
        round1(c, d, a, b, x[10], 11);
        round1(b, c, d, a, x[11], 19);

        round1(a, b, c, d, x[12], 3);
        round1(d, a, b, c, x[13], 7);
        round1(c, d, a, b, x[14], 11);
        round1(b, c, d, a, x[15], 19);


        // ROUND 2
        round2(a, b, c, d, x[0], 3);
        round2(d, a, b, c, x[4], 5);
        round2(c, d, a, b, x[8], 9);
        round2(b, c, d, a, x[12], 13);

        round2(a, b, c, d, x[1], 3);
        round2(d, a, b, c, x[5], 5);
        round2(c, d, a, b, x[9], 9);
        round2(b, c, d, a, x[13], 13);

        round2(a, b, c, d, x[2], 3);
        round2(d, a, b, c, x[6], 5);
        round2(c, d, a, b, x[10], 9);
        round2(b, c, d, a, x[14], 13);

        round2(a, b, c, d, x[3], 3);
        round2(d, a, b, c, x[7], 5);
        round2(c, d, a, b, x[11], 9);
        round2(b, c, d, a, x[15], 13);


        // ROUND 3
        round3(a, b, c, d, x[0], 3);
        round3(d, a, b, c, x[8], 9);
        round3(c, d, a, b, x[4], 11);
        round3(b, c, d, a, x[12], 15);

        round3(a, b, c, d, x[2], 3);
        round3(d, a, b, c, x[10], 9);
        round3(c, d, a, b, x[6], 11);
        round3(b, c, d, a, x[14], 15);

        round3(a, b, c, d, x[1], 3);
        round3(d, a, b, c, x[9], 9);
        round3(c, d, a, b, x[5], 11);
        round3(b, c, d, a, x[13], 15);

        round3(a, b, c, d, x[3], 3);
        round3(d, a, b, c, x[11], 9);
        round3(c, d, a, b, x[7], 11);
        round3(b, c, d, a, x[15], 15);

        dig[0] = a + 0x67452301;
        dig[1] = b + 0xEFCDAB89;
        dig[2] = c + 0x98BADCFE;
        dig[3] = d + 0x10325476;
    }
} // namespace cu::crypto::md4::device

namespace cu::crypto::md4
{
    CU_DEFINE_TESTHASH(test, device::hash, DIGEST_SIZE)

    CU_DEFINE_CRACKHASH(crack, device::hash, DIGEST_SIZE)

    CU_DEFINE_CRACKHASH(crack_oneblock, device::hash_oneblock, DIGEST_SIZE)

    ::crypto::rainbow::Table build_rainbow_table(size_t tab_sz, size_t chain_sz, bool watch)
    {
        auto d_tab{rainbow::build_table<device::hash_oneblock, DIGEST_SIZE>(tab_sz, chain_sz, watch)};

        return ::crypto::rainbow::Table{d_tab.chain, d_tab.tab};
    }
} // namespace cu::crypto::md4
