#include "sha256/sha256.hpp"
#include "avxlib.hpp"
#include <cstring>

static constexpr uint32_t sha256_h0[8] = {
    0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, 0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19,
};

static constexpr uint32_t sha256_k[64] = {
    0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
    0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
    0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
    0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
    0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
    0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
    0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
    0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2,
};

namespace crypto::sha256
{
    struct sha256_ctx
    {
        size_t tot_len;
        size_t len;
        uint8_t block[BLOCK_SIZE * 2];
        uint32_t h[DIGEST_SIZE / sizeof(uint32_t)];
    };

    static inline uint32_t CH(uint32_t x, uint32_t y, uint32_t z) { return (x & y) ^ (~x & z); }
    static inline uint32_t MAJ(uint32_t x, uint32_t y, uint32_t z)
    {
        return (x & y) ^ (x & z) ^ (y & z);
    }

    static inline uint32_t SHA256_F1(uint32_t x)
    {
        return _rotr(x, 2) ^ _rotr(x, 13) ^ _rotr(x, 22);
    }
    static inline uint32_t SHA256_F2(uint32_t x)
    {
        return _rotr(x, 6) ^ _rotr(x, 11) ^ _rotr(x, 25);
    }

    static inline uint32_t SHA256_F3(uint32_t x) { return _rotr(x, 7) ^ _rotr(x, 18) ^ x >> 3; }
    static inline uint32_t SHA256_F4(uint32_t x) { return _rotr(x, 17) ^ _rotr(x, 19) ^ x >> 10; }

    static inline void UNPACK32(uint32_t x, void *str) { *(uint32_t *)str = _bswap(x); }

    static inline uint32_t PACK32(const uint8_t *str) { return _bswap(*(uint32_t *)str); }

    static inline void SHA256_SCR(uint32_t w[64], uint32_t i)
    {
        w[i] = SHA256_F4(w[i - 2]) + w[i - 7] + SHA256_F3(w[i - 15]) + w[i - 16];
    }

    static inline __m256i reverse32(__m256i x)
    {
        __m256i perm = _mm256_set_epi8(28, 29, 30, 31, //
                                       24, 25, 26, 27, //
                                       20, 21, 22, 23, //
                                       16, 17, 18, 19, //
                                       12, 13, 14, 15, //
                                       8, 9, 10, 11,   //
                                       4, 5, 6, 7,     //
                                       0, 1, 2, 3);

        return _mm256_shuffle_epi8(x, perm);
    }

    /* SHA-256 functions */
    static void sha256_transf(sha256_ctx *ctx, const uint8_t *message, uint32_t block_nb)
    {
        uint32_t w[64];
        uint32_t wv[8];
        uint32_t t1, t2;
        const uint8_t *sub_block;

        for (uint32_t i = 0; i < block_nb; i++)
        {
            sub_block = message + (i << 6);

            for (uint32_t j = 0; j < 16; j++)
                w[j] = PACK32(&sub_block[j << 2]);

            for (uint32_t j = 16; j < 64; j++)
                SHA256_SCR(w, j);

            for (uint32_t j = 0; j < 8; j++)
                wv[j] = ctx->h[j];

            for (uint32_t j = 0; j < 64; j++)
            {
                t1 = wv[7] + SHA256_F2(wv[4]) + CH(wv[4], wv[5], wv[6]) + sha256_k[j] + w[j];
                t2 = SHA256_F1(wv[0]) + MAJ(wv[0], wv[1], wv[2]);
                wv[7] = wv[6];
                wv[6] = wv[5];
                wv[5] = wv[4];
                wv[4] = wv[3] + t1;
                wv[3] = wv[2];
                wv[2] = wv[1];
                wv[1] = wv[0];
                wv[0] = t1 + t2;
            }

            for (uint32_t j = 0; j < 8; j++)
                ctx->h[j] += wv[j];
        }
    }

    static void sha256_transf_fast(sha256_ctx *ctx, const uint8_t *message)
    {
        uint32_t w[64];
        uint32_t wv[8] = {0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a,
                          0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19};
        __m256i t;

        t = _mm256_loadu_si256((const __m256i *)message);
        _mm256_storeu_si256((__m256i *)w, reverse32(t));

        t = _mm256_loadu_si256((const __m256i *)message + 1);
        _mm256_storeu_si256((__m256i *)w + 1, reverse32(t));

        for (uint32_t j = 16; j < 64; j++)
            SHA256_SCR(w, j);

        for (uint32_t j = 0; j < 64; j++)
        {
            uint32_t t1 = wv[7] + SHA256_F2(wv[4]) + CH(wv[4], wv[5], wv[6]) + sha256_k[j] + w[j];
            uint32_t t2 = SHA256_F1(wv[0]) + MAJ(wv[0], wv[1], wv[2]);

            wv[7] = wv[6];
            wv[6] = wv[5];
            wv[5] = wv[4];
            wv[4] = wv[3] + t1;
            wv[3] = wv[2];
            wv[2] = wv[1];
            wv[1] = wv[0];
            wv[0] = t1 + t2;
        }

        ctx->h[0] = 0x6a09e667 + wv[0];
        ctx->h[1] = 0xbb67ae85 + wv[1];
        ctx->h[2] = 0x3c6ef372 + wv[2];
        ctx->h[3] = 0xa54ff53a + wv[3];
        ctx->h[4] = 0x510e527f + wv[4];
        ctx->h[5] = 0x9b05688c + wv[5];
        ctx->h[6] = 0x1f83d9ab + wv[6];
        ctx->h[7] = 0x5be0cd19 + wv[7];
    }

    static void sha256_init(sha256_ctx *ctx)
    {
        for (int i = 0; i < 8; i++)
            ctx->h[i] = sha256_h0[i];

        ctx->len = 0;
        ctx->tot_len = 0;
    }

    static void sha256_update(sha256_ctx *ctx, const uint8_t *message, size_t len)
    {
        uint32_t block_nb;
        size_t new_len, rem_len, tmp_len;
        const uint8_t *shifted_message;

        tmp_len = BLOCK_SIZE - ctx->len;
        rem_len = len < tmp_len ? len : tmp_len;

        memcpy(&ctx->block[ctx->len], message, rem_len);

        if (ctx->len + len < BLOCK_SIZE)
        {
            ctx->len += len;
            return;
        }

        new_len = len - rem_len;
        block_nb = new_len / BLOCK_SIZE;

        shifted_message = message + rem_len;

        sha256_transf(ctx, ctx->block, 1);
        sha256_transf(ctx, shifted_message, block_nb);

        rem_len = new_len % BLOCK_SIZE;

        memcpy(ctx->block, &shifted_message[block_nb << 6], rem_len);

        ctx->len = rem_len;
        ctx->tot_len += (block_nb + 1) << 6;
    }

    static void sha256_final(sha256_ctx *ctx, uint8_t *digest)
    {
        size_t block_nb;
        size_t pm_len;
        size_t len_b;

        int i;

        block_nb = (1 + ((BLOCK_SIZE - 9) < (ctx->len % BLOCK_SIZE)));

        len_b = (ctx->tot_len + ctx->len) << 3;
        pm_len = block_nb << 6;

        memset(ctx->block + ctx->len, 0, pm_len - ctx->len);
        ctx->block[ctx->len] = 0x80;
        UNPACK32(len_b, ctx->block + pm_len - 4);

        sha256_transf(ctx, ctx->block, block_nb);

        for (i = 0; i < 8; i++)
            UNPACK32(ctx->h[i], &digest[i << 2]);
    }

    void hash_oneblock(void *digest, const void *message, size_t len)
    {
        sha256_ctx ctx{};
        uint8_t *dig = (uint8_t *)digest;

        ctx.len = len;
        memcpy(ctx.block, message, len);

        ctx.block[ctx.len] = 0x80;
        UNPACK32(ctx.len << 3, ctx.block + 64 - 4);

        sha256_transf_fast(&ctx, ctx.block);

        for (int i = 0; i < 8; i++)
            UNPACK32(ctx.h[i], dig + (i << 2));
    }

    void hash(void *digest, const void *message, size_t len)
    {
        sha256_ctx ctx;

        sha256_init(&ctx);
        sha256_update(&ctx, (const uint8_t *)message, len);
        sha256_final(&ctx, (uint8_t *)digest);
    }
} // namespace sha256
