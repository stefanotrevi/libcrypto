#include "cu_crypto.cuh"
#include "sha256/cu_sha256.hpp"
#include <cstring>
#include <iostream>

namespace cu::crypto::sha256::device
{
    static constexpr uint32_t BLOCK_SIZE = (512 / 8);

    typedef struct
    {
        uint32_t tot_len;
        uint32_t len;
        uint8_t block[BLOCK_SIZE * 2];
        uint32_t h[8];
    } sha256_ctx;

    __device__ static inline uint32_t CH(uint32_t x, uint32_t y, uint32_t z)
    {
        y &= x;
        z &= ~x;
        y ^= z;

        return y;
    }
    __device__ static inline uint32_t MAJ(uint32_t x, uint32_t y, uint32_t z)
    {
        uint32_t t = x;

        t &= y;
        x &= z;
        y &= z;
        t ^= x;
        t ^= y;

        return t;
    }

    __device__ static inline uint32_t rotr(uint32_t x, uint32_t y)
    {
        uint32_t t = x;

        t >>= y;
        x <<= (32 - y);
        t |= x;

        return t;
    }

    __device__ static inline uint32_t SHA256_F1(uint32_t x)
    {
        uint32_t t = rotr(x, 2);

        t ^= rotr(x, 13);
        t ^= rotr(x, 22);

        return t;
    }
    __device__ static inline uint32_t SHA256_F2(uint32_t x)
    {
        uint32_t t = rotr(x, 6);

        t ^= rotr(x, 11);
        t ^= rotr(x, 25);

        return t;
    }

    __device__ static inline uint32_t SHA256_F3(uint32_t x)
    {
        uint32_t t = rotr(x, 7);

        t ^= rotr(x, 18);
        x >>= 3;
        t ^= x;

        return t;
    }
    __device__ static inline uint32_t SHA256_F4(uint32_t x)
    {
        uint32_t t = rotr(x, 17);

        t ^= rotr(x, 19);
        x >>= 10;
        t ^= x;

        return t;
    }

    __device__ static inline uint32_t bswap(uint32_t x)
    {
        x = x << 16 | x >> 16;
        x = x << 8 & 0xFF00FF00 | x >> 8 & ~0xFF00FF00;

        return x;
    }

    __device__ static inline void UNPACK32(uint32_t x, uint8_t *str)
    {
        *(uint32_t *)str = bswap(x);
    }

    __device__ static inline uint32_t PACK32(const uint8_t *str)
    {
        return bswap(*(uint32_t *)str);
    }

    __device__ static inline void SHA256_SCR(uint32_t w[64], uint32_t i)
    {
        w[i] = SHA256_F4(w[i - 2]) + w[i - 7] + SHA256_F3(w[i - 15]) + w[i - 16];
    }

    __device__ static void sha256_transf(sha256_ctx *ctx, const uint8_t *message)
    {
        static constexpr uint32_t sha256_k[64] = {
            0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4,
            0xab1c5ed5, 0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe,
            0x9bdc06a7, 0xc19bf174, 0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f,
            0x4a7484aa, 0x5cb0a9dc, 0x76f988da, 0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
            0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc,
            0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85, 0xa2bfe8a1, 0xa81a664b,
            0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070, 0x19a4c116,
            0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
            0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7,
            0xc67178f2,
        };

        uint32_t w[64];
        uint32_t wv[8] = {0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a,
                          0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19};

        for (uint32_t j = 0; j < 16; j++)
            w[j] = PACK32(&message[j << 2]);

        for (uint32_t j = 16; j < 64; j++)
            SHA256_SCR(w, j);

        for (uint32_t j = 0; j < 64; j++)
        {
            uint32_t t1 = wv[7] + SHA256_F2(wv[4]) + CH(wv[4], wv[5], wv[6]) + sha256_k[j] + w[j];
            uint32_t t2 = SHA256_F1(wv[0]) + MAJ(wv[0], wv[1], wv[2]);

            wv[7] = wv[6];
            wv[6] = wv[5];
            wv[5] = wv[4];
            wv[4] = wv[3] + t1;
            wv[3] = wv[2];
            wv[2] = wv[1];
            wv[1] = wv[0];
            wv[0] = t1 + t2;
        }

        ctx->h[0] = 0x6a09e667 + wv[0];
        ctx->h[1] = 0xbb67ae85 + wv[1];
        ctx->h[2] = 0x3c6ef372 + wv[2];
        ctx->h[3] = 0xa54ff53a + wv[3];
        ctx->h[4] = 0x510e527f + wv[4];
        ctx->h[5] = 0x9b05688c + wv[5];
        ctx->h[6] = 0x1f83d9ab + wv[6];
        ctx->h[7] = 0x5be0cd19 + wv[7];
    }

    __device__ static void hash_oneblock(void *digest, const void *message, uint32_t len)
    {
        sha256_ctx ctx{};
        uint8_t *dig = (uint8_t *)digest;

        ctx.len = len;

        memcpy(ctx.block, message, len);

        ctx.block[ctx.len] = 0x80;
        UNPACK32(ctx.len << 3, ctx.block + 64 - 4);

        sha256_transf(&ctx, ctx.block);

        for (int i = 0; i < 8; i++)
            UNPACK32(ctx.h[i], dig + (i << 2));
    }

    template<uint32_t len>
    __device__ static void hash_oneblock_const(uint32_t *m)
    {
        ((uint8_t *)m)[len] = 0x80;

        static constexpr const uint32_t sha256_k[64] = {
            0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4,
            0xab1c5ed5, 0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe,
            0x9bdc06a7, 0xc19bf174, 0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f,
            0x4a7484aa, 0x5cb0a9dc, 0x76f988da, 0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
            0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc,
            0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85, 0xa2bfe8a1, 0xa81a664b,
            0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070, 0x19a4c116,
            0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
            0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7,
            0xc67178f2,
        };

        uint32_t wv[8] = {0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a,
                          0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19};

        m[0] = bswap(m[0]);
        m[1] = bswap(m[1]);
        m[2] = bswap(m[2]);
        m[3] = bswap(m[3]);
        m[4] = bswap(m[4]);
        m[5] = bswap(m[5]);
        m[6] = bswap(m[6]);
        m[7] = bswap(m[7]);
        m[8] = bswap(m[8]);
        m[9] = bswap(m[9]);
        m[10] = bswap(m[10]);
        m[11] = bswap(m[11]);
        m[12] = bswap(m[12]);
        m[13] = bswap(m[13]);
        m[14] = bswap(m[14]);
        m[15] = len * 8;

// 0 - 16
#pragma unroll(16)
        for (uint32_t j = 0; j < 16; j++)
        {
            uint32_t t1 = wv[7];
            t1 += SHA256_F2(wv[4]);
            t1 += CH(wv[4], wv[5], wv[6]);
            t1 += sha256_k[j];
            t1 += m[j];

            uint32_t t2 = SHA256_F1(wv[0]);
            t2 += MAJ(wv[0], wv[1], wv[2]);

            wv[7] = wv[6];
            wv[6] = wv[5];
            wv[5] = wv[4];
            wv[4] = wv[3];
            wv[4] += t1;
            wv[3] = wv[2];
            wv[2] = wv[1];
            wv[1] = wv[0];
            wv[0] = t1;
            wv[0] += t2;
        }

        m[0] += SHA256_F4(m[14]), m[0] += m[9], m[0] += SHA256_F3(m[1]);
        m[1] += SHA256_F4(m[15]), m[1] += m[10], m[1] += SHA256_F3(m[2]);
        m[2] += SHA256_F4(m[0]), m[2] += m[11], m[2] += SHA256_F3(m[3]);
        m[3] += SHA256_F4(m[1]), m[3] += m[12], m[3] += SHA256_F3(m[4]);
        m[4] += SHA256_F4(m[2]), m[4] += m[13], m[4] += SHA256_F3(m[5]);
        m[5] += SHA256_F4(m[3]), m[5] += m[14], m[5] += SHA256_F3(m[6]);
        m[6] += SHA256_F4(m[4]), m[6] += m[15], m[6] += SHA256_F3(m[7]);
        m[7] += SHA256_F4(m[5]), m[7] += m[0], m[7] += SHA256_F3(m[8]);
        m[8] += SHA256_F4(m[6]), m[8] += m[1], m[8] += SHA256_F3(m[9]);
        m[9] += SHA256_F4(m[7]), m[9] += m[2], m[9] += SHA256_F3(m[10]);
        m[10] += SHA256_F4(m[8]), m[10] += m[3], m[10] += SHA256_F3(m[11]);
        m[11] += SHA256_F4(m[9]), m[11] += m[4], m[11] += SHA256_F3(m[12]);
        m[12] += SHA256_F4(m[10]), m[12] += m[5], m[12] += SHA256_F3(m[13]);
        m[13] += SHA256_F4(m[11]), m[13] += m[6], m[13] += SHA256_F3(m[14]);
        m[14] += SHA256_F4(m[12]), m[14] += m[7], m[14] += SHA256_F3(m[15]);
        m[15] += SHA256_F4(m[13]), m[15] += m[8], m[15] += SHA256_F3(m[0]);

// 16 - 32
#pragma unroll(16)
        for (uint32_t j = 0; j < 16; j++)
        {
            uint32_t t1 = wv[7];
            t1 += SHA256_F2(wv[4]);
            t1 += CH(wv[4], wv[5], wv[6]);
            t1 += sha256_k[16 + j];
            t1 += m[j];

            uint32_t t2 = SHA256_F1(wv[0]);
            t2 += MAJ(wv[0], wv[1], wv[2]);

            wv[7] = wv[6];
            wv[6] = wv[5];
            wv[5] = wv[4];
            wv[4] = wv[3];
            wv[4] += t1;
            wv[3] = wv[2];
            wv[2] = wv[1];
            wv[1] = wv[0];
            wv[0] = t1;
            wv[0] += t2;
        }

        m[0] += SHA256_F4(m[14]), m[0] += m[9], m[0] += SHA256_F3(m[1]);
        m[1] += SHA256_F4(m[15]), m[1] += m[10], m[1] += SHA256_F3(m[2]);
        m[2] += SHA256_F4(m[0]), m[2] += m[11], m[2] += SHA256_F3(m[3]);
        m[3] += SHA256_F4(m[1]), m[3] += m[12], m[3] += SHA256_F3(m[4]);
        m[4] += SHA256_F4(m[2]), m[4] += m[13], m[4] += SHA256_F3(m[5]);
        m[5] += SHA256_F4(m[3]), m[5] += m[14], m[5] += SHA256_F3(m[6]);
        m[6] += SHA256_F4(m[4]), m[6] += m[15], m[6] += SHA256_F3(m[7]);
        m[7] += SHA256_F4(m[5]), m[7] += m[0], m[7] += SHA256_F3(m[8]);
        m[8] += SHA256_F4(m[6]), m[8] += m[1], m[8] += SHA256_F3(m[9]);
        m[9] += SHA256_F4(m[7]), m[9] += m[2], m[9] += SHA256_F3(m[10]);
        m[10] += SHA256_F4(m[8]), m[10] += m[3], m[10] += SHA256_F3(m[11]);
        m[11] += SHA256_F4(m[9]), m[11] += m[4], m[11] += SHA256_F3(m[12]);
        m[12] += SHA256_F4(m[10]), m[12] += m[5], m[12] += SHA256_F3(m[13]);
        m[13] += SHA256_F4(m[11]), m[13] += m[6], m[13] += SHA256_F3(m[14]);
        m[14] += SHA256_F4(m[12]), m[14] += m[7], m[14] += SHA256_F3(m[15]);
        m[15] += SHA256_F4(m[13]), m[15] += m[8], m[15] += SHA256_F3(m[0]);

// 32 - 48
#pragma unroll(16)
        for (uint32_t j = 0; j < 16; j++)
        {
            uint32_t t1 = wv[7];
            t1 += SHA256_F2(wv[4]);
            t1 += CH(wv[4], wv[5], wv[6]);
            t1 += sha256_k[32 + j];
            t1 += m[j];

            uint32_t t2 = SHA256_F1(wv[0]);
            t2 += MAJ(wv[0], wv[1], wv[2]);

            wv[7] = wv[6];
            wv[6] = wv[5];
            wv[5] = wv[4];
            wv[4] = wv[3];
            wv[4] += t1;
            wv[3] = wv[2];
            wv[2] = wv[1];
            wv[1] = wv[0];
            wv[0] = t1;
            wv[0] += t2;
        }

        m[0] += SHA256_F4(m[14]), m[0] += m[9], m[0] += SHA256_F3(m[1]);
        m[1] += SHA256_F4(m[15]), m[1] += m[10], m[1] += SHA256_F3(m[2]);
        m[2] += SHA256_F4(m[0]), m[2] += m[11], m[2] += SHA256_F3(m[3]);
        m[3] += SHA256_F4(m[1]), m[3] += m[12], m[3] += SHA256_F3(m[4]);
        m[4] += SHA256_F4(m[2]), m[4] += m[13], m[4] += SHA256_F3(m[5]);
        m[5] += SHA256_F4(m[3]), m[5] += m[14], m[5] += SHA256_F3(m[6]);
        m[6] += SHA256_F4(m[4]), m[6] += m[15], m[6] += SHA256_F3(m[7]);
        m[7] += SHA256_F4(m[5]), m[7] += m[0], m[7] += SHA256_F3(m[8]);
        m[8] += SHA256_F4(m[6]), m[8] += m[1], m[8] += SHA256_F3(m[9]);
        m[9] += SHA256_F4(m[7]), m[9] += m[2], m[9] += SHA256_F3(m[10]);
        m[10] += SHA256_F4(m[8]), m[10] += m[3], m[10] += SHA256_F3(m[11]);
        m[11] += SHA256_F4(m[9]), m[11] += m[4], m[11] += SHA256_F3(m[12]);
        m[12] += SHA256_F4(m[10]), m[12] += m[5], m[12] += SHA256_F3(m[13]);
        m[13] += SHA256_F4(m[11]), m[13] += m[6], m[13] += SHA256_F3(m[14]);
        m[14] += SHA256_F4(m[12]), m[14] += m[7], m[14] += SHA256_F3(m[15]);
        m[15] += SHA256_F4(m[13]), m[15] += m[8], m[15] += SHA256_F3(m[0]);

// 48 - 64
#pragma unroll(16)
        for (uint32_t j = 0; j < 16; j++)
        {
            uint32_t t1 = wv[7];
            t1 += SHA256_F2(wv[4]);
            t1 += CH(wv[4], wv[5], wv[6]);
            t1 += sha256_k[48 + j];
            t1 += m[j];

            uint32_t t2 = SHA256_F1(wv[0]);
            t2 += MAJ(wv[0], wv[1], wv[2]);

            wv[7] = wv[6];
            wv[6] = wv[5];
            wv[5] = wv[4];
            wv[4] = wv[3];
            wv[4] += t1;
            wv[3] = wv[2];
            wv[2] = wv[1];
            wv[1] = wv[0];
            wv[0] = t1;
            wv[0] += t2;
        }

        m[0] = wv[0];
        m[1] = wv[1];
        m[2] = wv[2];
        m[3] = wv[3];
        m[4] = wv[4];
        m[5] = wv[5];
        m[6] = wv[6];
        m[7] = wv[7];

        m[0] += 0x6a09e667;
        m[1] += 0xbb67ae85;
        m[2] += 0x3c6ef372;
        m[3] += 0xa54ff53a;
        m[4] += 0x510e527f;
        m[5] += 0x9b05688c;
        m[6] += 0x1f83d9ab;
        m[7] += 0x5be0cd19;

        m[0] = bswap(m[0]);
        m[1] = bswap(m[1]);
        m[2] = bswap(m[2]);
        m[3] = bswap(m[3]);
        m[4] = bswap(m[4]);
        m[5] = bswap(m[5]);
        m[6] = bswap(m[6]);
        m[7] = bswap(m[7]);
    }

#define lengthof(...) sizeof(__VA_ARGS__) / sizeof(__VA_ARGS__[0])

    template<uint8_t digits>
    __global__ void crack_ker(uint64_t id, uint64_t *dev_id)
    {
        static constexpr uint8_t alphabet[] = "abcdefghijklmnopqrstuvwxyzaeiou_";
        static constexpr uint8_t target[] = "hackerman";
        static constexpr uint32_t SALT_PRESZ = 20;
        static constexpr uint32_t SALT_SZ = SALT_PRESZ + digits; // to represent a decimal uint64_t

        uint8_t salt[64] = "Rg5vhFkyH7VEqZd3Ne9V";
        uint64_t core_idx = blockIdx.x * blockDim.x + threadIdx.x;

        id += core_idx;

        uint64_t t = id;
        if constexpr (digits >= 1)
        {
            salt[SALT_PRESZ + digits - 1] = '0' + t % 10;
            t /= 10;
        }
        if constexpr (digits >= 2)
        {
            salt[SALT_PRESZ + digits - 2] = '0' + t % 10;
            t /= 10;
        }
        if constexpr (digits >= 3)
        {
            salt[SALT_PRESZ + digits - 3] = '0' + t % 10;
            t /= 10;
        }
        if constexpr (digits >= 4)
        {
            salt[SALT_PRESZ + digits - 4] = '0' + t % 10;
            t /= 10;
        }
        if constexpr (digits >= 5)
        {
            salt[SALT_PRESZ + digits - 5] = '0' + t % 10;
            t /= 10;
        }
        if constexpr (digits >= 6)
        {
            salt[SALT_PRESZ + digits - 6] = '0' + t % 10;
            t /= 10;
        }
        if constexpr (digits >= 7)
        {
            salt[SALT_PRESZ + digits - 7] = '0' + t % 10;
            t /= 10;
        }
        if constexpr (digits >= 8)
        {
            salt[SALT_PRESZ + digits - 8] = '0' + t % 10;
            t /= 10;
        }
        if constexpr (digits >= 9)
        {
            salt[SALT_PRESZ + digits - 9] = '0' + t % 10;
            t /= 10;
        }
        if constexpr (digits >= 10)
        {
            salt[SALT_PRESZ + digits - 10] = '0' + t % 10;
            t /= 10;
        }
        if constexpr (digits >= 11)
        {
            salt[SALT_PRESZ + digits - 11] = '0' + t % 10;
            t /= 10;
        }
        if constexpr (digits >= 12)
        {
            salt[SALT_PRESZ + digits - 12] = '0' + t % 10;
            t /= 10;
        }
        if constexpr (digits >= 13)
        {
            salt[SALT_PRESZ + digits - 13] = '0' + t % 10;
            t /= 10;
        }
        if constexpr (digits >= 14)
        {
            salt[SALT_PRESZ + digits - 14] = '0' + t % 10;
            t /= 10;
        }
        if constexpr (digits >= 15)
        {
            salt[SALT_PRESZ + digits - 15] = '0' + t % 10;
            t /= 10;
        }
        if constexpr (digits >= 16)
        {
            salt[SALT_PRESZ + digits - 16] = '0' + t % 10;
            t /= 10;
        }


        hash_oneblock_const<SALT_SZ>((uint32_t *)salt);

        uint2 x;
        x.x = (uint32_t)salt[0] << 24;
        x.x |= (uint32_t)salt[1] << 16;
        x.x |= (uint32_t)salt[2] << 8;
        x.x |= (uint32_t)salt[3] << 0;

        x.y = (uint32_t)salt[4] << 24;
        x.y |= (uint32_t)salt[5] << 16;
        x.y |= (uint32_t)salt[6] << 8;
        x.y |= (uint32_t)salt[7] << 0;

        x.y = x.y >> 4 | x.x << 28;
        x.x >>= 4;

        bool found = true;
        if constexpr (lengthof(target) > 1)
            found &= (alphabet[(x.y >> 0) & 0b11111]) == target[0];
        if constexpr (lengthof(target) > 2)
            found &= (alphabet[(x.y >> 5) & 0b11111]) == target[1];
        if constexpr (lengthof(target) > 3)
            found &= (alphabet[(x.y >> 10) & 0b11111]) == target[2];
        if constexpr (lengthof(target) > 4)
            found &= (alphabet[(x.y >> 15) & 0b11111]) == target[3];
        if constexpr (lengthof(target) > 5)
            found &= (alphabet[(x.y >> 20) & 0b11111]) == target[4];
        if constexpr (lengthof(target) > 6)
            found &= (alphabet[(x.y >> 25) & 0b11111]) == target[5];

        if constexpr (lengthof(target) > 7)
            found &= (alphabet[(x.y >> 30 | x.x << 2 & 0b11100) & 0b11111]) == target[6];

        if constexpr (lengthof(target) > 8)
            found &= (alphabet[(x.x >> 3) & 0b11111]) == target[7];
        if constexpr (lengthof(target) > 9)
            found &= (alphabet[(x.x >> 8) & 0b11111]) == target[8];
        if constexpr (lengthof(target) > 10)
            found &= (alphabet[(x.x >> 13) & 0b11111]) == target[9];
        if constexpr (lengthof(target) > 11)
            found &= (alphabet[(x.x >> 18) & 0b11111]) == target[10];
        if constexpr (lengthof(target) > 12)
            found &= (alphabet[(x.x >> 23) & 0b11111]) == target[11];

        if (found)
            *dev_id = id;

        // printf("%u, %llu, %.12s\n", SALT_SZ, id, salt);
        /*for (uint32_t i = 0; i < lengthof(digest); ++i)
            printf("%02x ", digest[i]);
        printf("\n");*/
    }
} // namespace cu::crypto::sha256::device

namespace cu::crypto::sha256
{
    CU_DEFINE_TESTHASH(test, device::hash_oneblock, DIGEST_SIZE)
    CU_DEFINE_CRACKHASH(crack_oneblock, device::hash_oneblock, DIGEST_SIZE)

    uint64_t crack_id(uint64_t old, uint64_t last)
    {
        using clk = std::chrono::high_resolution_clock;
        using namespace std::chrono_literals;

        static constexpr uint32_t GRDSZ = 1 << 16;
        static constexpr uint32_t BLKSZ = MAX_BLKSZ;
        //        static cudaStream_t copyStream;
        //        static cudaStream_t computeStream;
        uint64_t *dev_id;

        uint64_t id = 0;

        cudaMalloc(&dev_id, sizeof(uint64_t));
        cudaMemcpy(dev_id, (const void *)&id, sizeof(id), cudaMemcpyHostToDevice);

        auto start = clk::now();
        for (uint64_t off = old + 1; !id && off < last; off += GRDSZ * BLKSZ)
        {
#define CRACK_CASE(n)                                                                              \
    case n: device::crack_ker<n><<<GRDSZ, BLKSZ, 0, 0>>>(off, dev_id); break;

            switch ((int)ceil(log10(off)))
            {
                CRACK_CASE(1)
                CRACK_CASE(2)
                CRACK_CASE(3)
                CRACK_CASE(4)
                CRACK_CASE(5)
                CRACK_CASE(6)
                CRACK_CASE(7)
                CRACK_CASE(8)
                CRACK_CASE(9)
                CRACK_CASE(10)
                CRACK_CASE(11)
                CRACK_CASE(12)
                CRACK_CASE(13)
                CRACK_CASE(14)
                CRACK_CASE(15)
                CRACK_CASE(16)
            default: break;
            }
#undef CRACK_CASE
            cudaMemcpyAsync((void *)&id, dev_id, sizeof(id), cudaMemcpyDeviceToHost, 0);
            /* if ((clk::now() - start) > 100ms)
            {
                std::cout << ((double)(off - old) / (1000000 / 10)) << " MH/s\r";
                std::cout.flush();
                old = off;
                start = clk::now();
            }*/
            //cudaDeviceSynchronize();
            //_sleep(1);
        }
        //cudaDeviceSynchronize();

        cudaFree(dev_id);
        //        cudaStreamDestroy(copyStream);
        //cudaStreamDestroy(computeStream);

        return id;
    }
} // namespace cu::crypto::sha256
