#!/usr/bin/env python3

import sys


def rotate_left(word, n, word_size=4):
    mask = 2**word_size - 1
    return ((word << n) & mask) | ((word >> (word_size - n) & mask))


def rotate_right(word, n, word_size=4):
    mask = 2**word_size - 1
    return ((word >> n) & mask) | ((word << (word_size - n) & mask))


def F(word):
    return (rotate_left(word, 2, 4) & rotate_left(word, 1, 4)) ^ word


def round_function(left, right, key):
    return ((F(left) ^ right ^ key), left)


def encrypt(word, key, rounds=16):
    left = (word >> 4) & 0xF
    right = word & 0xF

    for i in range(rounds):
        left, right = round_function(left, right, key & 0xF)
        key = rotate_right(key, 4, 64)

    return (left << 4) | right


def decrypt(word, key, rounds=16, from_round=16):
    left = word & 0xF
    right = (word >> 4) & 0xF

    # wind the key
    key = rotate_right(key, ((from_round-1)*4) % 64, 64)

    for i in range(rounds):
        left, right = round_function(left, right, key & 0xF)
        key = rotate_left(key, 4, 64)

    return left | (right << 4)


def create_testvectors():
    key = 0x1234567890ABCDEF
    c = encrypt(0xAB, key, 16)
    print("%02X %02X %016X" % (0xAB, c, key))
    c = encrypt(0xFF, key, 16)
    print("%02X %02X %016X" % (0xFF, c, key))
    key = 0x136855A334CD90EF
    c = encrypt(0x99, key, 16)
    print("%02X %02X %016X" % (0x99, c, key))
    c = encrypt(0x53, key, 16)
    print("%02X %02X %016X" % (0x53, c, key))


"""
if __name__ == "__main__":
    import random

    if len(sys.argv) == 1:
        print("Error occured")
        exit()

    key = int(sys.argv[1], 16)
    for i in range(80):
        word = random.getrandbits(8)
        cipher = encrypt(word, key)
        print("%02X %02X" % (word, cipher))
"""
create_testvectors()