from time import perf_counter_ns as clock
from hashlib import md5, sha256

msg = open("data.dat", "rb").read()

start = clock()
dig = str(md5(msg).hexdigest())
finish = clock()
elap = (finish - start) / 1_000_000

print(f"{dig}, {elap} ms.")
#print(md5(b"").hexdigest())
