#include "md5/md5.hpp"
#include "intrinsics.h"

#include <cstdlib>
#include <cstring>
#include <iomanip>

namespace crypto::md5
{
    static constexpr size_t BLOCK_SIZE = 512 / CHAR_BIT;
    static constexpr size_t BLOCK_MOD = 448 / CHAR_BIT;

    using Block = uint32_t[BLOCK_SIZE / sizeof(uint32_t)];

    using Transf = uint32_t(uint32_t x, uint32_t y, uint32_t z);

    static constexpr inline uint32_t F(uint32_t x, uint32_t y, uint32_t z)
    {
        return (x & y) | (~x & z);
        //        return z ^ (x & (y ^ z));
    }

    static constexpr inline uint32_t G(uint32_t x, uint32_t y, uint32_t z)
    {
        return (z & x) | (~z & y);
        //      return F(z, x, y);
    }

    static constexpr inline uint32_t H(uint32_t x, uint32_t y, uint32_t z) { return x ^ y ^ z; }

    static constexpr inline uint32_t I(uint32_t x, uint32_t y, uint32_t z) { return y ^ (x | ~z); }

    static constexpr inline uint32_t R(uint32_t j, uint32_t m, uint32_t a)
    {
        return (j * m + a) & 15;
    }

    static inline void round(uint32_t h[4], const Block &m, uint32_t i)
    {
        static constexpr uint32_t SIN[64] = {
            0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee, 0xf57c0faf, 0x4787c62a, 0xa8304613,
            0xfd469501, 0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be, 0x6b901122, 0xfd987193,
            0xa679438e, 0x49b40821, 0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa, 0xd62f105d,
            0x2441453,  0xd8a1e681, 0xe7d3fbc8, 0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
            0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a, 0xfffa3942, 0x8771f681, 0x6d9d6122,
            0xfde5380c, 0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70, 0x289b7ec6, 0xeaa127fa,
            0xd4ef3085, 0x4881d05,  0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665, 0xf4292244,
            0x432aff97, 0xab9423a7, 0xfc93a039, 0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
            0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1, 0xf7537e82, 0xbd3af235, 0x2ad7d2bb,
            0xeb86d391};
        static constexpr uint8_t ROT[16] = {
            7, 12, 17, 22, //
            5, 9,  14, 20, //
            4, 11, 16, 23, //
            6, 10, 15, 21, //
        };
        static constexpr uint8_t MUL[4] = {1, 5, 3, 7};
        static constexpr uint8_t ADD[4] = {0, 1, 5, 0};

        uint32_t j = 3 - ((i + 3) & 3);
        uint32_t k = i >> 4;

        uint32_t &a = h[(j + 0) & 3];
        uint32_t b = h[(j + 1) & 3];
        uint32_t c = h[(j + 2) & 3];
        uint32_t d = h[(j + 3) & 3];
        uint32_t f = k == 0 ? F(b, c, d) : k == 1 ? G(b, c, d) : k == 2 ? H(b, c, d) : I(b, c, d);

        a = b + _rotl(a + f + m[R(i, MUL[k], ADD[k])] + SIN[i], ROT[k * 4 + (i & 3)]);
    }

    static void hash_padded(uint32_t h[4], const Block *m, uint64_t sz)
    {
        for (uint64_t i = 0; i < sz; ++i)
        {
            uint32_t old[4] = {h[0], h[1], h[2], h[3]};

#pragma unroll(64)
            for (uint32_t j = 0; j < 64; ++j)
                round(h, m[i], j);

            h[0] += old[0];
            h[1] += old[1];
            h[2] += old[2];
            h[3] += old[3];
        }
    }

#ifdef MD5_EXTRA_FUNCTIONS
    void hash_padded_naive(uint32_t h[4], const md5::Block *m, uint64_t sz)
    {
        static constexpr uint32_t SIN_T[64] = {
            0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee, 0xf57c0faf, 0x4787c62a, 0xa8304613,
            0xfd469501, 0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be, 0x6b901122, 0xfd987193,
            0xa679438e, 0x49b40821, 0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa, 0xd62f105d,
            0x2441453,  0xd8a1e681, 0xe7d3fbc8, 0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
            0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a, 0xfffa3942, 0x8771f681, 0x6d9d6122,
            0xfde5380c, 0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70, 0x289b7ec6, 0xeaa127fa,
            0xd4ef3085, 0x4881d05,  0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665, 0xf4292244,
            0x432aff97, 0xab9423a7, 0xfc93a039, 0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
            0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1, 0xf7537e82, 0xbd3af235, 0x2ad7d2bb,
            0xeb86d391};
        static constexpr uint8_t SHIFT_T[64] = {
            7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, //
            5, 9,  14, 20, 5, 9,  14, 20, 5, 9,  14, 20, 5, 9,  14, 20, //
            4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, //
            6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, //
        };

        h[0] = 0x67452301;
        h[1] = 0xEFCDAB89;
        h[2] = 0x98BADCFE;
        h[3] = 0x10325476;

        for (size_t i = 0; i < sz; ++i)
        {
            uint32_t a = h[0], b = h[1], c = h[2], d = h[3];

            for (uint32_t j = 0; j < 64; ++j)
            {
                uint32_t f, g;
                if (j < 16)
                {
                    f = F(b, c, d);
                    g = j;
                }
                else if (j < 32)
                {
                    f = G(b, c, d);
                    g = (j * 5 + 1) % 16;
                }
                else if (j < 48)
                {
                    f = H(b, c, d);
                    g = (j * 3 + 5) % 16;
                }
                else
                {
                    f = I(b, c, d);
                    g = (j * 7) % 16;
                }

                f = f + a + SIN_T[j] + m[i][g];
                a = d;
                d = c;
                c = b;
                b += _rotl(f, SHIFT_T[j]);
            }
            h[0] += a;
            h[1] += b;
            h[2] += c;
            h[3] += d;
        }
    }

    template<Transf foo> static inline void round2(uint32_t h[4], const Block &m, uint32_t i)
    {
        static constexpr uint32_t SIN[64] = {
            0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee, 0xf57c0faf, 0x4787c62a, 0xa8304613,
            0xfd469501, 0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be, 0x6b901122, 0xfd987193,
            0xa679438e, 0x49b40821, 0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa, 0xd62f105d,
            0x2441453,  0xd8a1e681, 0xe7d3fbc8, 0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
            0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a, 0xfffa3942, 0x8771f681, 0x6d9d6122,
            0xfde5380c, 0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70, 0x289b7ec6, 0xeaa127fa,
            0xd4ef3085, 0x4881d05,  0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665, 0xf4292244,
            0x432aff97, 0xab9423a7, 0xfc93a039, 0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
            0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1, 0xf7537e82, 0xbd3af235, 0x2ad7d2bb,
            0xeb86d391};
        static constexpr uint8_t ROT[16] = {
            7, 12, 17, 22, //
            5, 9,  14, 20, //
            4, 11, 16, 23, //
            6, 10, 15, 21, //
        };
        static constexpr uint8_t MUL[4] = {1, 5, 3, 7};
        static constexpr uint8_t ADD[4] = {0, 1, 5, 0};

        uint32_t j = 3 - ((i + 3) & 3);
        uint32_t k = i / 16;

        uint32_t &a = h[(j + 0) & 3];
        uint32_t b = h[(j + 1) & 3];
        uint32_t c = h[(j + 2) & 3];
        uint32_t d = h[(j + 3) & 3];
        uint32_t f = foo(b, c, d);

        a = b + _rotl(a + f + m[R(i, MUL[k], ADD[k])] + SIN[i], ROT[k * 4 + (i & 3)]);
    }

    void hash_padded2(uint32_t h[4], const Block *m, uint64_t sz)
    {
        for (uint64_t i = 0; i < sz; ++i)
        {
            uint32_t old[4] = {h[0], h[1], h[2], h[3]};

            for (uint32_t j = 0; j < 16; ++j)
                round2<F>(h, m[i], j);
            for (uint32_t j = 16; j < 32; ++j)
                round2<G>(h, m[i], j);
            for (uint32_t j = 32; j < 48; ++j)
                round2<H>(h, m[i], j);
            for (uint32_t j = 48; j < 64; ++j)
                round2<I>(h, m[i], j);

            h[0] += old[0];
            h[1] += old[1];
            h[2] += old[2];
            h[3] += old[3];
        }
    }

    static inline uint32_t round3(uint32_t a, uint32_t b, uint32_t c, uint32_t d, Transf foo,
                                  uint32_t x, uint32_t r, uint32_t t)
    {
        return b + _rotl(a + foo(b, c, d) + x + t, r);
    }

    void hash_padded3(uint32_t h[4], const Block *m, uint64_t sz, const Block pad[2],
                      uint64_t pad_sz)
    {
        static constexpr uint32_t SIN_T[64] = {
            0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee, 0xf57c0faf, 0x4787c62a, 0xa8304613,
            0xfd469501, 0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be, 0x6b901122, 0xfd987193,
            0xa679438e, 0x49b40821, 0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa, 0xd62f105d,
            0x2441453,  0xd8a1e681, 0xe7d3fbc8, 0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
            0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a, 0xfffa3942, 0x8771f681, 0x6d9d6122,
            0xfde5380c, 0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70, 0x289b7ec6, 0xeaa127fa,
            0xd4ef3085, 0x4881d05,  0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665, 0xf4292244,
            0x432aff97, 0xab9423a7, 0xfc93a039, 0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
            0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1, 0xf7537e82, 0xbd3af235, 0x2ad7d2bb,
            0xeb86d391};

        h[0] = 0x67452301;
        h[1] = 0xEFCDAB89;
        h[2] = 0x98BADCFE;
        h[3] = 0x10325476;

        for (uint64_t i = 0; i < sz; ++i)
        {
            uint32_t old[4] = {h[0], h[1], h[2], h[3]};

            // F
            h[0] = round3(h[0], h[1], h[2], h[3], F, m[i][0], 7, SIN_T[0]);
            h[3] = round3(h[3], h[0], h[1], h[2], F, m[i][1], 12, SIN_T[1]);
            h[2] = round3(h[2], h[3], h[0], h[1], F, m[i][2], 17, SIN_T[2]);
            h[1] = round3(h[1], h[2], h[3], h[0], F, m[i][3], 22, SIN_T[3]);

            h[0] = round3(h[0], h[1], h[2], h[3], F, m[i][4], 7, SIN_T[4]);
            h[3] = round3(h[3], h[0], h[1], h[2], F, m[i][5], 12, SIN_T[5]);
            h[2] = round3(h[2], h[3], h[0], h[1], F, m[i][6], 17, SIN_T[6]);
            h[1] = round3(h[1], h[2], h[3], h[0], F, m[i][7], 22, SIN_T[7]);

            h[0] = round3(h[0], h[1], h[2], h[3], F, m[i][8], 7, SIN_T[8]);
            h[3] = round3(h[3], h[0], h[1], h[2], F, m[i][9], 12, SIN_T[9]);
            h[2] = round3(h[2], h[3], h[0], h[1], F, m[i][10], 17, SIN_T[10]);
            h[1] = round3(h[1], h[2], h[3], h[0], F, m[i][11], 22, SIN_T[11]);

            h[0] = round3(h[0], h[1], h[2], h[3], F, m[i][12], 7, SIN_T[12]);
            h[3] = round3(h[3], h[0], h[1], h[2], F, m[i][13], 12, SIN_T[13]);
            h[2] = round3(h[2], h[3], h[0], h[1], F, m[i][14], 17, SIN_T[14]);
            h[1] = round3(h[1], h[2], h[3], h[0], F, m[i][15], 22, SIN_T[15]);

            // G
            h[0] = round3(h[0], h[1], h[2], h[3], G, m[i][1], 5, SIN_T[16]);
            h[3] = round3(h[3], h[0], h[1], h[2], G, m[i][6], 9, SIN_T[17]);
            h[2] = round3(h[2], h[3], h[0], h[1], G, m[i][11], 14, SIN_T[18]);
            h[1] = round3(h[1], h[2], h[3], h[0], G, m[i][0], 20, SIN_T[19]);

            h[0] = round3(h[0], h[1], h[2], h[3], G, m[i][5], 5, SIN_T[20]);
            h[3] = round3(h[3], h[0], h[1], h[2], G, m[i][10], 9, SIN_T[21]);
            h[2] = round3(h[2], h[3], h[0], h[1], G, m[i][15], 14, SIN_T[22]);
            h[1] = round3(h[1], h[2], h[3], h[0], G, m[i][4], 20, SIN_T[23]);

            h[0] = round3(h[0], h[1], h[2], h[3], G, m[i][9], 5, SIN_T[24]);
            h[3] = round3(h[3], h[0], h[1], h[2], G, m[i][14], 9, SIN_T[25]);
            h[2] = round3(h[2], h[3], h[0], h[1], G, m[i][3], 14, SIN_T[26]);
            h[1] = round3(h[1], h[2], h[3], h[0], G, m[i][8], 20, SIN_T[27]);

            h[0] = round3(h[0], h[1], h[2], h[3], G, m[i][13], 5, SIN_T[28]);
            h[3] = round3(h[3], h[0], h[1], h[2], G, m[i][2], 9, SIN_T[29]);
            h[2] = round3(h[2], h[3], h[0], h[1], G, m[i][7], 14, SIN_T[30]);
            h[1] = round3(h[1], h[2], h[3], h[0], G, m[i][12], 20, SIN_T[31]);

            // H
            h[0] = round3(h[0], h[1], h[2], h[3], H, m[i][5], 4, SIN_T[32]);
            h[3] = round3(h[3], h[0], h[1], h[2], H, m[i][8], 11, SIN_T[33]);
            h[2] = round3(h[2], h[3], h[0], h[1], H, m[i][11], 16, SIN_T[34]);
            h[1] = round3(h[1], h[2], h[3], h[0], H, m[i][14], 23, SIN_T[35]);

            h[0] = round3(h[0], h[1], h[2], h[3], H, m[i][1], 4, SIN_T[36]);
            h[3] = round3(h[3], h[0], h[1], h[2], H, m[i][4], 11, SIN_T[37]);
            h[2] = round3(h[2], h[3], h[0], h[1], H, m[i][7], 16, SIN_T[38]);
            h[1] = round3(h[1], h[2], h[3], h[0], H, m[i][10], 23, SIN_T[39]);

            h[0] = round3(h[0], h[1], h[2], h[3], H, m[i][13], 4, SIN_T[40]);
            h[3] = round3(h[3], h[0], h[1], h[2], H, m[i][0], 11, SIN_T[41]);
            h[2] = round3(h[2], h[3], h[0], h[1], H, m[i][3], 16, SIN_T[42]);
            h[1] = round3(h[1], h[2], h[3], h[0], H, m[i][6], 23, SIN_T[43]);

            h[0] = round3(h[0], h[1], h[2], h[3], H, m[i][9], 4, SIN_T[44]);
            h[3] = round3(h[3], h[0], h[1], h[2], H, m[i][12], 11, SIN_T[45]);
            h[2] = round3(h[2], h[3], h[0], h[1], H, m[i][15], 16, SIN_T[46]);
            h[1] = round3(h[1], h[2], h[3], h[0], H, m[i][2], 23, SIN_T[47]);

            // I
            h[0] = round3(h[0], h[1], h[2], h[3], I, m[i][0], 6, SIN_T[48]);
            h[3] = round3(h[3], h[0], h[1], h[2], I, m[i][7], 10, SIN_T[49]);
            h[2] = round3(h[2], h[3], h[0], h[1], I, m[i][14], 15, SIN_T[50]);
            h[1] = round3(h[1], h[2], h[3], h[0], I, m[i][5], 21, SIN_T[51]);

            h[0] = round3(h[0], h[1], h[2], h[3], I, m[i][12], 6, SIN_T[52]);
            h[3] = round3(h[3], h[0], h[1], h[2], I, m[i][3], 10, SIN_T[53]);
            h[2] = round3(h[2], h[3], h[0], h[1], I, m[i][10], 15, SIN_T[54]);
            h[1] = round3(h[1], h[2], h[3], h[0], I, m[i][1], 21, SIN_T[55]);

            h[0] = round3(h[0], h[1], h[2], h[3], I, m[i][8], 6, SIN_T[56]);
            h[3] = round3(h[3], h[0], h[1], h[2], I, m[i][15], 10, SIN_T[57]);
            h[2] = round3(h[2], h[3], h[0], h[1], I, m[i][6], 15, SIN_T[58]);
            h[1] = round3(h[1], h[2], h[3], h[0], I, m[i][13], 21, SIN_T[59]);

            h[0] = round3(h[0], h[1], h[2], h[3], I, m[i][4], 6, SIN_T[60]);
            h[3] = round3(h[3], h[0], h[1], h[2], I, m[i][11], 10, SIN_T[61]);
            h[2] = round3(h[2], h[3], h[0], h[1], I, m[i][2], 15, SIN_T[62]);
            h[1] = round3(h[1], h[2], h[3], h[0], I, m[i][9], 21, SIN_T[63]);

            h[0] += old[0];
            h[1] += old[1];
            h[2] += old[2];
            h[3] += old[3];
        }

        for (uint64_t i = 0; i < pad_sz; ++i)
        {
            uint32_t old[4] = {h[0], h[1], h[2], h[3]};

            // F
            h[0] = round3(h[0], h[1], h[2], h[3], F, pad[i][0], 7, SIN_T[0]);
            h[3] = round3(h[3], h[0], h[1], h[2], F, pad[i][1], 12, SIN_T[1]);
            h[2] = round3(h[2], h[3], h[0], h[1], F, pad[i][2], 17, SIN_T[2]);
            h[1] = round3(h[1], h[2], h[3], h[0], F, pad[i][3], 22, SIN_T[3]);

            h[0] = round3(h[0], h[1], h[2], h[3], F, pad[i][4], 7, SIN_T[4]);
            h[3] = round3(h[3], h[0], h[1], h[2], F, pad[i][5], 12, SIN_T[5]);
            h[2] = round3(h[2], h[3], h[0], h[1], F, pad[i][6], 17, SIN_T[6]);
            h[1] = round3(h[1], h[2], h[3], h[0], F, pad[i][7], 22, SIN_T[7]);

            h[0] = round3(h[0], h[1], h[2], h[3], F, pad[i][8], 7, SIN_T[8]);
            h[3] = round3(h[3], h[0], h[1], h[2], F, pad[i][9], 12, SIN_T[9]);
            h[2] = round3(h[2], h[3], h[0], h[1], F, pad[i][10], 17, SIN_T[10]);
            h[1] = round3(h[1], h[2], h[3], h[0], F, pad[i][11], 22, SIN_T[11]);

            h[0] = round3(h[0], h[1], h[2], h[3], F, pad[i][12], 7, SIN_T[12]);
            h[3] = round3(h[3], h[0], h[1], h[2], F, pad[i][13], 12, SIN_T[13]);
            h[2] = round3(h[2], h[3], h[0], h[1], F, pad[i][14], 17, SIN_T[14]);
            h[1] = round3(h[1], h[2], h[3], h[0], F, pad[i][15], 22, SIN_T[15]);

            // G
            h[0] = round3(h[0], h[1], h[2], h[3], G, pad[i][1], 5, SIN_T[16]);
            h[3] = round3(h[3], h[0], h[1], h[2], G, pad[i][6], 9, SIN_T[17]);
            h[2] = round3(h[2], h[3], h[0], h[1], G, pad[i][11], 14, SIN_T[18]);
            h[1] = round3(h[1], h[2], h[3], h[0], G, pad[i][0], 20, SIN_T[19]);

            h[0] = round3(h[0], h[1], h[2], h[3], G, pad[i][5], 5, SIN_T[20]);
            h[3] = round3(h[3], h[0], h[1], h[2], G, pad[i][10], 9, SIN_T[21]);
            h[2] = round3(h[2], h[3], h[0], h[1], G, pad[i][15], 14, SIN_T[22]);
            h[1] = round3(h[1], h[2], h[3], h[0], G, pad[i][4], 20, SIN_T[23]);

            h[0] = round3(h[0], h[1], h[2], h[3], G, pad[i][9], 5, SIN_T[24]);
            h[3] = round3(h[3], h[0], h[1], h[2], G, pad[i][14], 9, SIN_T[25]);
            h[2] = round3(h[2], h[3], h[0], h[1], G, pad[i][3], 14, SIN_T[26]);
            h[1] = round3(h[1], h[2], h[3], h[0], G, pad[i][8], 20, SIN_T[27]);

            h[0] = round3(h[0], h[1], h[2], h[3], G, pad[i][13], 5, SIN_T[28]);
            h[3] = round3(h[3], h[0], h[1], h[2], G, pad[i][2], 9, SIN_T[29]);
            h[2] = round3(h[2], h[3], h[0], h[1], G, pad[i][7], 14, SIN_T[30]);
            h[1] = round3(h[1], h[2], h[3], h[0], G, pad[i][12], 20, SIN_T[31]);

            // H
            h[0] = round3(h[0], h[1], h[2], h[3], H, pad[i][5], 4, SIN_T[32]);
            h[3] = round3(h[3], h[0], h[1], h[2], H, pad[i][8], 11, SIN_T[33]);
            h[2] = round3(h[2], h[3], h[0], h[1], H, pad[i][11], 16, SIN_T[34]);
            h[1] = round3(h[1], h[2], h[3], h[0], H, pad[i][14], 23, SIN_T[35]);

            h[0] = round3(h[0], h[1], h[2], h[3], H, pad[i][1], 4, SIN_T[36]);
            h[3] = round3(h[3], h[0], h[1], h[2], H, pad[i][4], 11, SIN_T[37]);
            h[2] = round3(h[2], h[3], h[0], h[1], H, pad[i][7], 16, SIN_T[38]);
            h[1] = round3(h[1], h[2], h[3], h[0], H, pad[i][10], 23, SIN_T[39]);

            h[0] = round3(h[0], h[1], h[2], h[3], H, pad[i][13], 4, SIN_T[40]);
            h[3] = round3(h[3], h[0], h[1], h[2], H, pad[i][0], 11, SIN_T[41]);
            h[2] = round3(h[2], h[3], h[0], h[1], H, pad[i][3], 16, SIN_T[42]);
            h[1] = round3(h[1], h[2], h[3], h[0], H, pad[i][6], 23, SIN_T[43]);

            h[0] = round3(h[0], h[1], h[2], h[3], H, pad[i][9], 4, SIN_T[44]);
            h[3] = round3(h[3], h[0], h[1], h[2], H, pad[i][12], 11, SIN_T[45]);
            h[2] = round3(h[2], h[3], h[0], h[1], H, pad[i][15], 16, SIN_T[46]);
            h[1] = round3(h[1], h[2], h[3], h[0], H, pad[i][2], 23, SIN_T[47]);

            // I
            h[0] = round3(h[0], h[1], h[2], h[3], I, pad[i][0], 6, SIN_T[48]);
            h[3] = round3(h[3], h[0], h[1], h[2], I, pad[i][7], 10, SIN_T[49]);
            h[2] = round3(h[2], h[3], h[0], h[1], I, pad[i][14], 15, SIN_T[50]);
            h[1] = round3(h[1], h[2], h[3], h[0], I, pad[i][5], 21, SIN_T[51]);

            h[0] = round3(h[0], h[1], h[2], h[3], I, pad[i][12], 6, SIN_T[52]);
            h[3] = round3(h[3], h[0], h[1], h[2], I, pad[i][3], 10, SIN_T[53]);
            h[2] = round3(h[2], h[3], h[0], h[1], I, pad[i][10], 15, SIN_T[54]);
            h[1] = round3(h[1], h[2], h[3], h[0], I, pad[i][1], 21, SIN_T[55]);

            h[0] = round3(h[0], h[1], h[2], h[3], I, pad[i][8], 6, SIN_T[56]);
            h[3] = round3(h[3], h[0], h[1], h[2], I, pad[i][15], 10, SIN_T[57]);
            h[2] = round3(h[2], h[3], h[0], h[1], I, pad[i][6], 15, SIN_T[58]);
            h[1] = round3(h[1], h[2], h[3], h[0], I, pad[i][13], 21, SIN_T[59]);

            h[0] = round3(h[0], h[1], h[2], h[3], I, pad[i][4], 6, SIN_T[60]);
            h[3] = round3(h[3], h[0], h[1], h[2], I, pad[i][11], 10, SIN_T[61]);
            h[2] = round3(h[2], h[3], h[0], h[1], I, pad[i][2], 15, SIN_T[62]);
            h[1] = round3(h[1], h[2], h[3], h[0], I, pad[i][9], 21, SIN_T[63]);

            h[0] += old[0];
            h[1] += old[1];
            h[2] += old[2];
            h[3] += old[3];
        }
    }

#endif

    void hash(void *digest, const void *m, uint64_t sz)
    {
        Block pad[2]{};
        uint64_t rem_sz = sz % sizeof(Block);
        uint64_t blk_sz = sz / sizeof(Block) - (sz >= sizeof(Block));
        uint64_t off = ((rem_sz == 0) & (blk_sz != 0)) * sizeof(Block);
        uint64_t pad_sz = sizeof(Block) + ((rem_sz >= BLOCK_MOD) | (off != 0)) * sizeof(Block);
        uint32_t *dig = (uint32_t *)digest;

        dig[0] = 0x67452301;
        dig[1] = 0xEFCDAB89;
        dig[2] = 0x98BADCFE;
        dig[3] = 0x10325476;
        memcpy(pad, (Block *)m + blk_sz, rem_sz + off);

        ((uint8_t *)pad)[rem_sz + off] = 0x80;
        ((uint64_t *)pad)[pad_sz / sizeof(uint64_t) - 1] = sz * CHAR_BIT;


        hash_padded(dig, (Block *)m, blk_sz);
        hash_padded(dig, pad, pad_sz / sizeof(Block));
    }
} // namespace md5
