#include "th09/th09.hpp"
#include "tc09/tc09.hpp"
#include <cstring>

namespace crypto::th09
{
    void hash(void *digest, const void *message, size_t sz)
    {
        uint64_t dig = 0;

        if (!sz)
        {
            *(uint64_t *)digest = tc09::enc2(dig, FIX_KEY);
            return;
        }

        uint64_t *msg = (uint64_t *)message;
        size_t sz_div = sz / sizeof(uint64_t);
        size_t sz_rem = sz % sizeof(uint64_t);
        uint64_t pad = 0;

        for (size_t i = 0; i < sz_div; ++i)
            dig = tc09::enc2(msg[i] ^ dig, FIX_KEY);

        if (sz_rem)
        {
            memcpy(&pad, (const char *)message + sz - sz_rem, sz_rem);
            dig = tc09::enc2(pad ^ dig, FIX_KEY);
        }

        *(uint64_t *)digest = dig;
    }
} // namespace crypto::th09
