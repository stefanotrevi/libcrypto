#include "reverse.hpp"

#include <climits>

namespace reverse
{
    template<typename T> static constexpr size_t bsizeof() noexcept { return sizeof(T) * CHAR_BIT; }

/*
    static uint8_t reverse1(uint8_t x)
    {
        x = (uint8_t)((x & 0x55) << 1 | (x & ~0x55) >> 1);
        x = (uint8_t)((x & 0x33) << 2 | (x & ~0x33) >> 2);
        x = (uint8_t)((x & 0x0F) << 4 | (x & ~0x0F) >> 4);

        return x;
    }
*/
    uint8_t reverse(uint8_t x)
    {
        return (uint8_t)_pext_u64(_bswap64(_pdep_u64(x, 0x0101010101010101)), 0x0101010101010101);
    }

    uint16_t reverse(uint16_t x)
    {
        x = (uint16_t)((x & 0x5555) << 1 | (x & ~0x5555) >> 1);
        x = (uint16_t)((x & 0x3333) << 2 | (x & ~0x3333) >> 2);
        x = (uint16_t)((x & 0x0F0F) << 4 | (x & ~0x0F0F) >> 4);

        return x;
    }

    uint32_t reverse(uint32_t x)
    {
        x = (x & 0x55555555) << 1 | (x & ~0x55555555) >> 1;
        x = (x & 0x33333333) << 2 | (x & ~0x33333333) >> 2;
        x = (x & 0x0F0F0F0F) << 4 | (x & ~0x0F0F0F0F) >> 4;

        return x;
    }

    uint64_t reverse(uint64_t x)
    {
        x = (x & 0x5555555555555555) << 1 | (x & ~0x5555555555555555) >> 1;
        x = (x & 0x3333333333333333) << 2 | (x & ~0x3333333333333333) >> 2;
        x = (x & 0x0F0F0F0F0F0F0F0F) << 4 | (x & ~0x0F0F0F0F0F0F0F0F) >> 4;

        return x;
    }

/*
    static __m256i reverse_avx1(__m256i x)
    {
        __m256i y, m;

        m = _mm256_set1_epi8(0x55);
        y = _mm256_slli_epi64(_mm256_and_si256(m, x), 1);
        x = _mm256_srli_epi64(_mm256_andnot_si256(m, x), 1);
        x = _mm256_or_si256(x, y);

        m = _mm256_set1_epi8(0x33);
        y = _mm256_slli_epi64(_mm256_and_si256(m, x), 2);
        x = _mm256_srli_epi64(_mm256_andnot_si256(m, x), 2);
        x = _mm256_or_si256(x, y);

        m = _mm256_set1_epi8(0x0F);
        y = _mm256_slli_epi64(_mm256_and_si256(m, x), 4);
        x = _mm256_srli_epi64(_mm256_andnot_si256(m, x), 4);
        x = _mm256_or_si256(x, y);

        return x;
    }
*/

    // faster than avx2 on single call (2 bitwise-and < 1 load)
    __m256i reverse_avx2(__m256i x)
    {
        __m256i y;
        __m256i m1 = _mm256_set1_epi8(0x0F);
        __m256i m2 = _mm256_set_epi64x(0xFF77BB33DD559911, 0xEE66AA22CC448800, //
                                       0xFF77BB33DD559911, 0xEE66AA22CC448800);

        y = _mm256_and_si256(m1, x);
        x = _mm256_andnot_si256(m1, x);
        x = _mm256_srli_epi64(x, 4);


        x = _mm256_shuffle_epi8(m2, x);
        y = _mm256_shuffle_epi8(m2, y);

        y = _mm256_andnot_si256(m1, y);
        x = _mm256_and_si256(m1, x);
        x = _mm256_or_si256(x, y);

        return x;
    }

    // faster than avx and also than avx2 when used in loops
    static __m256i reverse(__m256i x)
    {
        __m256i y, m;

        m = _mm256_set1_epi8(0x0F);
        y = _mm256_and_si256(m, x);
        x = _mm256_andnot_si256(m, x);
        x = _mm256_srli_epi64(x, 4);

        m = _mm256_set_epi64x(0x0F070B030D050901, 0x0E060A020C040800, //
                              0x0F070B030D050901, 0x0E060A020C040800);
        x = _mm256_shuffle_epi8(m, x);

        m = _mm256_set_epi64x(0x0F070B030D050901 << 4, 0x0E060A020C040800 << 4, //
                              0x0F070B030D050901 << 4, 0x0E060A020C040800 << 4);
        y = _mm256_shuffle_epi8(m, y);

        x = _mm256_or_si256(x, y);

        return x;
    }

    void reverse(uint8_t *arr, size_t sz)
    {
        __m256i *v = (__m256i *)arr;
        size_t rem = sz % sizeof(*v);

        for (__m256i *v_end = (__m256i *)arr + sz / sizeof(*v); v < v_end; ++v)
            _mm256_storeu_si256(v, reverse(_mm256_loadu_si256(v)));

        for (size_t i = sz - rem; i < sz; ++i)
            arr[i] = reverse(arr[i]);
    }
} // namespace reverse
