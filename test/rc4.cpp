#include "rc4/rc4.hpp"
#include "measure.hpp"
#include "string_utils.hpp"
#include <array>
#include <climits>
#include <iostream>

namespace rc4 = crypto::rc4;

static bool run_tests()
{
    std::array<uint8_t, 128 / CHAR_BIT> msg{};
    std::array<uint8_t, 128 / CHAR_BIT> tmp{};
    std::array<std::array<uint8_t, 128 / CHAR_BIT>, 4> cyp{
        "dd5bcb0018e922d494759d7c395d02d3"_x,
        "c8446f8f77abf737685353eb89a1c9eb"_x,
        "af3e30f9c095045938151575c3fb9098"_x,
        "f8cb6274db99b80b1d2012a98ed48f0e"_x,
    };
    auto key{"1ada31d5cf688221c109163908ebe51debb46227c6cc8b37641910833222772a"_x};
    std::array<size_t, 4> off{0, 16, 240, 256};
    bool check = true;
    bool all_check = true;

    std::cout << std::boolalpha;

    std::cout << "Encryption... ";
    check = true;
    for (size_t i = 0; i < cyp.size(); ++i)
    {
        rc4::enc(tmp.data(), msg.data(), msg.size(), key.data(), key.size(), off[i]);
        check &= tmp == cyp[i];
    }
    std::cout << check << '\n';
    all_check &= check;

    std::cout << "Decryption... ";
    check = true;
    for (size_t i = 0; i < cyp.size(); ++i)
    {
        rc4::dec(tmp.data(), cyp[i].data(), cyp[i].size(), key.data(), key.size(), off[i]);
        check &= std::ranges::all_of(tmp, [](auto &&x) { return x == 0; });
    }
    std::cout << check << '\n';
    all_check &= check;


    return all_check;
}

int main()
{
    std::cout << "\n==== Testing RC4 implementation ====\n";

    bool all_check = run_tests();

#ifdef MEASURE_PERFORMANCE
    std::cout << "Measuring performance...\n";
    measure([]() { uint8_t c[32], m[32], k[32]; rc4::enc(c, m, sizeof(m), k, sizeof(k), 0); }, 1 << 20, 10);
#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    return 0;
}
