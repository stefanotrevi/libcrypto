#include "th07/th07.hpp"
#include "measure.hpp"
#include <array>

namespace th07 = crypto::th07;

template<size_t sz> using Array = std::array<std::string, sz>;

static bool run_tests()
{
    Array<4> msg = {"", "hello", "Hello there!", "General Kenobi! You're a bold one!"};
    Array<4> dig = {"e1e159592525b8b8", "aa26526bc7bdf84d", "8f410370f3268c31", "363a8f571203b96d"};
    bool check = true;
    bool all_check = true;

    std::cout << std::boolalpha;
    std::cout << "Hashing... ";
    for (size_t i = 0; i < msg.size(); ++i)
        check &= (std::string)th07::Hash{msg[i]} == dig[i];
    std::cout << check << '\n';
    all_check &= check;

    return all_check;
}

int main()
{
    std::cout << "\n==== Testing TH07 implementation ====\n";

    bool all_check = run_tests();

#ifdef MEASURE_PERFORMANCE
    std::cout << "Measuring hashing performance...\n";
    measure(
        []()
        {
            uint64_t dig;
            th07::hash(&dig, "mess", sizeof("mess"));
        },
        1 << 25, 5);
#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";
    return 0;
}
