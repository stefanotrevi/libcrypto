#include "klein/klein.hpp"
#include "klein/cu_klein.hpp"
#include "measure.hpp"
#include <array>
#include <iostream>

template<size_t sz> using Array = std::array<uint64_t, sz>;

namespace klein = crypto::klein;

namespace cu_klein = cu::crypto::klein;

static bool run_tests()
{
    Array<4> key{0x0000000000000000, 0xFFFFFFFFFFFFFFFF, 0x1234567890ABCDEF, 0x0000000000000000};
    Array<4> msg{0xFFFFFFFFFFFFFFFF, 0x0000000000000000, 0xFFFFFFFFFFFFFFFF, 0x1234567890ABCDEF};
    Array<4> cyp{0xCDC0B51F14722BBE, 0x6456764E8602E154, 0x592356C4997176C8, 0x629F9D6DFF95800E};
    bool check = true;
    bool all_check = true;

    std::cout << std::boolalpha;

    std::cout << "Encryption... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
        check &= klein::enc2(msg[i], key[i]) == cyp[i];
    std::cout << check << '\n';
    all_check &= check;


    std::cout << "Decryption... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
        check &= klein::dec2(cyp[i], key[i]) == msg[i];
    std::cout << check << '\n';
    all_check &= check;


    std::cout << "Key schedule... ";
    check = true;
    {
        auto scheduled{cyp};
        for (auto &k : scheduled)
        {
            k = klein::sched(k, klein::ROUNDS);
            k = klein::desched(k, klein::ROUNDS);
        }
        check = scheduled == cyp;
    }
    std::cout << check << '\n';
    all_check &= check;


    std::cout << "Round inversion... ";
    check = true;
    {
        auto mixed{cyp};
        for (auto &m : mixed)
        {
            m = klein::round(m);
            m = klein::round_inv(m);
        }
        check = mixed == cyp;
    }
    std::cout << check << '\n';
    all_check &= check;

#ifdef USE_CUDA
    std::cout << "CUDA encryption... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
        check &= cu_klein::test_enc(msg[i], key[i]) == cyp[i];
    std::cout << check << '\n';
    all_check &= check;


    std::cout << "CUDA decryption (no scheduling)... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
    {
        uint64_t k = klein::sched(key[i], klein::ROUNDS);
        check &= cu_klein::test_dec_nosched(cyp[i], k) == msg[i];
    }
    std::cout << check << '\n';
    all_check &= check;
#endif

    return all_check;
}

int main()
{
    std::cout << "\n==== Testing KLEIN implementation ====\n";

    bool all_check = run_tests();
#ifdef MEASURE_PERFORMANCE
    std::cout << "Measuring performance...\n";
    measure([](size_t i) { klein::enc(i, i); }, 1 << 20, 5, "Encryption");
    measure([](size_t i) { klein::enc2(i, i); }, 1 << 20, 5, "Optimized encryption");
 
    #ifdef USE_CUDA
    measure([](size_t i) { cu_klein::crack(i, i, klein::ROUNDS, 1 << 28); }, 1, 5,
            "CUDA brute forcce");
    #endif

#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    return 0;
}
