#include "aes/aes.hpp"
#include "measure.hpp"
#include "string_utils.hpp"
#include <iostream>
#include <ippcp.h>
#include <numeric>
#include <random>

#ifndef _WIN32
    #include <openssl/aes.h>
#endif

int main()
{
    static constexpr size_t BUF_SIZE = 1ULL << 30;

    std::mt19937_64 rng{std::random_device{}()};

    std::vector<uint8_t> msg(BUF_SIZE);
    std::vector<uint8_t> cip(BUF_SIZE);
    std::iota(msg.begin(), msg.end(), 0);

    auto key = "2b7e151628aed2a6abf7158809cf4f3c"_x;
    auto nonce = "f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff"_x;
    double elap;

    elap = measure(
        [&]()
        {
            // define and setup AES cipher
            int ctxSize = 0;
            ippsAESGetSize(&ctxSize);
            IppsAESSpec *pAES = (IppsAESSpec *)(new Ipp8u[ctxSize]);
            ippsAESInit(key.data(), key.size(), pAES, ctxSize);

            auto my_nonce = nonce;

            // encryption
            ippsAESEncryptCTR(msg.data(), msg.data(), msg.size(), pAES, my_nonce.data(), 64);

            // remove secret and release resource
            ippsAESInit(0, key.size(), pAES, ctxSize);

            delete[] (Ipp8u *)pAES;
        },
        1, 10, "Intel IPP", false);
    std::cout << "Encryption speed (Intel CTR): " << (BUF_SIZE / elap / 1000) << " MB/s\n";

#ifndef _WIN32
    elap = measure(
        [&]()
        {
            AES_KEY aes_key;
            AES_set_encrypt_key(key.data(), key.size() * 8, &aes_key);

            auto my_nonce = nonce;

            AES_cbc_encrypt(msg.data(), msg.data(), msg.size(), &aes_key, my_nonce.data(),
                            AES_ENCRYPT);
        },
        1, 1, "OpenSSL CTR", false);
    std::cout << "Encryption speed (OpenSSL CBC): " << (BUF_SIZE / elap / 1000) << " MB/s\n";
#endif
    elap = measure(
        [&]()
        { crypto::aes::enc3_ctr(msg.data(), msg.data(), msg.size(), key.data(), nonce.data()); }, 1,
        10, "enc3_ctr", false);
    std::cout << "Encryption speed (CTR): " << (BUF_SIZE / elap / 1000) << " MB/s\n";

    elap = measure(
        [&]()
        { crypto::aes::enc3_ctr256(msg.data(), msg.data(), msg.size(), key.data(), nonce.data()); },
        1, 10, "enc3_ctr512", false);

    std::cout << "Encryption speed (CTR-AVX2): " << (BUF_SIZE / elap / 1000) << " MB/s\n";

    std::cout << std::boolalpha;

    return 0;
}
