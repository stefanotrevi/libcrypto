#include "sha256/sha256.hpp"
#include "measure.hpp"
#include "sha256/cu_sha256.hpp"
#include <fstream>
#include <iostream>
#include <string>

namespace sha256 = crypto::sha256;
namespace cu_sha256 = cu::crypto::sha256;

static bool run_tests()
{
    static constexpr const char msg[] = "abc";
    static constexpr const char *digest =
        "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad";
    static constexpr const char *vlc_digest =
        "409e0cb6f80c840aefa6f7f48d168b3cce63eb1bb1a67b44929f6d6dd1bc8fe5";

    bool check = true;
    bool all_check = true;

    std::cout << std::boolalpha;

    std::cout << "\n==== Testing SHA256 implementation ====\n";

    std::cout << "Hashing... ";
    sha256::Hash hash{msg};
    check = (std::string)hash == digest;
    std::cout << check << '\n';
    all_check &= check;

    std::cout << "Hashing one block... ";

    char my_digest[sha256::DIGEST_SIZE] = {0};

    sha256::hash_oneblock(my_digest, msg, sizeof(msg) - 1);

    check = hexdump(my_digest) == digest;
    std::cout << check << '\n';
    all_check &= check;

    std::cout << "VLC Hashing... ";
    std::ifstream vlc_file{
        "C:/Users/stefa/Downloads/vlc-3.0.19-win64.exe",
        std::ios::binary};
    std::vector<uint8_t> vlc_data{std::istreambuf_iterator{vlc_file}, {}};

    hash = sha256::Hash{vlc_data};
    std::cout << hash << '\n';
    check = (std::string)hash == vlc_digest;
    std::cout << check << '\n';
    all_check &= check;

#ifdef USE_CUDA
    uint8_t dig[sha256::DIGEST_SIZE];

    std::cout << "CUDA one block hashing... ";
    check = true;
    cu_sha256::test(dig, msg, sizeof(msg) - 1);
    check = hexdump(dig) == digest;
    std::cout << check << '\n';
    all_check &= check;
#endif

    return all_check;
}

int main()
{
    bool all_check = run_tests();

#ifdef MEASURE_PERFORMANCE
    uint8_t res[sha256::DIGEST_SIZE];
    std::cout << "Measuring performance...\n";
    measure([&]() { sha256::Hash("hello"); }, 1 << 18, 5, "sha256");

    #ifdef USE_CUDA
    std::cout << "Measuring CUDA performance...\n";
    measure([&]()
            { cu_sha256::crack_oneblock(res, sha256::Hash{"passwd"}.data(), 0, ~0, 0, true); });
    #endif
#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    return 0;
}
