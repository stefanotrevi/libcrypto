#include <array>
#include <bitset>
#include <iostream>
#include <vector>

#include "aes/aes.hpp"
#include "crypto.hpp"
#include "measure.hpp"
#include "string_utils.hpp"

namespace aes = crypto::aes;

using Block = std::array<uint8_t, 16>;
using Vector = std::vector<Block>;
using Cipher = decltype(aes::enc);

static bool test_cipher(Cipher enc, const Vector &cip, const Vector &msg, Block key)
{
    bool pass = true;

    for (size_t i = 0; i < 1; ++i)
    {
        Block e;
        enc(e.data(), msg[i].data(), key.data());
        pass &= e == cip[i];
    }

    return pass;
}

static bool test_ctr(const Block &cip, const Block &msg, Block key, Block nonce)
{
    bool pass = true;

    Block e;
    aes::enc3_ctr(e.data(), msg.data(), msg.size(), key.data(), nonce.data());
    pass &= e == cip;

    aes::enc3_ctr256(e.data(), msg.data(), msg.size(), key.data(), nonce.data());
    pass &= e == cip;

    return pass;
}

static bool run_tests()
{
    Vector msg{"6bc1bee22e409f96e93d7e117393172a"_x};
    Vector cip{"3ad77bb40d7a3660a89ecaf32466ef97"_x};
    auto key = "2b7e151628aed2a6abf7158809cf4f3c"_x;
    bool pass, all_pass;

    std::cout << "==== Testing AES naive implementation ====\n";
    all_pass = true;

    all_pass &= pass = test_cipher(aes::enc, cip, msg, key);
    std::cout << "Naive Encryption... " << pass << '\n';


    all_pass &= pass = test_cipher(aes::enc2, cip, msg, key);
    std::cout << "Instrinsics Encryption... " << pass << '\n';

    all_pass &= pass = test_cipher(aes::enc3, cip, msg, key);
    std::cout << "Instrinsics Encryption 2... " << pass << '\n';

    all_pass &= pass = test_ctr(
        "874d6191b620e3261bef6864990db6ce"_x, "6bc1bee22e409f96e93d7e117393172a"_x,
        "2b7e151628aed2a6abf7158809cf4f3c"_x, "f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff"_x);
    std::cout << "CTR mode... " << pass << '\n';

#ifdef USE_CUDA
#endif

    return all_pass;
}

int main()
{
    std::cout << std::hex << std::uppercase;
    std::cout << std::boolalpha;
    bool all_check = run_tests();

#ifdef MEASURE_PERFORMANCE
    uint8_t m[16];
    uint8_t k[16];
    uint8_t c[16];

    measure([&]() { aes::enc(c, m, k); }, 1 << 19, 5, "Naive encryption");

    measure([&]() { aes::enc2(c, m, k); }, 1 << 19, 5, "Optimized encryption");

    measure([&]() { aes::enc3(c, m, k); }, 1 << 19, 5, "Optimized encryption + key precompute");
#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    return 0;
}
