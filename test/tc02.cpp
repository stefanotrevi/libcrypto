#include "tc02/tc02.hpp"
#include "measure.hpp"
#include "tc02/cu_tc02.hpp"
#include <array>
#include <iostream>

template<size_t sz> using Array = std::array<uint64_t, sz>;

namespace tc02 = crypto::tc02;
namespace cu_tc02 = cu::crypto::tc02;

static bool run_tests()
{
    Array<8> msg{0x6E84408E7A9A6603, 0x18C1B2A16C0FE1E0, 0x93D2F04F3EE6A0C3, 0xF3BD7ED653022FDA,
                 0x88B0C5E58D5D7DB2, 0x806F7A047F5A313D, 0x33E292DC1F9CA5A7, 0x586378B0DE8E9BB9};

    Array<8> mid{0x85D2C8C6C8DF8E4C, 0xB585271273C8F21E, 0x932998DEA3E91D1A, 0xB59FD9BE3823F8FE,
                 0xD45F6D982A86885B, 0x1FA1A2CCB4F9D2AD, 0x423C7D106E5482AF, 0x29DA56960BB30622};

    Array<8> cyp{0x9694CF2E31A41914, 0x2977B24A123C71DF, 0x9AC764544B098977, 0x5822392D98CED7C1,
                 0x84EF5DF476315690, 0x70D5E1BEA6532036, 0x3A3C5E3654185C08, 0xE4275E34400DACD5};
    uint64_t key = 0xB5225436F8E926E5;
    bool check = true;
    bool all_check = true;

    std::cout << std::boolalpha;


    std::cout << "Encryption... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
        check &= tc02::enc(msg[i], key, 9) == cyp[i];
    std::cout << check << '\n';
    all_check &= check;


    std::cout << "Decryption... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
        check &= tc02::dec(cyp[i], key, 9) == msg[i];
    std::cout << check << '\n';
    all_check &= check;


    std::cout << "Key schedule... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
        check &= tc02::dec(tc02::enc(msg[i], key, 9), tc02::sched(key, 5), 4) == mid[i];
    std::cout << check << '\n';
    all_check &= check;


#ifdef USE_CUDA
    std::cout << "CUDA Encryption... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
        check &= cu_tc02::test_enc(msg[i], key, 9) == cyp[i];
    std::cout << check << '\n';
    all_check &= check;
#endif

    return all_check;
}

int main()
{
    std::cout << "\n==== Testing TC02 implementation ====\n";

    bool all_check = run_tests();

#ifdef MEASURE_PERFORMANCE
    std::cout << "Measuring performance...\n";
    measure([](size_t i) { tc02::enc(i, i); }, 1 << 22, 10);

    #ifdef USE_CUDA
    std::cout << "Measuring CUDA performance...\n";
    measure([](size_t i) { cu_tc02::crack(0, 0, tc02::ROUNDS, 1 << 28); }, 1 << 5, 10);
    #endif
#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    return 0;
}
