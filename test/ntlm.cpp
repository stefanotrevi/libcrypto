#include "ntlm/ntlm.hpp"
#include "measure.hpp"
#include <iostream>

namespace ntlm1 = crypto::ntlm1;
namespace ntlm2 = crypto::ntlm2;

static bool run_tests()
{
    std::string msg[] = {"password", "another_one", ""};
    std::string hsh1[] = {"E52CAC67419A9A224A3B108F3FA6CB6D", "8433A2AF6CF0312928EDFCA92B4D4233",
                          "AAD3B435B51404EEAAD3B435B51404EE"};
    std::string hsh2[] = {"8846F7EAEE8FB117AD06BDD830B7586C", "E33364FFE45616F4B8FA5FE34409A968",
                          "31D6CFE0D16AE931B73C59D7E0C089C0"};

    bool check = true;
    bool all_check = true;

    std::cout << std::boolalpha;

    std::cout << "\n==== Testing LM/NTLM implementation ====\n";

    std::cout << "LM hashing... ";
    check = true;
    for (size_t i = 0; i < std::size(msg); ++i)
        check &= (std::string)ntlm1::Hash{msg[i]} == hsh1[i];
    std::cout << check << '\n';
    all_check &= check;


    std::cout << "NTLM hashing... ";
    check = true;
    for (size_t i = 0; i < std::size(msg); ++i)
        check &= (std::string)ntlm2::Hash{msg[i]} == hsh2[i];
    std::cout << check << '\n';
    all_check &= check;

    return all_check;
}

int main()
{
    bool all_check = run_tests();

#ifdef MEASURE_PERFORMANCE
    std::cout << "Measuring LM performance...\n";
    measure([&]() { ntlm1::Hash{"hello", sizeof("hello")}; }, 1 << 20, 4);

    std::cout << "Measuring NTLM performance...\n";
    measure([&]() { ntlm2::Hash{"hello", sizeof("hello")}; }, 1 << 20, 4);
#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    return 0;
}
