#include "measure.hpp"
#include "tc03/tc03.hpp"
#include <array>
#include <iostream>

template<size_t sz> using Array = std::array<uint8_t, sz>;

namespace tc03 = crypto::tc03;

static bool run_tests()
{
    uint64_t key = 0x1234567890ABCDEF;
    Array<2> msg{0xAB, 0xFF};
    Array<2> cyp{0xC8, 0x7C};

    bool check = true;
    bool all_check = true;

    std::cout << std::boolalpha;

    std::cout << "Encryption_v1... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
        check &= tc03::enc(msg[i], key) == cyp[i];
    std::cout << check << '\n';
    all_check &= check;


    check = true;
    std::cout << "Decryption_v1... ";
    for (size_t i = 0; i < msg.size(); ++i)
        check &= tc03::dec(cyp[i], key) == msg[i];
    std::cout << check << '\n';
    all_check &= check;


    std::cout << "Encryption_v2... ";
    for (size_t i = 0; i < msg.size(); ++i)
        check &= tc03::enc2(msg[i], key) == cyp[i];
    std::cout << check << '\n';
    all_check &= check;


    check = true;
    std::cout << "Decryption_v2... ";
    for (size_t i = 0; i < msg.size(); ++i)
        check &= tc03::dec2(cyp[i], key) == msg[i];
    std::cout << check << '\n';
    all_check &= check;


    return all_check;
}

int main()
{
    std::cout << "\n==== Testing TC03 implementation ====\n";

    bool all_check = run_tests();

#ifdef MEASURE_PERFORMANCE
    std::cout << "Measuring encryption performance...\n";
    measure([](size_t i) { tc03::enc(i, i); }, 1 << 24, 10);
    std::cout << "Measuring optimized encryption performance...\n";
    measure([](size_t i) { tc03::enc2(i, i); }, 1 << 24, 10);
#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    return 0;
}
