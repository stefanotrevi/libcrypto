#include "lotss/lotss.hpp"
#include "measure.hpp"
#include <iostream>

namespace lotss = crypto::lotss;

static bool run_tests()
{
    char msg[] = "abc";

    bool check = true;
    bool all_check = true;

    std::cout << std::boolalpha;

    std::cout << "\n==== Testing LOTSS implementation ====\n";

    std::cout << "Signing valid message... ";

    size_t msgsz = sizeof(msg);
    std::vector<uint8_t> sk(lotss::compute_sksize(msgsz));
    std::vector<uint8_t> pk(lotss::compute_pksize(msgsz));
    std::vector<uint8_t> sig(lotss::compute_sigsize(msgsz));

    lotss::keygen(sk.data(), sk.size(), pk.data(), pk.size());
    lotss::sign(sig.data(), sk.data(), msg, msgsz);
    check = lotss::check(sig.data(), pk.data(), msg, msgsz);

    std::cout << check << '\n';
    all_check &= check;

    std::cout << "Signing invalid message... ";
    ++msg[0];
    check = !lotss::check(sig.data(), pk.data(), msg, msgsz);
    std::cout << check << '\n';
    all_check &= check;

    return all_check;
}

static bool run_sign(const char *msg, size_t msg_sz)
{
    std::vector<uint8_t> sk(lotss::compute_sksize(msg_sz));
    std::vector<uint8_t> pk(lotss::compute_pksize(msg_sz));
    std::vector<uint8_t> sig(lotss::compute_sigsize(msg_sz));

    lotss::keygen(sk.data(), sk.size(), pk.data(), pk.size());
    lotss::sign(sig.data(), sk.data(), msg, msg_sz);

    return lotss::check(sig.data(), pk.data(), msg, msg_sz);
}


int main()
{
    bool all_check = run_tests();

#ifdef MEASURE_PERFORMANCE
    std::cout << "Measuring performance...\n";
    measure([&]() { run_sign("hello", sizeof("hello")); }, 1 << 10, 5, "lotss");
#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    return 0;
}
