#include <bitset>
#include <iostream>
#include <vector>

#include "crypto.hpp"
#include "des/cu_des.hpp"
#include "des/des.hpp"
#include "measure.hpp"

using Vector = std::vector<uint64_t>;
using Cypher = crypto::EncryptionFunction<uint64_t, uint64_t>;

namespace des = crypto::des;
namespace cu_des = cu::crypto::des;

static bool test_cypher(Cypher foo, const Vector &msg_in, const Vector &msg_out, uint64_t key)
{
    bool pass = true;

    for (size_t i = 0; i < msg_in.size(); ++i)
    {
        uint64_t e = foo(msg_in[i], key, des::ROUNDS);
        pass &= e == msg_out[i];
    }

    return pass;
}

// We only test optimized functions, assuming the naive implementation is correct
static bool test_schedule(uint64_t key)
{
    Vector keys(des::ROUNDS);
    Vector keys2(des::ROUNDS);

    bool pass = des::reduce_key(key) == des::reduce_key2(key);

    key = des::reduce_key(key);

    des::build_round_keys(key, keys.data());
    des::build_round_keys2(key, keys2.data());
    for (size_t i = 0; i < keys.size(); ++i)
        pass &= keys[i] == keys2[i];

    return pass;
}

static bool test_augment(uint64_t key)
{
    return des::augment_key(des::reduce_key(key)) == des::augment_key2(des::reduce_key2(key));
}

static bool test_initial_permute(const Vector &msg)
{
    bool pass = true;

    for (auto &&m : msg)
        pass &= des::initial_permute(m) == des::initial_permute2(m);

    return pass;
}

static bool test_final_permute(const Vector &msg)
{
    bool pass = true;

    for (auto &&m : msg)
        pass &= des::final_permute(m) == des::final_permute2(m);

    return pass;
}

static bool test_expand(const Vector &msg)
{
    bool pass = true;

    for (auto &&m : msg)
        pass &= des::expand((uint32_t)m) == des::expand2((uint32_t)m);

    return pass;
}

static bool test_sbox(const Vector &msg)
{
    bool pass = true;

    for (auto &&m : msg)
        pass &= des::sbox(m) == des::sbox2(m);

    return pass;
}

static bool test_perm(const Vector &msg)
{
    bool pass = true;

    for (auto &&m : msg)
        pass &= des::perm((uint32_t)m) == des::perm2((uint32_t)m);

    return pass;
}

static bool test_round(const Vector &msg, uint64_t key)
{
    bool pass = true;

    for (auto &&m : msg)
        pass &= des::round((uint32_t)m, key) == des::round2((uint32_t)m, key);

    return pass;
}

static bool run_tests()
{
    Vector msg{0x5468652071756663, 0x6B2062726F776E20, 0x666F78206A756D70};
    Vector cyp{0xA28E91724C4BBA31, 0x167E47EC24F71D63, 0x2C1A917234425365};
    uint64_t key = 0x0123456789ABCDEF;
    bool pass, all_pass;

    std::cout << "==== Testing DES naive implementation ====\n";
    all_pass = true;

    all_pass &= pass = test_cypher(des::dec, cyp, msg, key);
    std::cout << "Decryption... " << pass << '\n';


    std::cout << "\n==== Testing DES Optimized implementation ====\n";

    all_pass &= pass = test_schedule(key);
    std::cout << "Key schedule... " << pass << '\n';

    all_pass &= pass = test_augment(key);
    std::cout << "Key augment... " << pass << '\n';

    all_pass &= pass = test_initial_permute(msg);
    std::cout << "Initial permute... " << pass << '\n';

    all_pass &= pass = test_final_permute(msg);
    std::cout << "Final permute... " << pass << '\n';

    all_pass &= pass = test_expand(msg);
    std::cout << "Message expansion... " << pass << '\n';

    all_pass &= pass = test_sbox(msg);
    std::cout << "Sbox... " << pass << '\n';

    all_pass &= pass = test_perm(msg);
    std::cout << "Message permutation... " << pass << '\n';

    all_pass &= pass = test_round(msg, key);
    std::cout << "Round function... " << pass << '\n';

    all_pass &= pass = test_cypher(des::enc2, msg, cyp, key);
    std::cout << "Encryption... " << pass << '\n';

    all_pass &= pass = test_cypher(des::dec2, cyp, msg, key);
    std::cout << "Decryption... " << pass << '\n';

#ifdef USE_CUDA
    std::cout << "\n==== Testing DES CUDA implementation ====\n";

    all_pass &= pass = test_cypher(cu_des::test_enc, msg, cyp, key);
    std::cout << "Encryption... " << pass << '\n';

    all_pass &= pass = test_cypher(cu_des::test_dec, cyp, msg, key);
    std::cout << "Decryption... " << pass << '\n';
#endif

    return all_pass;
}

int main()
{
    std::cout << std::hex << std::uppercase;
    std::cout << std::boolalpha;
    bool all_check = run_tests();

#ifdef MEASURE_PERFORMANCE
    std::cout << "Measuring performance...\n";

//    measure([]() { des::enc(0, 0); }, 1 << 19, 5, "Naive encryption");

    measure([]() { des::enc2(0, 0); }, 1 << 19, 5, "Optimized encryption");
    #ifdef USE_CUDA
//    measure([]() { cu_des::crack(0, 0, des::ROUNDS, 1ULL << 28); }, 1, 5, "CUDA brute force");
    #endif
#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    return 0;
}
