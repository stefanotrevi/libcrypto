#include "wotss/wotss.hpp"
#include "measure.hpp"
#include <iostream>

namespace wotss = crypto::wotss;

static bool run_tests()
{
    char msg[] = "abc";

    bool check = true;
    bool all_check = true;

    std::cout << std::boolalpha;

    std::cout << "\n==== Testing WOTSS implementation ====\n";

    std::cout << "Signing valid message... ";

    size_t msgsz = sizeof(msg);
    std::array<uint8_t, wotss::SK_SIZE> sk{};
    std::array<uint8_t, wotss::PK_SIZE> pk{};
    std::array<uint8_t, wotss::SIG_SIZE> sig{};

    wotss::keygen(sk.data(), pk.data());
    wotss::sign(sig.data(), sk.data(), msg, msgsz);
    check = wotss::check(sig.data(), pk.data(), msg, msgsz);

    std::cout << check << '\n';
    all_check &= check;

    std::cout << "Signing invalid message... ";
    ++msg[0];
    check = !wotss::check(sig.data(), pk.data(), msg, msgsz);
    std::cout << check << '\n';
    all_check &= check;

    return all_check;
}

static bool run_sign(const char *msg, size_t msg_sz)
{
    std::array<uint8_t, wotss::SK_SIZE> sk{};
    std::array<uint8_t, wotss::PK_SIZE> pk{};
    std::array<uint8_t, wotss::SIG_SIZE> sig{};

    wotss::keygen(sk.data(), pk.data());
    wotss::sign(sig.data(), sk.data(), msg, msg_sz);

    return wotss::check(sig.data(), pk.data(), msg, msg_sz);
}


int main()
{
    bool all_check = run_tests();

#ifdef MEASURE_PERFORMANCE
    std::cout << "Measuring performance...\n";
    measure([&]() { run_sign("hello", sizeof("hello")); }, 1 << 10, 5, "lotss");
#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    return 0;
}
