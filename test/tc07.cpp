#include "tc07/tc07.hpp"
#include "measure.hpp"
#include "tc07/cu_tc07.hpp"
#include <array>
#include <iostream>

template<size_t sz> using Array = std::array<uint64_t, sz>;

namespace tc07 = crypto::tc07;
namespace cu_tc07 = cu::crypto::tc07;
static constexpr int ROUNDS = 4;

static bool run_tests()
{
    Array<8> msg{0xD89EBB732C75353F, 0xDA39C04295F6AA8B, 0x50DCC840C8E20AA7, 0x1BE44517DFFDC013,
                 0xAC114E18EF51BC66, 0xFF52024F66DF5981, 0xA19D93C0F078318E, 0x332D33DE67551DEC};

    Array<8> cyp{0xD6EBA35DF6AA2041, 0x91DD0F0A02C3931E, 0x727B0C2BA910DB6B, 0x93A5A0720B879822,
                 0x9162E3B383A512C7, 0x124F9E14F971EB3E, 0x5C40FD3F7BFF27BF, 0x1122DC52C6BAD798};
    uint64_t key = 0x84C7D1031BEA3913;
    bool check = true;
    bool all_check = true;

    std::cout << std::boolalpha;

    std::cout << "Encryption... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
    {
        uint64_t m_cyp = tc07::enc(msg[i], key, ROUNDS);
        check &= m_cyp == cyp[i];
    }
    std::cout << check << '\n';
    all_check &= check;


#ifdef USE_CUDA
    std::cout << "CUDA encryption... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
    {
        uint64_t m_cyp = cu_tc07::test_enc(msg[i], key, ROUNDS);
        check &= m_cyp == cyp[i];
    }
    std::cout << check << '\n';
    all_check &= check;
#endif

    return all_check;
}

int main()
{
    std::cout << "\n==== Testing TC07 implementation ====\n";

    bool all_check = run_tests();

#ifdef MEASURE_PERFORMANCE
    std::cout << "Measuring performance...\n";
    measure([](size_t i) { tc07::enc(i, i); }, 1 << 24, 5);
#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    return 0;
}
