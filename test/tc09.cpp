#include "tc09/tc09.hpp"
#include "measure.hpp"
#include "tc09/cu_tc09.hpp"
#include <array>
#include <iostream>

template<size_t sz> using Array = std::array<uint64_t, sz>;

namespace tc09 = crypto::tc09;
namespace cu_tc09 = cu::crypto::tc09;

static bool run_tests()
{
    Array<2> msg{0xD345F2535E233B32, 0x6ACD521CBF784244};
    Array<2> cyp{0xEF3BF22D396894E, 0xCA54E133FE5AE1A4};
    uint64_t key = 0xDDC998C61C2BE901;
    bool check = true;
    bool all_check = true;

    std::cout << std::boolalpha;

    std::cout << "Encryption v1... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
    {
        uint64_t m_cyp = tc09::enc(msg[i], key);
        check &= m_cyp == cyp[i];
    }
    std::cout << check << '\n';
    all_check &= check;


    std::cout << "Decryption v1... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
    {
        uint64_t m_cyp = tc09::dec(cyp[i], key);
        check &= m_cyp == msg[i];
    }
    std::cout << check << '\n';
    all_check &= check;


    std::cout << "Encryption v2... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
    {
        uint64_t m_cyp = tc09::enc2(msg[i], key);
        check &= m_cyp == cyp[i];
    }
    std::cout << check << '\n';
    all_check &= check;


    std::cout << "Decryption v2... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
    {
        uint64_t m_cyp = tc09::dec2(cyp[i], key);
        check &= m_cyp == msg[i];
    }
    std::cout << check << '\n';
    all_check &= check;

#ifdef USE_CUDA
    std::cout << "CUDA encryption... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
    {
        uint64_t m_cyp = cu_tc09::test_enc(msg[i], key);
        check &= m_cyp == cyp[i];
    }
    std::cout << check << '\n';
    all_check &= check;
#endif

    return all_check;
}

int main()
{
    std::cout << "\n==== Testing TC09 implementation ====\n";

    bool all_check = run_tests();

#ifdef MEASURE_PERFORMANCE
    std::cout << "Measuring encryption performance...\n";
    measure([](size_t i) { tc09::enc(i, i); }, 1 << 22, 5, "encryption v1");
    std::cout << "Measuring optimized encryption performance...\n";
    measure([](size_t i) { tc09::enc2(i, i); }, 1 << 22, 5, "encryption v2");

    #ifdef USE_CUDA
    std::cout << "Measuring CUDA crack performance...\n";
    measure([]() { cu_tc09::crack(0, 0x11116666CCCCBBBB); });
    #endif
#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    return 0;
}
