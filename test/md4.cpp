#include "md4/md4.hpp"
#include "md4/cu_md4.hpp"
#include "measure.hpp"

namespace md4 = crypto::md4;
namespace cu_md4 = cu::crypto::md4;

static bool run_tests()
{
    std::string msg[] = {"The quick brown fox jumps over the lazy dog",
                         "The quick brown fox jumps over the lazy cog", ""};
    std::string hsh[] = {"1bee69a46ba811185c194762abaeae90", "b86e130ce7028da59e672d56ad0113df",
                         "31d6cfe0d16ae931b73c59d7e0c089c0"};
    bool check = true;
    bool all_check = true;
    std::cout << std::boolalpha;

    std::cout << "\n==== Testing MD4 implementation ====\n";

    std::cout << "Hashing... ";
    check = true;
    for (size_t i = 0; i < std::size(msg); ++i)
    {
        auto hash = md4::Hash{msg[i]};
        check &= (std::string)md4::Hash{msg[i]} == hsh[i];
    }
    std::cout << check << '\n';
    all_check &= check;

#ifdef USE_CUDA
    std::cout << "CUDA hashing... ";
    check = true;
    for (size_t i = 0; i < std::size(msg); ++i)
    {
        uint8_t dig[md4::DIGEST_SIZE];

        cu_md4::test(dig, msg[i].data(), msg[i].size());

        check &= hexdump(dig) == hsh[i];
    }
    std::cout << check << '\n';
    all_check &= check;
#endif

    return all_check;
}

int main()
{
    static constexpr const char msg[] = "passwr";

    md4::Hash hash{msg, sizeof(msg) - sizeof(msg[0])};

    bool all_check = run_tests();

#ifdef MEASURE_PERFORMANCE
    char ret[17]{};

    std::cout << "Measuring performance...\n";
    measure([&]() { md4::Hash{"hello"}; }, 1 << 22, 4);

    #ifdef USE_CUDA
    std::cout << "Measuring CUDA performance...\n";
    measure([&]() { cu_md4::crack_oneblock(ret, hash.data(), 0, ~0, 0, true); });
    std::cout << "Retrieved message: " << ret << '\n';
    #endif
#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    return 0;
}
