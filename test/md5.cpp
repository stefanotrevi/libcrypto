#include "md5/md5.hpp"
#include "measure.hpp"
#include <fstream>
#include <iostream>
#include <iterator>
#include <vector>

namespace md5 = crypto::md5;

static bool run_tests()
{
    std::string msg[] = {"The quick brown fox jumps over the lazy dog",
                         "The quick brown fox jumps over the lazy dog.", ""};
    std::string hsh[] = {"9e107d9d372bb6826bd81d3542a419d6", "e4d909c290d0fb1ca068ffaddf22cbd0",
                         "d41d8cd98f00b204e9800998ecf8427e"};
    bool check = true;
    bool all_check = true;

    std::cout << std::boolalpha;

    std::cout << "\n==== Testing MD5 implementation ====\n";

    std::cout << "Hashing... ";
    check = true;
    for (size_t i = 0; i < std::size(msg); ++i)
        check &= (std::string)md5::Hash{msg[i]} == hsh[i];
    std::cout << check << '\n';
    all_check &= check;

    return all_check;
}

int main()
{
    bool all_check = run_tests();

#ifdef MEASURE_PERFORMANCE
    std::cout << "Measuring performance...\n";
    measure([&]() { md5::Hash{"hello"}; }, 1 << 20, 4);
#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    return 0;
}
