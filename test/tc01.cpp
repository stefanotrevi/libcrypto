#include <array>
#include <iostream>

#include "measure.hpp"
#include "tc01/cu_tc01.hpp"
#include "tc01/tc01.hpp"

template<size_t sz> using Array = std::array<uint64_t, sz>;

namespace tc01 = crypto::tc01;
namespace cu_tc01 = cu::crypto::tc01;

static bool run_tests()
{
    Array<4> msg{0xEC1280349633527F, 0xB64A1D6937722941, 0x20371847D94F8848, 0x060A15C31F0C659C};

    Array<4> cyp{0xFC69D147018F67DF, 0x99324F3EAB39D783, 0x8267BED5C75F149D, 0x70FE986BA2624117};
    uint64_t key = 0x84C7D1031BEA3913;
    bool check = true;
    bool all_check = true;

    std::cout << "Encryption... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
    {
        uint64_t m_cyp = tc01::enc(msg[i], key);
        check &= m_cyp == cyp[i];
    }
    std::cout << check << '\n';

#ifdef USE_CUDA
    std::cout << "CUDA encryption... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
    {
        uint64_t m_cyp = cu_tc01::test_enc(msg[i], key);
        check &= m_cyp == cyp[i];
    }
    std::cout << check << '\n';
#endif

    return all_check;
}

int main()
{
    std::cout << std::boolalpha;

    std::cout << "\n==== Testing TC01 implementation ====\n";

    bool all_check = run_tests();

#ifdef MEASURE_PERFORMANCE
    std::cout << "Measuring performance...\n";
    measure([](size_t i) { tc01::enc(i, i); }, 1 << 20, 5);

#ifdef USE_CUDA
    std::cout << "Measuring CUDA performance...\n";
    measure([](size_t i) { cu_tc01::crack(0, 0, tc01::ROUNDS, 1 << 28); });
#endif
#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    return 0;
}
