#include "measure.hpp"
#include "tc05/tc05.hpp"
#include <array>
#include <iostream>

template<size_t sz> using Array = std::array<uint32_t, sz>;

namespace tc05 = crypto::tc05;

static bool run_tests()
{
    Array<8> msg{0xF860F25E, 0x32B30CAD, 0xC59A1AC8, 0x9DA1FEF8,
                 0x8E37175A, 0x23E35206, 0xCB9EB539, 0x2F1F6D1D};

    Array<8> cyp{0x9C429B21, 0x12B8E9AA, 0x2188C256, 0x26AF19D6,
                 0xBCACE55B, 0x0DF773A5, 0xEC76C15E, 0x0D7FB299};
    uint64_t key = 0xBD6E4E908F1AB7A7;
    bool check = true;
    bool all_check = true;

    std::cout << std::boolalpha;

    std::cout << "Encryption... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
        check &= tc05::enc(msg[i], key) == cyp[i];
    std::cout << check << '\n';
    all_check &= check;


    std::cout << "Decryption... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
        check &= tc05::dec(cyp[i], key) == msg[i];
    std::cout << check << '\n';
    all_check &= check;


    return all_check;
}

int main()
{
    std::cout << "\n==== Testing TC05 implementation ====\n";

    bool all_check = run_tests();

#ifdef MEASURE_PERFORMANCE
    std::cout << "Measuring performance...\n";
    measure([](size_t i) { tc05::enc(i, i); }, 1 << 22, 10);
#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    return 0;
}
