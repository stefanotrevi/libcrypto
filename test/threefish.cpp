#include "threefish/threefish.hpp"
#include "measure.hpp"
#include "string_utils.hpp"
#include <array>
#include <iostream>

namespace threefish = crypto::threefish;

using Msg = std::array<uint8_t, threefish::BLOCK_SZ>;


static bool run_tests()
{
    std::array<Msg, 2> tweak{
        "0000000000000000000000000000000000000000000000000000000000000000"_x,
        "07060504030201000F0E0D0C0B0A090800000000000000000000000000000000"_x,
    };

    std::array<Msg, 2> key{
        "0000000000000000000000000000000000000000000000000000000000000000"_x,
        "17161514131211101F1E1D1C1B1A191827262524232221202F2E2D2C2B2A2928"_x,
    };

    std::array<Msg, 2> msg{
        "0000000000000000000000000000000000000000000000000000000000000000"_x,
        "F8F9FAFBFCFDFEFFF0F1F2F3F4F5F6F7E8E9EAEBECEDEEEFE0E1E2E3E4E5E6E7"_x,
    };

    std::array<Msg, 2> cyp{
        "94EEEA8B1F2ADA84ADF103313EAE6670952419A1F4B16D53D83F13E63C9F6B11"_x,
        "DF8FEA0EFF91D0E0D50AD82EE69281C976F48D58085D869DDF975E95B5567065"_x,
    };

    bool check = true;
    bool all_check = true;

    std::cout << std::boolalpha;

    std::cout << "Encryption... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
    {
        Msg res;

        threefish::enc(res.data(), msg[i].data(), key[i].data(), tweak[i].data());
        std::cout << hexdump(res) << '\n';
        check &= res == cyp[i];
    }
    std::cout << check << '\n';
    all_check &= check;

    return all_check;
}

int main()
{
    std::cout << "\n==== Testing Threefish implementation ====\n";

    bool all_check = run_tests();

#ifdef MEASURE_PERFORMANCE
    std::cout << "Measuring encryption performance...\n";
//    measure([](size_t i) { threefish::enc(i, i); }, 1 << 22, 5, "encryption");
#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    return 0;
}
