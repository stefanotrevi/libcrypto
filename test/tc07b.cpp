#include "tc07/tc07b.hpp"
#include "measure.hpp"
#include <array>
#include <iostream>

template<size_t sz> using Array = std::array<uint64_t, sz>;

namespace tc07b = crypto::tc07b;

static bool run_tests()
{
    Array<2> msg{0x0123456789ABCDEF, 0x0000000000000000};
    Array<2> cyp{0x8929564E267F6EEA, 0x2E00CFC70B40411B};
    uint64_t key = 0x0123456789ABCDEF;
    bool check = true;
    bool all_check = true;

    std::cout << std::hex << std::uppercase << std::boolalpha;

    std::cout << "Encryption... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
    {
        uint64_t m_cyp = tc07b::enc2(msg[i], key);
        check &= m_cyp == cyp[i];
    }
    std::cout << check << '\n';
    all_check &= check;


    std::cout << "Decryption... ";
    check = true;
    for (size_t i = 0; i < msg.size(); ++i)
    {
        uint64_t m_cyp = tc07b::dec2(cyp[i], key);
        check &= m_cyp == msg[i];
    }
    std::cout << check << '\n';
    all_check &= check;


    std::cout << "Partial decryption... ";
    for (size_t i = 0; i < msg.size(); ++i)
    {
        uint32_t m_cyp = cyp[i];

        for (int j = 0; j < tc07b::ROUNDS; ++j)
        {
            uint32_t k = tc07b::sched(key, tc07b::ROUNDS - j - 1);
            m_cyp = tc07b::pdec(m_cyp) ^ k;
        }
        check &= m_cyp == (msg[i] & 0xFFFFFFFF);
    }
    std::cout << check << '\n';
    all_check &= check;


    return all_check;
}

int main()
{
    std::cout << "\n==== Testing TC07B implementation ====\n";

    bool all_check = run_tests();

#ifdef MEASURE_PERFORMANCE
    std::cout << "Measuring encryption performance...\n";
    measure([](size_t i) { tc07b::enc(i, i); }, 1 << 24, 5);

    std::cout << "Measuring optimized encryption performance...\n";
    measure([](size_t i) { tc07b::enc2(i, i); }, 1 << 24, 5);
#endif

    std::cout << "\n==== " << (all_check ? "ALL TESTS SUCCEEDED" : "SOME TESTS FAILED")
              << " ====\n\n";

    return 0;
}
