#pragma once

#include "cu_default.cuh"

namespace cu::reverse
{
    __host__ void rev(uint8_t* v, size_t sz);
} // namespace cu::reverse
