#pragma once

#include "crypto.hpp"
#include <climits>

namespace crypto::md4
{
    static constexpr size_t DIGEST_SIZE = 128 / CHAR_BIT;

    void hash(void *digest, const void *msg, size_t sz);

    class Hash : public crypto::Hash<uint32_t, DIGEST_SIZE, hash>
    {
    public:
        using Base::Hash;
    };
} // namespace md4
