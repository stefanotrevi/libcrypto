#pragma once

#include <cinttypes>
#include <climits>
#include <cstddef>
#include "rainbow/rainbow.hpp"

namespace cu::crypto::md4
{
    static constexpr size_t DIGEST_SIZE = 128 / CHAR_BIT;
    
    void test(void *dig, const void *msg, uint32_t sz);
    size_t crack(void *res, const void *dig, uint8_t known_len = 0, uint64_t limit = ~0,
                 uint64_t off = 0, bool watch = false);
    size_t crack_oneblock(void *res, const void *dig, uint8_t known_len = 0, uint64_t limit = ~0,
                          uint64_t off = 0, bool watch = false);

    ::crypto::rainbow::Table build_rainbow_table(size_t tab_sz, size_t chain_sz, bool watch = false);

} // namespace cu::crypto::md4
