#pragma once

#include "print_iterables.hpp"
#include <algorithm>
#include <array>
#include <string>
#include <cstdint>

constexpr inline uint8_t ascii_to_digit(char c)
{
    uint8_t x = 0;

    if ('0' <= c && c <= '9')
        x = c - '0' + 0x0;
    else if ('A' <= c && c <= 'F')
        x = c - 'A' + 0xA;
    else if ('a' <= c && c <= 'f')
        x = c - 'a' + 0xa;

    return x;
}

template<size_t sz> //
struct StrToHex
{
    std::array<uint8_t, sz / 2> v;

    constexpr StrToHex(const char (&str)[sz])
    {
        for (size_t i = 0; i < v.size(); ++i)
            v[i] = ascii_to_digit(str[i * 2]) << 4 | ascii_to_digit(str[i * 2 + 1]);
    }
};

template<StrToHex cvt> consteval auto operator""_x()
{
    return cvt.v;
}

namespace detail
{
    template<typename T, bool reverse = false> consteval T str_to_t(const char *s, size_t sz)
    {
        T x = 0;

        for (size_t i = 0; i < sizeof(x) && i < sz; ++i)
            if constexpr (reverse)
                x |= static_cast<decltype(x)>(s[i]) << (i * 8);
            else
                x |= static_cast<decltype(x)>(s[i]) << ((sizeof(x) - i - 1) * 8);

        return x;
    }
} // namespace detail

#define CAT0(x, y) x##y
#define CAT(x, y) CAT0(x, y)

#define OPERATOR_STR_TO_INTTYPE_NAMED(sign, size, type, name)                                      \
    consteval type name(const char *s, size_t sz) { return detail::str_to_t<type, false>(s, sz); } \
                                                                                                   \
    consteval type CAT(name, r)(const char *s, size_t sz)                                          \
    {                                                                                              \
        return detail::str_to_t<type, true>(s, sz);                                                \
    }

#define OPERATOR_STR_TO_INTTYPE(sign, size)                                                        \
    OPERATOR_STR_TO_INTTYPE_NAMED(sign, size, CAT(CAT(CAT(sign, int), size), _t),                  \
                                  CAT(CAT(operator""_, sign), size))

OPERATOR_STR_TO_INTTYPE(, 64)
OPERATOR_STR_TO_INTTYPE(u, 64)
OPERATOR_STR_TO_INTTYPE(, 32)
OPERATOR_STR_TO_INTTYPE(u, 32)
OPERATOR_STR_TO_INTTYPE(, 16)
OPERATOR_STR_TO_INTTYPE(u, 16)
OPERATOR_STR_TO_INTTYPE(, 8)
OPERATOR_STR_TO_INTTYPE(u, 8)

#undef OPERATOR_STR_TO_INTTYPE
#undef OPERATOR_STR_TO_INTTYPE_NAMED
#undef CAT
#undef CAT0

template<bool reverse = false, typename T>
std::string hexdump(T *arr, size_t sz, bool upper = false, size_t spacing = 0)
{
    const char *TO_CHAR = upper ? "0123456789ABCDEF" : "0123456789abcdef";

    std::string s(sz * sizeof(T) * 2, 0);
    uint8_t *d = (uint8_t *)arr;

    if constexpr (reverse)
        for (size_t i = 0; i < sz; ++i)
        {
            size_t k = i * sizeof(T);
            for (size_t j = 0; j < sizeof(T); ++j)
            {
                s[(k + j) * 2] = TO_CHAR[d[k + sizeof(T) - j - 1] >> 4];
                s[(k + j) * 2 + 1] = TO_CHAR[d[k + sizeof(T) - j - 1] & 0xF];
            }
        }
    else
        for (size_t i = 0; i < sz * sizeof(T); ++i)
        {
            s[i * 2] = TO_CHAR[d[i] >> 4];
            s[i * 2 + 1] = TO_CHAR[d[i] & 0xF];
        }

    if (spacing)
        for (size_t i = 0, j = 0; i < s.size(); ++i, ++j)
            if (i && (j % spacing) == 0)
                s.insert(i++, " ");

    return s;
}

template<bool reverse = false, std::ranges::range Iter>
std::string hexdump(const Iter &it, bool upper = false, size_t spacing = 0)
{
    return hexdump<reverse>(&*std::begin(it), std::distance(std::begin(it), std::end(it)), upper,
                            spacing);
}
