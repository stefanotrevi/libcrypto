#pragma once

#include <cstdint>

namespace crypto::klein
{
    static constexpr int ROUNDS = 12;

    // Apply the SubNibbles transformation, which is the inverse of itself
    uint64_t sub(uint64_t m);

    // Apply the RotateNibbles transformation
    uint64_t rot(uint64_t m);

    // Invert the RotateNibbles transformation
    uint64_t rot_inv(uint64_t m);

    // Apply the MixNibbles transformation
    uint64_t mix(uint64_t m);

    // Invert the MixNibbles transformation
    uint64_t mix_inv(uint64_t m);

    // Apply all the transformations
    uint64_t round(uint64_t m);

    // Invert all the transformations
    uint64_t round_inv(uint64_t m);

    // Schedule key for the next round
    uint64_t next_key(uint64_t k, int round);

    // Schedule key for the previous round
    uint64_t prev_key(uint64_t k, int round);

    // Schedule several rounds
    uint64_t sched(uint64_t k, int rounds);

    // Deschedule several rounds
    uint64_t desched(uint64_t k, int rounds);

    // Encrypt
    uint64_t enc(uint64_t m, uint64_t k, int rounds = ROUNDS);

    // Decrypt
    uint64_t dec(uint64_t m, uint64_t k, int rounds = ROUNDS);

    // Faster implementation
    uint64_t enc2(uint64_t m, uint64_t k, int rounds = ROUNDS);
    uint64_t dec2(uint64_t m, uint64_t k, int rounds = ROUNDS);

} // namespace klein
