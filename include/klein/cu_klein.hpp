#pragma once

#include <cstddef>
#include <cstdint>

namespace cu::crypto::klein
{
    static constexpr int ROUNDS = 12;

    uint64_t test_enc(uint64_t msg, uint64_t key, int rounds = ROUNDS);

    uint64_t test_dec_nosched(uint64_t msg, uint64_t key, int rounds = ROUNDS);

    uint64_t crack(uint64_t msg, uint64_t cyp, int rounds = ROUNDS, uint64_t lim = ~0,
                   uint64_t off = 0, uint64_t msk = ~0, uint64_t known = 0, size_t watch = 0);

    uint64_t crack_dec_nosched(uint64_t msg, uint64_t cyp, int rounds = ROUNDS, uint64_t lim = ~0,
                               uint64_t off = 0, uint64_t msk = ~0, uint64_t known = 0,
                               size_t watch = 0);

} // namespace cu::crypto::klein
