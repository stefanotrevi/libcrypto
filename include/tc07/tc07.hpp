#pragma once

#include <cinttypes>

namespace crypto::tc07
{
    static constexpr int ROUNDS = 10;

    uint64_t sched(uint64_t k, int rounds = ROUNDS);

    uint64_t enc(uint64_t m, uint64_t k, int rounds = ROUNDS);
    uint64_t enc2(uint64_t m, uint64_t k, int rounds = ROUNDS);

    uint64_t enc2_nomix(uint64_t m, uint64_t k, int rounds = ROUNDS);
    uint64_t dec2_nomix(uint64_t m, uint64_t k, int rounds = ROUNDS);
} // namespace tc07
