#pragma once

#include <cinttypes>
#include <vector>

namespace cu::crypto::tc07
{
    static constexpr int ROUNDS = 10;
    using Vector = std::vector<uint64_t>;

    uint64_t test_enc(uint64_t msg, uint64_t key, int rounds = ROUNDS);
    uint64_t crack(uint64_t msg, uint64_t cyp, int rounds = ROUNDS, uint64_t lim = ~0,
                   uint64_t off = 0, uint64_t msk = ~0, uint64_t known = 0, size_t watch = 0);

    Vector candidates(const Vector &msg, const Vector &cyp, int rounds,
                      std::pair<uint32_t, uint64_t> dep);
} // namespace cu::crypto::tc07
