#pragma once

#include <cinttypes>

namespace crypto::tc07c
{
    static constexpr int ROUNDS = 7;

    uint64_t sched(uint64_t k, int rounds = ROUNDS);

    uint64_t enc(uint64_t m, uint64_t k, int rounds = ROUNDS);

    uint64_t enc_nomix(uint64_t m, uint64_t k, int rounds = ROUNDS);
    uint64_t dec_nomix(uint64_t m, uint64_t k, int rounds = ROUNDS);
} // namespace crypto::tc07c
