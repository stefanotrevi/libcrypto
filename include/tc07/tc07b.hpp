#pragma once

#include <cinttypes>

namespace crypto::tc07b
{
    static constexpr int ROUNDS = 10;

    uint64_t sbox(uint64_t m);
    uint64_t sbox_inv(uint64_t m);

    uint64_t rows(uint64_t m);
    uint64_t rows_inv(uint64_t m);

    uint64_t columns(uint64_t m);
    uint64_t columns_inv(uint64_t m);

    uint64_t next_key(uint64_t k);
    uint64_t prev_key(uint64_t k);
    uint64_t sched(uint64_t k, int rounds = ROUNDS);

    uint64_t enc(uint64_t m, uint64_t k, int rounds = ROUNDS);
    uint64_t dec(uint64_t m, uint64_t k, int rounds = ROUNDS);

    uint64_t enc2(uint64_t m, uint64_t k, int rounds = ROUNDS);
    uint64_t dec2_nosched(uint64_t m, uint64_t k, int rounds);
    uint64_t dec2(uint64_t m, uint64_t k, int rounds = ROUNDS);

    uint32_t pdec(uint32_t m);
} // namespace tc07b
