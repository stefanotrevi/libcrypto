#pragma once

#include "cu_crypto.cuh"
#include "rainbow.hpp"
#include <boost/sort/pdqsort/pdqsort.hpp>
#include <vector>

namespace cu::crypto::rainbow
{
    static constexpr uint64_t MAX_LETTERS = ::crypto::rainbow::Table::MAX_LETTERS;
    static constexpr uint64_t BITMASK = ::crypto::rainbow::Table::BITMASK;

    struct Table
    {
        std::vector<uint64_t> chain;                    // reduction function vector
        std::vector<std::pair<uint64_t, uint64_t>> tab; // actual table
    };

    namespace kernel
    {
        template<HashFunction hash, uint32_t digest_sz>
        __global__ void build_table(ulonglong2 *d_tab, size_t tab_sz, const uint64_t *d_chain,
                                    uint32_t chain_sz, size_t off)
        {
            size_t id = off + blockIdx.x * blockDim.x + threadIdx.x;

            if (id >= tab_sz)
                return;

            uint64_t res = d_tab[id].x;
            uint8_t dig[digest_sz];
            uint64_t msg[digest_sz / sizeof(uint64_t)];

            for (uint32_t i = 0; i < chain_sz; ++i)
            {
                // Apply hash function and reduction
                size_t len = u64_to_str(res, (char *)msg);

                hash(dig, msg, len);
                hash(msg, dig, digest_sz);

                res = (msg[0] ^ d_chain[i]) & BITMASK;
            }
            d_tab[id].y = res;
        }
    } // namespace kernel

    template<HashFunction hash, uint32_t digest_sz>
    Table build_table(size_t tab_sz, size_t chain_sz, bool watch)
    {
        static constexpr uint32_t BLKSZ = MAX_BLKSZ;
        static constexpr uint32_t GRDSZ = 1 << 12;

        std::mt19937_64 rng{seeded_rng()};
        uint64_t *d_chain;
        ulonglong2 *d_tab;
        Table tab;

        tab.chain.resize(chain_sz);
        std::generate(tab.chain.begin(), tab.chain.end(), std::ref(rng));

        tab.tab.resize(tab_sz);

#pragma omp parallel for
        for (size_t i = 0; i < tab_sz; ++i)
            tab.tab[i].first = rng() & BITMASK;

        cudaMalloc(&d_chain, chain_sz * sizeof(*d_chain));
        cudaMemcpy(d_chain, tab.chain.data(), chain_sz * sizeof(*d_chain), cudaMemcpyHostToDevice);

        cudaMalloc(&d_tab, tab_sz * sizeof(*d_tab));
        cudaMemcpy(d_tab, tab.tab.data(), tab_sz * sizeof(*d_tab), cudaMemcpyHostToDevice);

        if (watch)
        {
            for (size_t off = 0; off < tab_sz; off += GRDSZ * BLKSZ)
            {
                kernel::build_table<hash, digest_sz>
                    <<<GRDSZ, BLKSZ>>>(d_tab, tab_sz, d_chain, chain_sz, off);
                std::cout << off << '/' << tab_sz << '\r';
                std::cout.flush();
                if (auto err = cudaGetLastError(); err != cudaSuccess)
                    std::cerr << cudaGetErrorString(err) << '\n';
                cudaDeviceSynchronize();
            }
            std::cout << '\n';
        }
        else
            kernel::build_table<hash, digest_sz>
                <<<tab_sz / BLKSZ + 1, BLKSZ>>>(d_tab, tab_sz, d_chain, chain_sz, 0);

        cudaDeviceSynchronize();

        cudaMemcpy(tab.tab.data(), d_tab, tab_sz * sizeof(*d_tab), cudaMemcpyDeviceToHost);

        cudaFree(d_chain);
        cudaFree(d_tab);
        // sort so we can do binary search later
        boost::sort::pdqsort_branchless(tab.tab.begin(), tab.tab.end(),
                                        [](auto &&x, auto &&y) { return x.second < y.second; });

        return tab;
    }
} // namespace cu::crypto::rainbow
