#pragma once

#include "crypto_base.hpp"
#include <cstdint>
#include <vector>

namespace crypto::rainbow
{
    class Table
    {
    public:
        static constexpr uint64_t MAX_LETTERS = 5;
        static constexpr uint64_t BITMASK = (1ULL << BITS_PER_CHAR * MAX_LETTERS) - 1;
        std::vector<uint64_t> chain;                    // reduction function vector
        std::vector<std::pair<uint64_t, uint64_t>> tab; // actual table

        Table() = default;
        Table(const std::vector<uint64_t> &c, const std::vector<std::pair<uint64_t, uint64_t>> &t) :
            chain{c}, tab{t} {};
        Table(HashFunction hash, size_t digest_sz, size_t tab_sz, size_t chain_sz,
              uint64_t watch = 0);
        Table(const char *fname);

        void save(const char *fname) const;
    };


    void crack(HashFunction hash, size_t digest_sz, const Table &table, void *res,
               const void *digest);
} // namespace crypto::rainbow
