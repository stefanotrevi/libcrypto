#pragma once

#include <type_traits>

namespace mystd
{
    struct nonesuch
    {
        ~nonesuch() = delete;
        nonesuch(nonesuch const &) = delete;
        void operator=(nonesuch const &) = delete;
    };

    namespace detail
    {
        template<class Default, class AlwaysVoid, template<class...> class Op, class... Args>
        struct detector
        {
            using value_t = std::false_type;
            using type = Default;
        };

        template<class Default, template<class...> class Op, class... Args>
        struct detector<Default, std::void_t<Op<Args...>>, Op, Args...>
        {
            using value_t = std::true_type;
            using type = Op<Args...>;
        };

    } // namespace detail

    template<template<class...> class Op, class... Args>
    using is_detected = typename detail::detector<nonesuch, void, Op, Args...>::value_t;

    template<template<class...> class Op, class... Args>
    using detected_t = typename detail::detector<nonesuch, void, Op, Args...>::type;

    template<class Default, template<class...> class Op, class... Args>
    using detected_or = detail::detector<Default, void, Op, Args...>;

    template<typename T> using begin_t = decltype(std::declval<T>().begin());
    template<typename T> using end_t = decltype(std::declval<T>().end());

    template<typename T> using is_beginnable = mystd::is_detected<begin_t, T>;
    template<typename T> using is_endable = mystd::is_detected<end_t, T>;
    template<typename T> constexpr bool is_beginnable_v = is_beginnable<T>::value;
    template<typename T> constexpr bool is_endable_v = is_endable<T>::value;
    template<typename T> constexpr bool is_iterable_v = is_beginnable_v<T> && (is_endable_v<T>);
} // namespace mystd
