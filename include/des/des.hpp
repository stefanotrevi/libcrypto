#pragma once

#include <cstdint>

namespace crypto::des
{
    static constexpr int ROUNDS = 16;

    uint64_t initial_permute(uint64_t m);
    uint64_t initial_permute2(uint64_t m);

    uint64_t rotate_key(uint64_t k, uint8_t s);

    uint64_t reduce_key(uint64_t k);
    uint64_t reduce_key2(uint64_t k);

    uint64_t augment_key(uint64_t k);
    uint64_t augment_key2(uint64_t k);

    uint64_t compress_key(uint64_t k);
    uint64_t compress_key2(uint64_t k);

    void build_round_keys(uint64_t k, uint64_t sk[ROUNDS]);
    void build_round_keys2(uint64_t k, uint64_t sk[ROUNDS]);

    uint64_t expand(uint32_t m);
    uint64_t expand2(uint32_t m);

    uint32_t sbox(uint64_t m);
    uint32_t sbox2(uint64_t m);

    uint32_t perm(uint32_t m);
    uint32_t perm2(uint32_t m);

    uint32_t round(uint32_t m, uint64_t k);
    uint32_t round2(uint32_t m, uint64_t k);

    uint64_t final_permute(uint64_t m);
    uint64_t final_permute2(uint64_t m);

    uint64_t enc(uint64_t m, uint64_t k, int rounds = ROUNDS);
    uint64_t enc2(uint64_t m, uint64_t k, int rounds = ROUNDS);

    uint64_t dec(uint64_t m, uint64_t k, int rounds = ROUNDS);
    uint64_t dec2(uint64_t m, uint64_t k, int rounds = ROUNDS);

    uint64_t crack(uint64_t m, uint64_t c, uint64_t off = 0);
    uint64_t crack_parallel(uint64_t m, uint64_t c, uint64_t off = 0);
} // namespace crypto::des
