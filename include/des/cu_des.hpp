#pragma once

#include <cinttypes>

namespace cu::crypto::des
{
    static constexpr int ROUNDS = 16;

    uint64_t test_enc(uint64_t m, uint64_t k, int rounds = ROUNDS);
    uint64_t test_dec(uint64_t m, uint64_t k, int rounds = ROUNDS);
    uint64_t crack(uint64_t m, uint64_t c, uint64_t off = 0, uint64_t limit = ~0);
} // namespace cu::crypto::des
