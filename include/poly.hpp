#pragma once

namespace poly
{
    template<typename T> T mul1(T a, T b);

    template<typename T> T mul2(T a, T b);

    template<typename T> T mul3(T a, T b);

    template<typename T> T red(T a, T b);

    template<typename T> T mulred(T a, T b, T c);

} // namespace poly

/* End of Header */

#include <bit>

#ifdef _WIN32
    #include <intrin.h>
#else
    #include <x86intrin.h>
#endif


namespace poly
{
    template<typename T> T mul1(T a, T b)
    {
        T ab = 0;

        while (b)
        {
            ab ^= a * (b & 1);
            a <<= 1;
            b >>= 1;
        }

        return ab;
    }

    template<typename T> T mul2(T a, T b)
    {
        T ab = 0;

        while (b)
        {
            if (b & 1)
                ab ^= a;
            a <<= 1;
            b >>= 1;
        }
        return ab;
    }

    template<typename T> T mul3(T a, T b)
    {
        __m128i v_a = _mm_set1_epi64x(a);
        __m128i v_b = _mm_set1_epi64x(b);

        v_a = _mm_clmulepi64_si128(v_a, v_b, 0);

        return (T)_mm_extract_epi64(v_a, 0);
    }

    template<typename T> T red(T a, T b)
    {
        T w_a = std::bit_width(a) - 1;
        T w_b = std::bit_width(b) - 1;

        b <<= w_a - w_b;
        w_a = T{1} << w_a;
        w_b = T{1} << w_b;

        while (w_a >= w_b)
        {
            if (a & w_a)
                a ^= b;
            b >>= 1;
            w_a >>= 1;
        }

        return a;
    }

    template<typename T> T mulred1(T a, T b, T c)
    {
        T ab = 0;
        T msb = std::bit_width(c) - 2;

        while (b)
        {
            ab ^= a * (b & 1);
            a = (a << 1) ^ (c * (a >> msb)); // a <= c -> a >> msb can only be 0 or 1
            b >>= 1;
        }

        return ab;
    }

    template<typename T> T mulred2(T a, T b, T c)
    {
        T ab = 0;
        T msb = std::bit_floor(c);

        while (b)
        {
            if (b & 1)
                ab ^= a;

            a = a & msb ? (a << 1) ^ c : a << 1;
            b >>= 1;
        }

        return ab;
    }

} // namespace poly
