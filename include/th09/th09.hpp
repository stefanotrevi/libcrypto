#pragma once

#include "crypto.hpp"

namespace crypto::th09
{
    static constexpr size_t DIGEST_SIZE = sizeof(uint64_t);
    static constexpr uint64_t FIX_KEY = 0xba5f29ec463451b0;

    void hash(void *digest, const void *msg, size_t sz);

    class Hash : public crypto::Hash<uint64_t, DIGEST_SIZE, hash>
    {
    public:
        using Base::Hash;
    };
}; // namespace crypto::th09
