#pragma once

#include <cstddef>

namespace crypto::rc4
{
    void enc(void *cyp, const void *msg, size_t msg_sz, const void *key, size_t key_sz, size_t off);

    void dec(void *msg, const void *cyp, size_t cyp_sz, const void *key, size_t key_sz, size_t off);
} // namespace crypto::rc4
