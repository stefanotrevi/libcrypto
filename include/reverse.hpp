#pragma once

#include <cinttypes>
#include <cstddef>
#include "avxlib.hpp"

namespace reverse
{
    // reverse the bits in every byte of x
    uint8_t reverse(uint8_t x);
    uint16_t reverse(uint16_t x);
    uint32_t reverse(uint32_t x);
    uint64_t reverse(uint64_t x);

    __m256i reverse_avx2(__m256i x);
    // reverse the bits in the bytes of an array
    void reverse(uint8_t *v, size_t sz);
} // namespace reverse
