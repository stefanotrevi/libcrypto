#pragma once

#include "crypto.hpp"
#include "intrinsics.h"

namespace crypto::ntlm1
{
    static constexpr size_t DIGEST_SIZE = 16;

    void hash(void *digest, const void *msg, size_t sz);

    class Hash : public crypto::Hash<uint64_t, DIGEST_SIZE, hash>
    {
    public:
        using Base::Hash;

        explicit operator std::string() const { return hexdump<true>(digest, true); }

        void insert(const std::string &str)
        {
            Base::insert(str);

            digest[0] = _bswap64(digest[0]);
            digest[1] = _bswap64(digest[1]);
        }
    };

} // namespace ntlm1

namespace crypto::ntlm2
{
    static constexpr size_t DIGEST_SIZE = 16;

    void hash(void *digest, const void *msg, size_t sz);

    class Hash : public crypto::Hash<uint64_t, DIGEST_SIZE, hash>
    {
    public:
        using Base::Hash;

        explicit operator std::string() const { return hexdump(digest, true); }
    };
} // namespace ntlm2
