#pragma once

#include <cinttypes>
#include <vector>

namespace cu::crypto::tc05
{
    static constexpr int ROUNDS = 10;
    using Vector = std::vector<uint64_t>;

    uint64_t test_enc(uint64_t msg, uint64_t key, int rounds = ROUNDS);
    uint64_t crack(uint64_t msg, uint64_t cyp, int rounds = ROUNDS, uint64_t lim = ~0,
                   uint64_t off = 0, uint64_t msk = ~0, uint64_t known = 0, size_t watch = 0);
} // namespace cu::crypto::tc05
