#pragma once

#include <cstdint>

#ifndef _WIN32
using std::size_t;
#endif

namespace crypto::aes
{
    uint8_t sbox(uint8_t x);

    void enc(void *cip, const void *msg, const void *key);
    void dec(void *msg, const void *cip, const void *key);
    void enc2(void *cip, const void *msg, const void *key);

    void enc3(void *cip, const void *msg, const void *key);

    void enc3_cbc(void *cip, const void *msg, size_t n, const void *key);

    void enc3_ctr(void *cip, const void *msg, size_t n, const void *key, const void *nonce);

    void enc3_ctr256(void *cip, const void *msg, size_t n, const void *key, const void *nonce);

#ifdef __AVX512__
    void enc3_ctr512(void *cip, const void *msg, size_t n, const void *key, const void *nonce);
#endif
} // namespace crypto::aes
