#pragma once

#include "crypto.hpp"

namespace crypto::th07
{
    static constexpr size_t DIGEST_SIZE = sizeof(uint64_t);

    void hash(void *digest, const void *msg, size_t sz);

    void hash2(void *digest, const void *message, size_t sz);

    class Hash : public crypto::Hash<uint64_t, DIGEST_SIZE, hash>
    {
    public:
        using Base::Hash;
    };

    class Hash2 : public crypto::Hash<uint64_t, DIGEST_SIZE, hash2>
    {
    public:
        using Base::Hash;
    };
}; // namespace crypto::th07
