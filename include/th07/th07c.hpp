#pragma once

#include "crypto.hpp"

namespace crypto::th07c
{
    static constexpr size_t DIGEST_SIZE = sizeof(uint64_t);
    static constexpr size_t DIGEST_INIT = 0x9a5f28ec46d471d2;

    void hash(void *digest, const void *msg, size_t sz);

    class Hash : public crypto::Hash<uint64_t, DIGEST_SIZE, hash>
    {
    public:
        using Base::Hash;
    };
}; // namespace crypto::th07c
