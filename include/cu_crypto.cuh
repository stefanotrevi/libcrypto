#pragma once

#include "crypto_base.hpp"
#include "cu_default.cuh"
#include "intrinsics.h"
#include <algorithm>
#include <array>
#include <boost/type_traits/function_traits.hpp>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <random>

namespace cu::crypto
{
    // these are like crypto:: stuff, but sizes are 32 bits for performance
    template<typename Msg, typename Key>
    using EncryptionFunction = Msg(Msg msg, Key key, int rounds);
    using HashFunction = void(void *digest, const void *message, uint32_t sz);

    static constexpr uint32_t BITS_PER_CHAR = ::crypto::BITS_PER_CHAR;
    static constexpr uint32_t BITS_PER_CHAR_S = ::crypto::BITS_PER_CHAR_S;

    __device__ __host__ inline constexpr const char *alphabeth()
    {
        if constexpr (BITS_PER_CHAR == 4)
            return "0123456789ABCDEF";
        if constexpr (BITS_PER_CHAR == 5)
            return "abcdefghijklmnopqrstuvwxyz01234";
        if constexpr (BITS_PER_CHAR == 6)
            return "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    }

    template<uint8_t known_len = 0>
    __device__ __host__ inline uint32_t u64_to_str(uint64_t x, char str[16])
    {
#ifdef __CUDA_ARCH__
    #define clzll(x) __clzll(x)
#else
    #define clzll(x) _lzcnt_u64(x)
#endif
        uint32_t len = known_len ? known_len : ((63 + BITS_PER_CHAR) - clzll(x)) / BITS_PER_CHAR;

        if constexpr (BITS_PER_CHAR < 7)
        {
            static constexpr const char *char_map = alphabeth();

            for (uint64_t i = 0, t = x; i < len; ++i, x = t, t /= BITS_PER_CHAR_S)
                str[i] = char_map[t % BITS_PER_CHAR_S];

            str[len - 1] = char_map[x - 1];
        }
        else
        {
            for (uint64_t i = 0, t = x; i < len; ++i, t /= BITS_PER_CHAR_S)
                str[i] = t % BITS_PER_CHAR_S;
        }
        str[len] = '\0';


        return len;
#undef clzll
    }


    inline std::mt19937_64 seeded_rng()
    {
        using rres_t = std::random_device::result_type;

        static constexpr size_t N = std::mt19937_64::state_size *
                                    sizeof(std::mt19937_64::result_type);

        std::random_device source;
        std::array<rres_t, (N - 1) / sizeof(rres_t) + 1> random_data;

        std::generate(random_data.begin(), random_data.end(), std::ref(source));
        std::seed_seq seeds(random_data.begin(), random_data.end());

        return std::mt19937_64{seeds};
    }


    namespace device
    {
        template<typename Msg, typename Key, EncryptionFunction<Msg, Key> enc>
        __global__ void test_enc(Msg msg, Key key, int rounds, Msg *cyp)
        {
            *cyp = enc(msg, key, rounds);
        }

        template<typename Msg, typename Key, EncryptionFunction<Msg, Key> enc>
        __global__ void crack_enc(Msg msg, Msg cyp, int rounds, Key off, Key msk, Key known,
                                  Key *d_key)
        {
            Key key = off + blockIdx.x * blockDim.x + threadIdx.x;

            key = pdep_4(key, msk) | known;
            if (enc(msg, key, rounds) == cyp)
                *d_key = key;
        }


        template<HashFunction hash>
        __global__ void test_hash(void *dig, const void *msg, uint32_t sz)
        {
            hash(dig, msg, sz);
        }

        template<HashFunction hash, size_t dig_sz, uint8_t known_len = 0>
        __global__ void crack_hash(const uint32_t *dig, uint64_t off, uint64_t *msg)
        {

            uint64_t id = off + blockIdx.x * blockDim.x + threadIdx.x;
            uint32_t my_dig[dig_sz / sizeof(uint32_t)];
            bool valid = true;
            char guess[known_len ? known_len : 16];
            uint32_t len = u64_to_str<known_len>(id, guess);

            // having a fixed length improves speed by 10x!
            hash(my_dig, guess, known_len ? known_len : len);

            for (uint32_t i = 0; i < lengthof(my_dig); ++i)
                valid &= my_dig[i] == dig[i];

            if (valid)
                *msg = id;
        }
    } // namespace device

    template<typename Msg, typename Key, EncryptionFunction<Msg, Key> enc>
    Msg test_enc(Msg msg, Key key, int rounds)
    {
        Msg cyp;
        Msg *d_cyp;

        cudaMalloc(&d_cyp, sizeof(*d_cyp));
        device::test_enc<Msg, Key, enc><<<1, 1>>>(msg, key, rounds, d_cyp);
        cudaDeviceSynchronize();
        cudaMemcpy(&cyp, d_cyp, sizeof(cyp), cudaMemcpyDeviceToHost);
        cudaFree(d_cyp);

        return cyp;
    }

    template<typename Msg, typename Key, EncryptionFunction<Msg, Key> enc>
    Key crack_enc(Msg msg, Msg cyp, int rounds, Key lim, Key off, Key msk, Key known, size_t watch)
    {
        using clk = std::chrono::high_resolution_clock;

        static constexpr uint32_t GRDSZ = 1 << 16;
        static constexpr uint32_t BLKSZ = MAX_BLKSZ;

        if (test_enc<Msg, Key, enc>(msg, 0, rounds) == cyp)
            return 0;

        Key key = 0;
        Key *d_key;
        cudaStream_t copy_stream;

        cudaStreamCreate(&copy_stream);
        cudaMalloc(&d_key, sizeof(*d_key));
        cudaMemcpy(d_key, &key, sizeof(key), cudaMemcpyHostToDevice);

        std::cout << std::left;

        auto start = clk::now();
        for (size_t i = 0; off < lim && !key; ++i, off += GRDSZ * BLKSZ)
        {
            device::crack_enc<Msg, Key, enc>
                <<<GRDSZ, BLKSZ>>>(msg, cyp, rounds, off, msk, known, d_key);
            cudaMemcpyAsync(&key, d_key, sizeof(key), cudaMemcpyDeviceToHost, 0);
            if (watch && !(i % watch))
            {
                std::cout << std::setw(20) << off << '\t'
                          << std::chrono::duration_cast<std::chrono::seconds>(clk::now() - start).count()
                          << " sec. \r";
                std::cout.flush();
            }
        }
        if (watch)
            std::cout << '\n';

        std::cout << std::right;
        cudaDeviceSynchronize();
        cudaStreamDestroy(copy_stream);
        cudaFree(d_key);

        return key;
    }

    template<HashFunction hash, size_t dig_sz>
    void test_hash(void *dig, const void *msg, uint32_t sz)
    {
        void *d_dig;
        void *d_msg;

        cudaMalloc(&d_dig, dig_sz);
        cudaMalloc(&d_msg, sz);
        cudaMemcpy(d_msg, msg, sz, cudaMemcpyHostToDevice);
        device::test_hash<hash><<<1, 1>>>(d_dig, d_msg, sz);
        cudaDeviceSynchronize();
        cudaMemcpy(dig, d_dig, dig_sz, cudaMemcpyDeviceToHost);
        cudaFree(d_msg);
        cudaFree(d_dig);
    }

    template<HashFunction hash, size_t dig_sz>
    size_t crack_hash(void *res, const void *dig, uint8_t known_len, uint64_t limit, uint64_t off,
                      bool watch)
    {
        using namespace std::chrono;
        using clk = high_resolution_clock;

        static constexpr uint32_t GRDSZ = 1 << 16;
        static constexpr uint32_t BLKSZ = MAX_BLKSZ;

        uint64_t msg = 0, *d_msg;
        uint32_t *d_dig; // we use uint32_t as it's the fastest one for CUDA
        cudaStream_t copy_stream;

        cudaStreamCreate(&copy_stream);

        cudaMalloc(&d_msg, sizeof(*d_msg));
        cudaMemset(d_msg, 0, sizeof(*d_msg));
        cudaMalloc(&d_dig, dig_sz);
        cudaMemcpy(d_dig, dig, dig_sz, cudaMemcpyHostToDevice);

#define KNOWN_LEN_CASE(len)                                                                        \
    case len: device::crack_hash<hash, dig_sz, len><<<GRDSZ, BLKSZ>>>(d_dig, off, d_msg); break

        auto start = clk::now();
        for (; off < limit && !msg; off += GRDSZ * BLKSZ)
        {
            switch (known_len)
            {
                // optimize for small lengths (precomputing length improces speed by ~4 times)
                KNOWN_LEN_CASE(1);
                KNOWN_LEN_CASE(2);
                KNOWN_LEN_CASE(3);
                KNOWN_LEN_CASE(4);
                KNOWN_LEN_CASE(5);
                KNOWN_LEN_CASE(6);
                KNOWN_LEN_CASE(7);
                KNOWN_LEN_CASE(8);
                KNOWN_LEN_CASE(9);
                KNOWN_LEN_CASE(10);
                KNOWN_LEN_CASE(11);
                KNOWN_LEN_CASE(12);
                KNOWN_LEN_CASE(13);
                KNOWN_LEN_CASE(14);
                KNOWN_LEN_CASE(15);
                KNOWN_LEN_CASE(16);
            default: device::crack_hash<hash, dig_sz><<<GRDSZ, BLKSZ>>>(d_dig, off, d_msg);
            }
            cudaMemcpyAsync(&msg, d_msg, sizeof(msg), cudaMemcpyDeviceToHost, copy_stream);
        }
#undef KNOWN_LEN_CASE
        if (watch)
        {
            double elap = (double)(clk::now() - start).count() / 1'000'000'000;
            std::cout << "Hashrate: " << (msg / elap) << " H/s\n";
        }

        cudaDeviceSynchronize();
        cudaStreamDestroy(copy_stream);
        cudaFree(d_msg);
        cudaFree(d_dig);

        size_t len = u64_to_str(msg, (char *)res);
        if (known_len)
            ++((char *)res)[len - 1];
        ((char *)res)[len] = '\0';

        return len;
    }
} // namespace cu::crypto

#define CU_DEFINE_TESTENC_TYPES(name, kernel, Msg, Key)                                            \
    Msg name(Msg msg, Key key, int rounds)                                                         \
    {                                                                                              \
        return cu::crypto::test_enc<Msg, Key, kernel>(msg, key, rounds);                           \
    }

#define CU_DEFINE_TESTENC(name, kernel)                                                            \
    CU_DEFINE_TESTENC_TYPES(name, kernel, boost::function_traits<decltype(kernel)>::arg1_type,     \
                            boost::function_traits<decltype(kernel)>::arg2_type)


#define CU_DEFINE_CRACKENC_TYPES(name, kernel, Msg, Key)                                           \
    Key name(Msg msg, Msg cyp, int rounds, Key lim, Key off, Key msk, Key known, size_t watch)     \
    {                                                                                              \
        return cu::crypto::crack_enc<Msg, Key, kernel>(msg, cyp, rounds, lim, off, msk, known,     \
                                                       watch);                                     \
    }

#define CU_DEFINE_CRACKENC(name, kernel)                                                           \
    CU_DEFINE_CRACKENC_TYPES(name, kernel, boost::function_traits<decltype(kernel)>::arg1_type,    \
                             boost::function_traits<decltype(kernel)>::arg2_type)


#define CU_DEFINE_TESTHASH(name, kernel, digest_sz)                                                \
    void name(void *dig, const void *msg, uint32_t sz)                                             \
    {                                                                                              \
        return cu::crypto::test_hash<kernel, digest_sz>(dig, msg, sz);                             \
    }


#define CU_DEFINE_CRACKHASH(name, kernel, digest_sz)                                               \
    size_t name(void *res, const void *dig, uint8_t known_len, uint64_t limit, uint64_t off,       \
                bool watch)                                                                        \
    {                                                                                              \
        return cu::crypto::crack_hash<kernel, digest_sz>(res, dig, known_len, limit, off, watch);  \
    }
