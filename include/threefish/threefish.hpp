#pragma once

#include <cstddef>
#include <climits>

namespace crypto::threefish
{
    static constexpr size_t BLOCK_SZ = 256 / CHAR_BIT;
    static constexpr unsigned ROUNDS = 72;

    void enc(void *cyp, const void *msg, const void *key, const void *tweak,
             unsigned rounds = ROUNDS);
} // namespace crypto::threefish
