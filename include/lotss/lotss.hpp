#pragma once

#include <cstdint>
#include <cstddef>


namespace crypto::lotss
{
    size_t compute_sksize(size_t msg_sz);

    size_t compute_pksize(size_t msg_sz);

    size_t compute_sigsize(size_t msg_sz);

    void keygen(void *sk, size_t sk_sz, void *pk, size_t pk_sz);

    void sign(void *sig, const void *sk, const void *msg, size_t msg_sz);

    bool check(const void *sig, const void *pk, const void *msg, size_t msg_sz);

} // namespace crypto::lotss
