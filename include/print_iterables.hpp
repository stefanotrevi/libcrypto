#pragma once

#include <iomanip>
#include <ostream>
#include <ranges>

// Print pairs
template<typename T1, typename T2>
std::ostream &operator<<(std::ostream &os, const std::pair<T1, T2> &pair)
{
    return os << '(' << pair.first << ", " << pair.second << ')';
}

// Print iterables (except std::string, std::string_view and char arrays)
template<std::ranges::range Iter,
         std::enable_if_t<!std::is_same_v<Iter, std::string> &&
                              !std::is_same_v<Iter, std::string_view> &&
                              !std::is_same_v<decltype(*std::begin(Iter{})), char>,
                          bool> = true>
std::ostream &operator<<(std::ostream &os, const Iter &it)
{
    using T = decltype(*std::begin(it));
    using cast_type = std::conditional_t<std::is_convertible_v<T, unsigned char>, int, T>;

    bool fix_width = os.width() == sizeof(*std::begin(it) * 2);


    os << std::setw(0) << '{';
    if (std::begin(it) != std::end(it))
    {
        auto i = std::begin(it);
        auto last = std::end(it);

        for (--last; i != last; ++i)
            if (fix_width)
                os << std::setw(sizeof(*i) * 2) << std::setfill('0') << static_cast<cast_type>(*i)
                   << ", ";
            else
                os << static_cast<cast_type>(*i) << ", ";


        os << static_cast<cast_type>(*i);
    }

    return os << '}';
}

// Print char16_t
inline std::ostream &operator<<(std::ostream &os, const char16_t *str)
{
    while (*str)
        os << (char)*str++;

    return os;
}

#define hexout(x) std::hex << std::setw(sizeof(x) * 2) << std::setfill('0') << (x) << std::dec
#define hexoutit(x) std::hex << std::setw(sizeof(*std::begin(x)) * 2) << (x) << std::dec
