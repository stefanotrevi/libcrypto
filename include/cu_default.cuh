#pragma once

#include <cinttypes>
#include <cstddef>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

extern __device__ unsigned __brev(unsigned x);
extern __device__ unsigned long long __brevll(unsigned long long x);

extern __device__ unsigned __byte_perm(unsigned x, unsigned y, unsigned z);

extern __device__ int __clz(int x);
extern __device__ int __clzll(long long x);

extern __device__ int __ffs(int x);
extern __device__ int __ffsll(long long x);

extern __device__ int __popc(unsigned x);
extern __device__ int __popcll(unsigned long long x);

extern __device__ int atomicExch(int *address, int val);
extern __device__ unsigned atomicExch(unsigned *address, unsigned val);
extern __device__ unsigned long long atomicExch(unsigned long long *address,
                                                unsigned long long val);

extern __device__ int atomicAdd(int *address, int val);
extern __device__ unsigned atomicAdd(unsigned *address, unsigned val);
extern __device__ unsigned long long atomicAdd(unsigned long long *address, unsigned long long val);

extern __device__ void __syncthreads();

namespace cu
{
    static constexpr uint32_t MAX_BLKSZ = 1024U;

    template<typename T, uint32_t sz> __device__ constexpr uint32_t lengthof(const T (&)[sz])
    {
        return sz;
    }

    template<typename T1, typename T2> __device__ constexpr auto max(T1 x, T2 y)
    {
        return x > y ? x : y;
    }

    template<typename T> __device__ constexpr T rotl(T x, uint32_t s)
    {
        return (T)(x << s | x >> ((sizeof(T) * 8) - s));
    }

    template<typename T> __device__ constexpr T rotr(T x, uint32_t s)
    {
        return (T)(x >> s | x << ((sizeof(T) * 8) - s));
    }

    template<typename T> __device__ constexpr T pdep_4(T x, T msk)
    {
        T res = 0;

        for (uint32_t i = 0, j = 0; i < sizeof(T) * 2; ++i)
            if (msk >> i * 4 & 0xF)
            {
                res |= (x >> j * 4 & 0xF) << i * 4;
                ++j;
            }

        return res;
    }

    template<typename T> __device__ constexpr T pdep(T val, T mask)
    {
        T res = 0;

        for (T bb = 1; mask; bb += bb)
        {
            if (val & bb)
                res |= mask & -mask;
            mask &= mask - 1;
        }

        return res;
    }

} // namespace cu
