#pragma once

#include <cinttypes>

namespace crypto::tc02
{
    static constexpr int ROUNDS = 8;

    uint64_t sched(uint64_t k, int rounds = ROUNDS);

    uint64_t enc(uint64_t m, uint64_t k, int rounds = ROUNDS);

    uint64_t dec(uint64_t m, uint64_t k, int rounds = ROUNDS);
} // namespace crypto::tc02
