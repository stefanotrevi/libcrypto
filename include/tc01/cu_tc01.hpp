#pragma once

#include <cstdint>

namespace cu::crypto::tc01
{
    static constexpr int ROUNDS = 20;

    uint64_t test_enc(uint64_t msg, uint64_t key, int rounds = ROUNDS);

    uint64_t crack(uint64_t msg, uint64_t cyp, int rounds = ROUNDS, uint64_t lim = ~0ULL,
                   uint64_t off = 0, uint64_t msk = ~0ULL, uint64_t known = 0, size_t watch = 0);
} // namespace cu::crypto::tc01
