#pragma once

#include <cinttypes>

namespace crypto::tc01
{
    static constexpr int ROUNDS = 20;

    uint64_t enc(uint64_t m, uint64_t k, int rounds = ROUNDS);
}
