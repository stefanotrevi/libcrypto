#pragma once

#include "sha256/sha256.hpp"


namespace crypto::wotss
{
    static constexpr size_t SK_SIZE = sha256::DIGEST_SIZE * sha256::DIGEST_SIZE;
    static constexpr size_t PK_SIZE = SK_SIZE;
    static constexpr size_t SIG_SIZE = SK_SIZE;

    void keygen(void *sk, void *pk);

    void sign(void *sig, const void *sk, const void *msg, size_t msg_sz);

    bool check(const void *sig, const void *pk, const void *msg, size_t msg_sz);
} // namespace crypto::wotss
