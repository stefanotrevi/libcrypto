#pragma once

#include "crypto_base.hpp"
#include <climits>
#include <cstdint>

namespace cu::crypto::sha256
{
    static constexpr size_t DIGEST_SIZE = (256 / CHAR_BIT);

    void test(void *dig, const void *msg, uint32_t sz);

    size_t crack_oneblock(void *res, const void *dig, uint8_t known_len = 0, uint64_t limit = ~0,
                          uint64_t off = 0, bool watch = 0);

    uint64_t crack_id(uint64_t old, uint64_t last = ~0);
} // namespace cu::crypto::sha256
