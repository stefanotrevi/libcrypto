#pragma once

#include "crypto.hpp"
#include <climits>

namespace crypto::sha256
{
    static constexpr size_t DIGEST_SIZE = (256 / CHAR_BIT);
    static constexpr size_t BLOCK_SIZE = (512 / CHAR_BIT);

    void hash(void *digest, const void *message, size_t sz);
    void hash_oneblock(void *digest, const void *message, size_t sz);

    class Hash : public crypto::Hash<uint32_t, DIGEST_SIZE, hash>
    {
    public:
        using Base::Hash;
    };
} // namespace sha256
