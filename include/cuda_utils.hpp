#pragma once

#include "string_utils.hpp"
#include <cuda_runtime.h>
#include <iostream>

namespace cu
{
    void printDeviceProperties(int dev)
    {
        cudaDeviceProp prop;
        cudaGetDeviceProperties(&prop, dev);

        std::cout << "name: " << prop.name << '\n';
        std::cout << "uuid: " << hexdump(prop.uuid.bytes) << '\n';
        std::cout << "luid: " << hexdump(prop.luid) << '\n';
        std::cout << "luidDeviceNodeMask: " << prop.luidDeviceNodeMask << '\n';
        std::cout << "totalGlobalMem: " << prop.totalGlobalMem << '\n';
        std::cout << "sharedMemPerBlock: " << prop.sharedMemPerBlock << '\n';
        std::cout << "regsPerBlock: " << prop.regsPerBlock << '\n';
        std::cout << "warpSize: " << prop.warpSize << '\n';
        std::cout << "memPitch: " << prop.memPitch << '\n';
        std::cout << "maxThreadsPerBlock: " << prop.maxThreadsPerBlock << '\n';
        std::cout << "maxThreadsDim: " << prop.maxThreadsDim << '\n';
        std::cout << "maxGridSize: " << prop.maxGridSize << '\n';
        std::cout << "clockRate: " << prop.clockRate << '\n';
        std::cout << "totalConstMem: " << prop.totalConstMem << '\n';
        std::cout << "major: " << prop.major << '\n';
        std::cout << "minor: " << prop.minor << '\n';
        std::cout << "textureAlignment: " << prop.textureAlignment << '\n';
        std::cout << "texturePitchAlignment: " << prop.texturePitchAlignment << '\n';
        std::cout << "deviceOverlap: " << prop.deviceOverlap << '\n';
        std::cout << "multiProcessorCount: " << prop.multiProcessorCount << '\n';
        std::cout << "kernelExecTimeoutEnabled: " << prop.kernelExecTimeoutEnabled << '\n';
        std::cout << "integrated: " << prop.integrated << '\n';
        std::cout << "canMapHostMemory: " << prop.canMapHostMemory << '\n';
        std::cout << "computeMode: " << prop.computeMode << '\n';
        std::cout << "maxTexture1D: " << prop.maxTexture1D << '\n';
        std::cout << "maxTexture1DMipmap: " << prop.maxTexture1DMipmap << '\n';
        std::cout << "maxTexture1DLinear: " << prop.maxTexture1DLinear << '\n';
        std::cout << "maxTexture2D: " << prop.maxTexture2D << '\n';
        std::cout << "maxTexture2DMipmap: " << prop.maxTexture2DMipmap << '\n';
        std::cout << "maxTexture2DLinear: " << prop.maxTexture2DLinear << '\n';
        std::cout << "maxTexture2DGather: " << prop.maxTexture2DGather << '\n';
        std::cout << "maxTexture3D: " << prop.maxTexture3D << '\n';
        std::cout << "maxTexture3DAlt: " << prop.maxTexture3DAlt << '\n';
        std::cout << "maxTextureCubemap: " << prop.maxTextureCubemap << '\n';
        std::cout << "maxTexture1DLayered: " << prop.maxTexture1DLayered << '\n';
        std::cout << "maxTexture2DLayered: " << prop.maxTexture2DLayered << '\n';
        std::cout << "maxTextureCubemapLayered: " << prop.maxTextureCubemapLayered << '\n';
        std::cout << "maxSurface1D: " << prop.maxSurface1D << '\n';
        std::cout << "maxSurface2D: " << prop.maxSurface2D << '\n';
        std::cout << "maxSurface3D: " << prop.maxSurface3D << '\n';
        std::cout << "maxSurface1DLayered: " << prop.maxSurface1DLayered << '\n';
        std::cout << "maxSurface2DLayered: " << prop.maxSurface2DLayered << '\n';
        std::cout << "maxSurfaceCubemap: " << prop.maxSurfaceCubemap << '\n';
        std::cout << "maxSurfaceCubemapLayered: " << prop.maxSurfaceCubemapLayered << '\n';
        std::cout << "surfaceAlignment: " << prop.surfaceAlignment << '\n';
        std::cout << "concurrentKernels: " << prop.concurrentKernels << '\n';
        std::cout << "ECCEnabled: " << prop.ECCEnabled << '\n';
        std::cout << "pciBusID: " << prop.pciBusID << '\n';
        std::cout << "pciDeviceID: " << prop.pciDeviceID << '\n';
        std::cout << "pciDomainID: " << prop.pciDomainID << '\n';
        std::cout << "tccDriver: " << prop.tccDriver << '\n';
        std::cout << "asyncEngineCount: " << prop.asyncEngineCount << '\n';
        std::cout << "unifiedAddressing: " << prop.unifiedAddressing << '\n';
        std::cout << "memoryClockRate: " << prop.memoryClockRate << '\n';
        std::cout << "memoryBusWidth: " << prop.memoryBusWidth << '\n';
        std::cout << "l2CacheSize: " << prop.l2CacheSize << '\n';
        std::cout << "persistingL2CacheMaxSize: " << prop.persistingL2CacheMaxSize << '\n';
        std::cout << "maxThreadsPerMultiProcessor: " << prop.maxThreadsPerMultiProcessor << '\n';
        std::cout << "streamPrioritiesSupported: " << prop.streamPrioritiesSupported << '\n';
        std::cout << "globalL1CacheSupported: " << prop.globalL1CacheSupported << '\n';
        std::cout << "localL1CacheSupported: " << prop.localL1CacheSupported << '\n';
        std::cout << "sharedMemPerMultiprocessor: " << prop.sharedMemPerMultiprocessor << '\n';
        std::cout << "regsPerMultiprocessor: " << prop.regsPerMultiprocessor << '\n';
        std::cout << "managedMemory: " << prop.managedMemory << '\n';
        std::cout << "isMultiGpuBoard: " << prop.isMultiGpuBoard << '\n';
        std::cout << "multiGpuBoardGroupID: " << prop.multiGpuBoardGroupID << '\n';
        std::cout << "hostNativeAtomicSupported: " << prop.hostNativeAtomicSupported << '\n';
        std::cout << "singleToDoublePrecisionPerfRatio: " << prop.singleToDoublePrecisionPerfRatio
                  << '\n';
        std::cout << "pageableMemoryAccess: " << prop.pageableMemoryAccess << '\n';
        std::cout << "concurrentManagedAccess: " << prop.concurrentManagedAccess << '\n';
        std::cout << "computePreemptionSupported: " << prop.computePreemptionSupported << '\n';
        std::cout << "canUseHostPointerForRegisteredMem: " << prop.canUseHostPointerForRegisteredMem
                  << '\n';
        std::cout << "cooperativeLaunch: " << prop.cooperativeLaunch << '\n';
        std::cout << "cooperativeMultiDeviceLaunch: " << prop.cooperativeMultiDeviceLaunch << '\n';
        std::cout << "sharedMemPerBlockOptin: " << prop.sharedMemPerBlockOptin << '\n';
        std::cout << "pageableMemoryAccessUsesHostPageTables: "
                  << prop.pageableMemoryAccessUsesHostPageTables << '\n';
        std::cout << "directManagedMemAccessFromHost: " << prop.directManagedMemAccessFromHost
                  << '\n';
        std::cout << "maxBlocksPerMultiProcessor: " << prop.maxBlocksPerMultiProcessor << '\n';
        std::cout << "accessPolicyMaxWindowSize: " << prop.accessPolicyMaxWindowSize << '\n';
        std::cout << "reservedSharedMemPerBlock: " << prop.reservedSharedMemPerBlock << '\n';
    }
} // namespace cu