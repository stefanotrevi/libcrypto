#pragma once

#include <cstddef>

namespace crypto
{
    template<typename Msg, typename Key>
    using EncryptionFunction = Msg(Msg msg, Key key, int rounds);
    using HashFunction = void(void *digest, const void *msg, size_t sz);

    static constexpr size_t BITS_PER_CHAR = 5;
    static constexpr size_t BITS_PER_CHAR_S = 1 << BITS_PER_CHAR;
} // namespace crypto
