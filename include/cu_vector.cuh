#pragma once

#include "cu_default.cuh"

#define self (*this)

namespace cu
{
    /* Template class for std::vector-like CUDA arrays. Many functionalities are missing.
    All functions that work on the single elements are device-only, query functions (like size())
    are both device and host, and functions that modify the structure of the vector (like 
    copy-assignment) are host-only. 
    */
    template<typename T> class managed_allocator
    {
    public:
        using value_type = T;
        using size_type = size_t;
        using difference_type = ptrdiff_t;
        using propagate_on_container_move_assignment = std::true_type;
        using is_always_equal = std::true_type;

        constexpr managed_allocator() noexcept {}
        constexpr managed_allocator(const managed_allocator &a [[maybe_unused]]) noexcept {}
        template<typename T1>
        constexpr managed_allocator(const managed_allocator<T1> a [[maybe_unused]]) noexcept
        {}

        T *allocate(size_t n) const
        {
            T *ptr = nullptr;
            cudaMallocManaged(&ptr, n * sizeof(T));
            return ptr;
        }
        void deallocate(T *ptr, size_t n [[maybe_unused]]) const { cudaFree(ptr); }

        allocator &operator=(const managed_allocator &a) = default;
        bool operator==(const managed_allocator &a) const noexcept { return true; }
    };

    template<typename T> class device_allocator
    {
    public:
        using value_type = T;

        T *allocate(size_t n)
        {
            T *ptr = nullptr;
            cudaMalloc(&ptr, n * sizeof(T));
            return ptr;
        }
        void deallocate(T *ptr, size_t n [[maybe_unused]]) { cudaFree(ptr); }
    };

    template<typename T> class host_allocator
    {
    public:
        using value_type = T;

        T *allocate(size_t n)
        {
            T *ptr = nullptr;
            cudaHostAlloc(&ptr, n * sizeof(T), cudaHostAllocPortable);
            return ptr;
        }
        void deallocate(T *ptr, size_t n [[maybe_unused]]) { cudaFreeHost(ptr); }
    };

    template<typename T, typename Alloc = managed_allocator<T>> //
    class vector : public std::vector<T, Alloc>
    {
        using std::vector<T, managed_allocator<T>>::vector;

        int compare(const std::vector<T> &v) const noexcept
        {
            if (self.size() < v.size())
                return -1;
            if (self.size() > v.size())
                return 1;

            return memcmp(self.data(), v.data(), self.size());
        }

    public:
        vector(const std::vector<T> &v) : vector(v.size())
        {
            cudaMemcpy(self.data(), v.data(), self.size() * sizeof(T), cudaMemcpyDefault);
        }

        operator std::vector<T>() const noexcept
        {
            return std::vector<T>(self.begin(), self.end());
        }

        bool operator==(const std::vector<T> &v) const noexcept { return self.compare(v) == 0; }
        bool operator!=(const std::vector<T> &v) const noexcept { return self.compare(v) != 0; }
        bool operator<(const std::vector<T> &v) const noexcept { return self.compare(v) < 0; }
        bool operator<=(const std::vector<T> &v) const noexcept { return self.compare(v) <= 0; }
        bool operator>(const std::vector<T> &v) const noexcept { return self.compare(v) > 0; }
        bool operator>=(const std::vector<T> &v) const noexcept { return self.compare(v) >= 0; }
    };

    template<typename T> std::ostream &operator<<(std::ostream &os, const vector<T> &v)
    {
        os << "[ ";

        for (auto &&x : v)
            if constexpr (std::is_same_v<T, char> || std::is_same_v<T, unsigned char>)
                os << (short)x << ' ';
            else
                os << x << ' ';

        return os << ']';
    }
} // namespace cu

#undef self
