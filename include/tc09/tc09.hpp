#pragma once

#include <cinttypes>

namespace crypto::tc09
{
    static constexpr int ROUNDS = 10;

    uint64_t sbox(uint64_t m);
    uint64_t sbox_inv(uint64_t m);

    uint64_t rows(uint64_t m);
    uint64_t rows_inv(uint64_t m);

    uint64_t columns(uint64_t m);
    uint64_t columns_inv(uint64_t m);

    uint64_t enc(uint64_t m, uint64_t k, int rounds = ROUNDS);
    uint64_t dec(uint64_t m, uint64_t k, int rounds = ROUNDS);

    uint64_t enc2(uint64_t m, uint64_t k, int rounds = ROUNDS);
    uint64_t dec2(uint64_t m, uint64_t k, int rounds = ROUNDS);

    uint64_t pdec(uint64_t m);
} // namespace tc09
