#pragma once

#include <cinttypes>

namespace cu::crypto::tc09
{
    static constexpr int ROUNDS = 10;

    uint64_t test_enc(uint64_t msg, uint64_t key, int rounds = ROUNDS);

    uint64_t crack(uint64_t msg, uint64_t cyp, int rounds = ROUNDS, uint64_t lim = ~0,
                   uint64_t off = 0, uint64_t msk = ~0, uint64_t known = 0, uint64_t watch = 0);

    void compute_freq(uint16_t *d_freq, uint64_t c0, uint64_t c1, uint64_t d_in, uint64_t last,
                      uint64_t msk, uint64_t prev_key);
} // namespace cu::tc09
