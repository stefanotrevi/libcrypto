#include "cu_reverse.cuh"

namespace cu::reverse
{
    static __device__ uint32_t rev_byte(uint32_t x)
    {
        x = (x & 0x55555555) << 1 | (x & ~0x55555555) >> 1;
        x = (x & 0x33333333) << 2 | (x & ~0x33333333) >> 2;
        x = (x & 0x0F0F0F0F) << 4 | (x & ~0x0F0F0F0F) >> 4;

        return x;
    }

    static __global__ void rev_ker(uint8_t *v, size_t sz)
    {
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;

        if (i < sz)
            v[i] = __brev(v[i]) >> 24;
    }

    static __global__ void rev_ker_ub(uint32_t *v)
    {
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;

        v[i] = rev_byte(v[i]);
    }

    void rev(uint8_t *v, size_t sz)
    {
        uint32_t *v32 = (uint32_t *)v;

        sz /= 4;
        if (sz < BLKSZ)
            rev_ker_ub<<<1, sz>>>(v32);
        else if (sz % BLKSZ)
            rev_ker<<<sz / BLKSZ, BLKSZ>>>(v, sz);
        else
            rev_ker_ub<<<sz / BLKSZ, BLKSZ>>>(v32);
        cudaDeviceSynchronize();
    }
} // namespace cu::reverse
