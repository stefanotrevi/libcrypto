#pragma once

#include <cinttypes>

namespace crypto::tc03
{
    static constexpr int ROUNDS = 16;

    uint64_t schedule(uint64_t k, int rounds = ROUNDS);

    uint8_t enc(uint8_t m, uint64_t k, int rounds = ROUNDS);

    uint8_t dec(uint8_t m, uint64_t k, int rounds = ROUNDS);

    uint8_t enc2(uint8_t m, uint64_t k, int rounds = ROUNDS);

    uint8_t dec2(uint8_t m, uint64_t k, int rounds = ROUNDS);
} // namespace tc03
