#pragma once

#include "crypto_base.hpp"
#include "string_utils.hpp"
#include <array>
#include <chrono>
#include <iostream>
#include <omp.h>
#include <random>

namespace crypto
{
    template<typename T, size_t digest_sz, HashFunction hash_foo> class Hash
    {
    public:
        static constexpr size_t DIGEST_SZ = digest_sz;
        static constexpr HashFunction *hash = hash_foo;

    protected:
        using Base = Hash;
        std::array<T, DIGEST_SZ / sizeof(T)> digest{};

    public:
        Hash() = default;

        Hash(const char *msg) : Hash(std::string_view{msg}) {}

        template<typename S> Hash(const S *msg, size_t sz)
        {
            hash(digest.data(), msg, sz * sizeof(S));
        }

        template<size_t sz> Hash(const char (&str)[sz]) : Hash(str) {}

        template<std::ranges::range Range> Hash(const Range &it) :
            Hash(&*std::cbegin(it), std::distance(std::cbegin(it), std::cend(it)))
        {}

        template<typename Iter> Hash(Iter first, Iter last) :
            Hash(&*first, std::distance(first, last))
        {}

        T *data() { return digest.data(); }
        T *data() const { return digest.data(); }

        void insert(const std::string &str)
        {
            uint8_t *dig = (uint8_t *)digest.data();

            digest.fill(0);
            for (size_t i = 0, n = std::min(DIGEST_SZ, str.size() / 2); i < n; ++i)
                dig[i] = ascii_to_digit(str[i * 2]) << 4 | ascii_to_digit(str[i * 2 + 1]);
        }

        T &operator[](size_t i) { return digest[i]; }
        const T &operator[](size_t i) const { return digest[i]; }
        virtual explicit operator std::string() const { return hexdump(digest); }

        friend std::ostream &operator<<(std::ostream &os, const Hash &h)
        {
            return os << (std::string)h;
        }
    };

    template<typename Msg, typename Key> Key crack_enc(EncryptionFunction<Msg, Key> enc, Msg msg,
                                                       Msg cyp, int rounds, Key limit = ~0,
                                                       Key mask = ~0, Key known = 0, Key watch = 0)
    {
        using clk = std::chrono::high_resolution_clock;

        if (enc(msg, 0, rounds) == cyp)
            return 0;

        Key key = 0;

#pragma omp parallel shared(key)
        {
            int th_n = omp_get_num_threads();
            int th_i = omp_get_thread_num();

            auto start = clk::now();
            for (Key k = th_i; k < limit && !key; k += th_n)
            {
                Key my_key = _pdep_u64(k, mask) | known;

                if (enc(msg, my_key, rounds) == cyp)
                    key = my_key;
                if (watch && !th_i && !(k % watch))
                {
                    std::cout << k << ' ' << (clk::now() - start).count() / 1'000'000'000.
                              << "sec. \r";
                    std::cout.flush();
                }
            }
        }

        return key;
    }

    std::mt19937_64 seeded_rng();

    size_t u64_to_str(uint64_t x, char *str);

    void crack_hash(HashFunction hash, size_t digest_sz, void *res, const void *digest,
                    uint64_t limit = ~0, uint64_t off = 0, uint64_t watch = 0);
} // namespace crypto
