Get-ChildItem . -Include "*.c", "*.h", "*.cu", "*.cuh", "*.cpp", "*.hpp" -Recurse -Name | 
ForEach-Object { (Get-Content $_).Count } | 
Measure-Object -Sum
