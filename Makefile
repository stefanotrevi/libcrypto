#### BEGIN PROJECT NAME ####
LIBNAME := crypto
#### END PROJECT NAME ####


#### BEGIN USER OPTIONS ####
# Enables debug mode: 0 (no debug), 1 (debug), 2 (debug with optimization)
DEBUG := 0
# Override default C compiler: 0/1
OVERRIDE_DEFAULT_CC := 0
# Override default C++ compiler: 0/1
OVERRIDE_DEFAULT_CXX := 0
# Choose compiler optimization level: 0, 1, 2, 3, fast
OPT_LEVEL := 3
# Enables forceful inlining, can heavily impact compilation time: 0/1
USE_ALWAYS_INLINE := 1
CXX_STD = 20
C_STD = 20
#### END USER OPTIONS ####

#### BEGIN PROJECT-SPECIFIC USER OPTIONS ####
# Enable micro-benchmarking during tests where available: 0/1
MEASURE_PERFORMANCE := 1
# Enable OpenMP parallelization where available: O/1
MULTICORE := 0
# Enable inline assembly code where available: 0/1
USE_ASM := 0
# Enables CUDA acceleration where available: 0/1
USE_CUDA := 1
# Enable custom implementations where available: 0/1
USE_CUSTOM := 1
# Enables OneAPI, automatically forces to use the Intel DPC++ Compiler
USE_ONEAPI := 1
#### END PROJECT-SPECIFIC USER OPTIONS ####

#### BEGIN TARGETS ####
#--- benchmarks: names must be unique across languages ---#
# C benchmark targets
TARGETS_BENCH_C :=

# CUDA benchmark targets
TARGETS_BENCH_CU :=

# C++ benchmark targets
TARGETS_BENCH_CXX :=

#--- executables: names must be unique across languages ---#
# C executable targets
TARGETS_EXE_CC :=

# CUDA executable targets
TARGETS_EXE_CU :=

# C++ executable targets
TARGETS_EXE_CXX :=

#--- libraries: add a dummy target if otherwise empty ---#
# C library targets
TARGETS_LIB_C :=

# CUDA library targets
TARGETS_LIB_CU :=
TARGETS_LIB_CU += des/des
TARGETS_LIB_CU += klein/klein
TARGETS_LIB_CU += md4/md4
TARGETS_LIB_CU += sha256/sha256
TARGETS_LIB_CU += tc01/tc01
TARGETS_LIB_CU += tc02/tc02
TARGETS_LIB_CU += tc07/tc07
TARGETS_LIB_CU += tc09/tc09

# C++ library targets
TARGETS_LIB_CXX := 
TARGETS_LIB_CXX += aes/aes
TARGETS_LIB_CXX += crypto
TARGETS_LIB_CXX += des/des
TARGETS_LIB_CXX += klein/klein
TARGETS_LIB_CXX += lotss/lotss
TARGETS_LIB_CXX += md4/md4
TARGETS_LIB_CXX += md5/md5
TARGETS_LIB_CXX += ntlm/ntlm
TARGETS_LIB_CXX += rainbow/rainbow
TARGETS_LIB_CXX += rc4/rc4
TARGETS_LIB_CXX += sha256/sha256
TARGETS_LIB_CXX += tc01/tc01
TARGETS_LIB_CXX += tc02/tc02
TARGETS_LIB_CXX += tc03/tc03
TARGETS_LIB_CXX += tc05/tc05
TARGETS_LIB_CXX += tc07/tc07
TARGETS_LIB_CXX += tc07/tc07b
TARGETS_LIB_CXX += tc07/tc07c
TARGETS_LIB_CXX += tc09/tc09
TARGETS_LIB_CXX += th07/th07
TARGETS_LIB_CXX += th07/th07c
TARGETS_LIB_CXX += th09/th09
TARGETS_LIB_CXX += threefish/threefish
TARGETS_LIB_CXX += wotss/wotss

#--- tests ---#
# C test targets
TARGETS_TEST_C :=

# CUDA test targets
TARGETS_TEST_CU :=

# C++ test targets
TARGETS_TEST_CXX :=
TARGETS_TEST_CXX += aes
TARGETS_TEST_CXX += aes_speed
TARGETS_TEST_CXX += des
TARGETS_TEST_CXX += klein
TARGETS_TEST_CXX += lotss
TARGETS_TEST_CXX += md4
TARGETS_TEST_CXX += md5
TARGETS_TEST_CXX += ntlm
TARGETS_TEST_CXX += rc4
TARGETS_TEST_CXX += sha256
TARGETS_TEST_CXX += tc01
TARGETS_TEST_CXX += tc02
TARGETS_TEST_CXX += tc03
TARGETS_TEST_CXX += tc05
TARGETS_TEST_CXX += tc07
TARGETS_TEST_CXX += tc07b
TARGETS_TEST_CXX += tc09
TARGETS_TEST_CXX += th07
TARGETS_TEST_CXX += threefish
TARGETS_TEST_CXX += wotss
#### END TARGETS ####

#### BEGIN DLLs ####
LDFLAGS := 
ifeq ($(OS), Windows_NT)
    LDFLAGS += -link
    LDFLAGS += clang\19\lib\windows\clang_rt.builtins-x86_64.lib
    ifeq ($(USE_CUDA), 1)
        LDFLAGS += cudart.lib
    endif
else
    #LDFLAGS += -lstdc++
    ifeq ($(USE_CUDA), 1)
        LDFLAGS += -lcudart
    endif
endif
#### END DLLs ####

######## 
# What comes next should (ideally) not require changes, unless new project-specific user options, 
# new compilers, etc. are needed
########

#### BEGIN DIRECTORIES SETUP ####
# Directory for bench files
BENCHPATH := bench
# Directory for executables
BINPATH := bin
# Directory for build object files
BUILDPATH := build
# path for C files
CPATH := c
# path for CUDA files
CUPATH := cu
# path for C++ files
CXXPATH := cxx
# Directory for dependencies
DEPPATH := dep
# Directory for header files
INCPATH := include
# Directory for output library
LIBPATH := lib
# Directory for source files
SRCPATH := src
# Directory for test files
TESTPATH := test
#### END DIRECTORIES SETUP ####

#### BEGIN TARGETS GROUPING ####
TARGETS_TEST_C := $(addprefix $(TESTPATH)/$(CPATH)/,$(TARGETS_TEST_C))
TARGETS_TEST_CU := $(addprefix $(TESTPATH)/$(CUPATH)/,$(TARGETS_TEST_CU))
TARGETS_TEST_CXX := $(addprefix $(TESTPATH)/$(CXXPATH)/,$(TARGETS_TEST_CXX))

TARGETS_BENCH_C := $(addprefix $(BENCHPATH)/$(CPATH)/,$(TARGETS_BENCH_C))
TARGETS_BENCH_CU := $(addprefix $(BENCHPATH)/$(CUPATH)/,$(TARGETS_BENCH_CU))
TARGETS_BENCH_CXX := $(addprefix $(BENCHPATH)/$(CXXPATH)/,$(TARGETS_BENCH_CXX))

TARGETS_LIB_C := $(addprefix $(LIBPATH)/$(CPATH)/,$(TARGETS_LIB_C))
TARGETS_LIB_CU := $(addprefix $(LIBPATH)/$(CUPATH)/,$(TARGETS_LIB_CU))
TARGETS_LIB_CXX := $(addprefix $(LIBPATH)/$(CXXPATH)/,$(TARGETS_LIB_CXX))

# Put all targets together
TARGETS_C := $(TARGETS_TEST_C) $(TARGETS_EXE_C) $(TARGETS_BENCH_C) $(TARGETS_LIB_C)
TARGETS_CU := $(TARGETS_TEST_CU) $(TARGETS_EXE_CU) $(TARGETS_BENCH_CU) $(TARGETS_LIB_CU)
TARGETS_CXX := $(TARGETS_TEST_CXX) $(TARGETS_EXE_CXX) $(TARGETS_BENCH_CXX) $(TARGETS_LIB_CXX)

TARGETS_TEST := $(TARGETS_TEST_C) $(TARGETS_TEST_CU) $(TARGETS_TEST_CXX)
TARGETS_EXE := $(TARGETS_EXE_C) $(TARGETS_EXE_CU) $(TARGETS_EXE_CXX)
TARGETS_BENCH := $(TARGETS_BENCH_C) $(TARGETS_BENCH_CU) $(TARGETS_BENCH_CXX)
TARGETS_LIB := $(TARGETS_LIB_C) $(TARGETS_LIB_CU) $(TARGETS_LIB_CXX)

TARGETS := $(TARGETS_C) $(TARGETS_CU) $(TARGETS_CXX) 
#### END TARGETS GROUPING ####


#### BEGIN COMPILER/LINKER SETUP ####
# If there is no default compiler, it's like we want to override it
ifeq ($(CC), )
    OVERRIDE_DEFAULT_CC := 1
endif
ifeq ($(CXX), )
    OVERRIDE_DEFAULT_CXX := 1
endif
ifeq ($(NVCC), )
    NVCC := nvcc
    NVCFLAGS := 
endif

ifeq ($(OVERRIDE_DEFAULT_CC), 1)
    ifeq ($(OS), Windows_NT)
        CC := cl
        CFLAGS :=
    else
        CC := cc
        CFLAGS :=
    endif
endif
ifeq ($(OVERRIDE_DEFAULT_CXX), 1)
    ifeq ($(OS), Windows_NT)
        CXX := cl
        CXXFLAGS :=
    else
        CXX := c++
        CXXFLAGS :=
    endif
endif

# Override flags for debugging
ifneq ($(DEBUG), 0)
    NVCFLAGS := -g
    ifeq ($(CC), cl)
        CFLAGS := -DEBUG -Zi
        CXXFLAGS := -DEBUG -Zi
    else
        CFLAGS := -g
        CXXFLAGS := -g
        ifeq ($(CC), icx)
            CFLAGS := -debug
            ifeq ($(DEBUG), 1)
                CFLAGS += -Rno-debug-disables-optimization
            endif
        endif
        ifeq ($(CXX), icpx)
            ifeq ($(DEBUG), 1)
                CXXFLAGS += -Rno-debug-disables-optimization
            endif
        endif
        ifeq ($(CXX), icx)
            CXXFLAGS := -debug
            ifeq ($(DEBUG), 1)
                CXXFLAGS += -Rno-debug-disables-optimization
            endif
        endif
    endif
endif
ifneq ($(DEBUG), 1)
    CFLAGS += -O$(OPT_LEVEL)
    CXXFLAGS += -O$(OPT_LEVEL)
    NVCFLAGS += -O$(OPT_LEVEL)
endif

# Additional inclusion paths
INCFLAGS := -I$(INCPATH) -I$(DEPPATH)

# Additional compiler flags 
ifeq ($(CC), cl)
    OBJ_OUT_FLAG := -Fo:
    EXE_OUT_FLAG := -Fe:
    DEP_OUT_CFLAG := -MF:
    DEP_OUT_CXXFLAG := -MF:
    CFLAGS += -nologo -arch:AVX2 -std:clatest -Wall -MP $(INCFLAGS)
    CXXFLAGS += -nologo -EHsc -arch:AVX2 -std:c++latest -Wall -MP $(INCFLAGS)
else
    OBJ_OUT_FLAG := -o # notice the extra space!
    EXE_OUT_FLAG := -o # notice the extra space!
    DEP_OUT_CFLAG := -MF # notice the extra space!
    DEP_OUT_CXXFLAG := -MF # notice the extra space!
    EXTRA_CFLAGS := -std=c$(C_STD) -march=native -Wall -MMD -MP $(INCFLAGS)
    EXTRA_CXXFLAGS := -std=c++$(CXX_STD) -march=native -Wall -Wno-bitwise-instead-of-logical -MMD -MP $(INCFLAGS)
    ifeq ($(CC), icx)
        ifeq ($(OS), Windows_NT)
            EXTRA_CFLAGS := -Qstd=c$(C_STD) -march=native -Qiopenmp -nologo -Wall -QMMD -QMP $(INCFLAGS)
            DEP_OUT_CFLAG := -QMF # notice the extra space!
        else
            EXTRA_CFLAGS += -qopenmp
        endif
    else
        EXTRA_CFLAGS += -fopenmp
    endif
    ifeq ($(CXX), icx)
        ifeq ($(OS), Windows_NT)
            EXTRA_CXXFLAGS := -Qstd=c++$(CXX_STD) -march=native -Qiopenmp -nologo -Wall -Wno-bitwise-instead-of-logical -QMMD -QMP $(INCFLAGS)
            DEP_OUT_CXXFLAG := -QMF # notice the extra space!
        else
            EXTRA_CXXFLAGS += -qopenmp
        endif
    else
        ifeq ($(CXX), icpx)
            EXTRA_CXXFLAGS += -qopenmp
        else
            EXTRA_CXXFLAGS += -fopenmp
        endif
    endif
    
    CFLAGS += $(EXTRA_CFLAGS)
    CXXFLAGS += $(EXTRA_CXXFLAGS)
endif

# CUDA compiler flags
NVCFLAGS += -std=c++$(CXX_STD) -arch=compute_89 -code=compute_89 -MD $(INCFLAGS)

ifeq ($(OS), Windows_NT)
    # allow parallel debug compilation
    NVCFLAGS += -Xcompiler -FS
endif

DEP_OUT_NVCFLAG := -MF # notice the extra space!


# Archive command and flags
ifeq ($(OS), Windows_NT)
    AR := lib
    ARFLAGS := -nologo
    LIBOUTFLAGS := /OUT:
    LIBEXT := lib
else
    AR := ar
    ARFLAGS := r 
    LIBEXT := a
endif
#### END COMPILER/LINKER SETUP ####


#### BEGIN MACRO DEFINITIONS SETUP ####
# Enable performance measurament code in test files
ifeq ($(MEASURE_PERFORMANCE), 1)
    CFLAGS += -DMEASURE_PERFORMANCE
    CXXFLAGS += -DMEASURE_PERFORMANCE
endif

# Enable parallel code
ifeq ($(MULTICORE), 1)
    CFLAGS += -DMULTICORE
    CXXFLAGS += -DMULTICORE
endif

# Enable always inline
ifeq ($(USE_ALWAYS_INLINE), 1)
    CFLAGS += -DUSE_ALWAYS_INLINE
    CXXFLAGS += -DUSE_ALWAYS_INLINE
endif

# Enable assembly code
ifeq ($(USE_ASM), 1)
    CFLAGS += -DUSE_ASM
    CXXFLAGS += -DUSE_ASM
endif

# Enable CUDA code
ifeq ($(USE_CUDA), 1)
    CFLAGS += -DUSE_CUDA
    CXXFLAGS += -DUSE_CUDA
endif

# Enable OneAPI code
ifeq ($(USE_ONEAPI), 1)
    CFLAGS += -DUSE_ONEAPI
    CXXFLAGS += -DUSE_ONEAPI
    ifeq ($(OS), Windows_NT)
        CFLAGS += -Qipp=crypto -Qipp-link:static
        CXXFLAGS += -Qipp=crypto -Qipp-link:static
    else
        CFLAGS += -qipp=crypto -ipp-link=static
        CXXFLAGS += -qipp=crypto -ipp-link=static
    endif
endif

# Enable custom implementations
ifeq ($(USE_CUSTOM), 1)
    CFLAGS += -DUSE_CUSTOM
    CXXFLAGS += -DUSE_CUSTOM
endif
#### END MACRO DEFINITIONS SETUP ####

#### BEGIN FILES SETUP ####
# Dependency file extension
DEPEXT := d
# Executable file extenstion
EXEEXT := exe
# Object file extenstion
ifeq ($(CC), cl)
    OBJEXT := obj
else
    OBJEXT := o
endif

# Dependency files (makes correctly recompile sources on header change)
DEP := $(TARGETS:%=$(BUILDPATH)/%.(DEPEXT))

# Library file
LIBFILE := $(LIBPATH)/lib$(LIBNAME).$(LIBEXT)

# Bash/Batch script to run all tests
TEST_COMMAND := $(TARGETS_TEST:%=$$SCRIPTPATH/%;\n)$(TARGETS_TEST:%=$$SCRIPTPATH/%;\n)
# Bash/Batch script to run all benchmarks
BENCH_COMMAND := $(TARGETS_BENCH:%=$$SCRIPTPATH/%;\n)$(TARGETS_BENCH:%=$$SCRIPTPATH/%;\n)
#### END FILES SETUP ####

#### BEGIN COMMAND ALIASING ####
# Make useful commands OS independent
ifeq ($(OS), Windows_NT)
    MKDIR = if not exist $(subst /,\,$1) mkdir $(subst /,\, $1)
    MV = mv $(1) $(2)
    RM = if exist $(1) rmdir /S /Q $(1)
    TOUCH = type nul >> $(1)
else
    MKDIR = mkdir -p $(1)
    MV = mv $(1) $(2)/
    RM = rm -rf $(1)/*
    TOUCH = touch $(1)
endif
#### END COMMAND ALIASING ####

#### BEGIN MAIN RECIPES ####
# Build all targets
all: library executables testsuite benchsuite
	

# Remove all build files
clean:
	$(call RM,$(BUILDPATH))
	$(call RM,$(BINPATH))
	$(call RM,$(LIBPATH))

# Build library
library: $(TARGETS_LIB)
	@$(call MKDIR, $(LIBPATH))
	$(AR) $(ARFLAGS) $(LIBOUTFLAGS)$(LIBFILE) $(foreach target, $(TARGETS_LIB), $(BUILDPATH)/$(target).$(OBJEXT))
# Build executables
executables: $(TARGETS_EXE)
	

# Build tests
testsuite: $(TARGETS_TEST)
	@$(call MKDIR, $(BINPATH)/$(TESTPATH))
	@echo -e '#!/bin/sh\nSCRIPTPATH=$$(dirname "$$(readlink -f "$$0")")\n$(TEST_COMMAND)' > $(BINPATH)/$(TESTPATH)/test.sh

# Build benchmarks
benchsuite: $(TARGETS_BENCH)
	@$(call MKDIR, $(BINPATH)/$(BENCHPATH))
	@echo -e '#!/bin/sh\nSCRIPTPATH=$$(dirname "$$(readlink -f "$$0")")\n$(BENCH_COMMAND)' > $(BINPATH)/$(BENCHPATH)/bench.sh

#### END MAIN RECIPES ####

#### BEGIN SPECIFIC TARGET DEPENDENCIES ####
#### END SPECIFIC TARGET DEPENDENCIES ####

#### BEGIN GENERIC TARGET RECIPES ####
# Build C bench targets
$(TARGETS_BENCH_C): %: $(BUILDPATH)/%.$(OBJEXT) library
	@$(call MKDIR, $(dir $(BINPATH)/$@.$(EXEEXT)))
	$(CC) $(CFLAGS) $< $(EXE_OUT_FLAG)$(BINPATH)/$@.$(EXEEXT) $(LIBFILE) $(LDFLAGS)

# Build CUDA bench targets
$(TARGETS_BENCH_CU): %: $(BUILDPATH)/%.$(OBJEXT) library
	@$(call MKDIR, $(dir $(BINPATH)/$@.$(EXEEXT)))
	$(NVCC) $(NVCFLAGS) $< $(EXE_OUT_FLAG)$(BINPATH)/$@.$(EXEEXT) $(LIBFILE) $(LDFLAGS)

# Build C++ bench targets
$(TARGETS_BENCH_CXX): %: $(BUILDPATH)/%.$(OBJEXT) library
	@$(call MKDIR, $(dir $(BINPATH)/$@.$(EXEEXT)))
	$(CXX) $(CXXFLAGS) $< $(EXE_OUT_FLAG)$(BINPATH)/$@.$(EXEEXT) $(LIBFILE) $(LDFLAGS)

# Build C executable targets
$(TARGETS_EXE_C): %: $(BUILDPATH)/%.$(OBJEXT) library
	@$(call MKDIR, $(dir $(BINPATH)/$@.$(EXEEXT)))
	$(CC) $(CFLAGS) $< $(EXE_OUT_FLAG)$(BINPATH)/$@.$(EXEEXT) $(LIBFILE) $(LDFLAGS)

# Build CUDA executable targets
$(TARGETS_EXE_CU): %: $(BUILDPATH)/%.$(OBJEXT) library
	@$(call MKDIR, $(dir $(BINPATH)/$@.$(EXEEXT)))
	$(NVCC) $(NVCFLAGS) $< $(EXE_OUT_FLAG)$(BINPATH)/$@.$(EXEEXT) $(LIBFILE) $(LDFLAGS)

# Build C++ executable targets
$(TARGETS_EXE_CXX): %: $(BUILDPATH)/%.$(OBJEXT) library
	@$(call MKDIR, $(dir $(BINPATH)/$@.$(EXEEXT)))
	$(CXX) $(CXXFLAGS) $< $(EXE_OUT_FLAG)$(BINPATH)/$@.$(EXEEXT) $(LIBFILE) $(LDFLAGS)

# Library targets dependencies
$(TARGETS_LIB): %: $(BUILDPATH)/%.$(OBJEXT)
	
	

# Build C test targets
$(TARGETS_TEST_C): %: $(BUILDPATH)/%.$(OBJEXT) library
	@$(call MKDIR, $(dir $(BINPATH)/$@.$(EXEEXT)))
	$(CC) $(CFLAGS) $< $(EXE_OUT_FLAG)$(BINPATH)/$@.$(EXEEXT) $(LIBFILE) $(LDFLAGS)

# Build CUDA test targets
$(TARGETS_TEST_CU): %: $(BUILDPATH)/%.$(OBJEXT) library
	@$(call MKDIR, $(dir $(BINPATH)/$@.$(EXEEXT)))
	$(NVCC) $(NVCFLAGS) $< $(EXE_OUT_FLAG)$(BINPATH)/$@.$(EXEEXT) $(LIBFILE) $(LDFLAGS)

# Build C++ test targets
$(TARGETS_TEST_CXX): %: $(BUILDPATH)/%.$(OBJEXT) library
	@$(call MKDIR, $(dir $(BINPATH)/$@.$(EXEEXT)))
	$(CXX) $(CXXFLAGS) $< $(EXE_OUT_FLAG)$(BINPATH)/$@.$(EXEEXT) $(LIBFILE) $(LDFLAGS)

#### END GENERIC TARGET RECIPES ####

#### BEGIN GENERIC SOURCE RECIPES ####
# Compile C benchmark sources
$(BUILDPATH)/$(BENCHPATH)/$(CPATH)/%.$(OBJEXT): $(BENCHPATH)/%.c
	@$(call MKDIR, $(dir $@))
	$(CC) $(CFLAGS) $(DEP_OUT_CFLAG)$(basename $@).$(DEPEXT) -c $< $(OBJ_OUT_FLAG)$@

# Compile CUDA benchmark sources
$(BUILDPATH)/$(BENCHPATH)/$(CUPATH)/%.$(OBJEXT): $(BENCHPATH)/%.cu
	@$(call MKDIR, $(dir $@))
	$(NVCC) $(NVCFLAGS) $(DEP_OUT_NVCFLAG)$(basename $@).$(DEPEXT) -c $< $(OBJ_OUT_FLAG)$@

# Compile C++ benchmark sources
$(BUILDPATH)/$(BENCHPATH)/$(CXXPATH)/%.$(OBJEXT): $(BENCHPATH)/%.cpp
	@$(call MKDIR, $(dir $@))
	$(CXX) $(CXXFLAGS) $(DEP_OUT_CXXFLAG)$(basename $@).$(DEPEXT) -c $< $(OBJ_OUT_FLAG)$@

# Compile C executable/library sources
$(BUILDPATH)/$(LIBPATH)/$(CPATH)/%.$(OBJEXT): $(SRCPATH)/%.c
	@$(call MKDIR, $(dir $@))
	$(CC) $(CFLAGS) $(DEP_OUT_CFLAG)$(basename $@).$(DEPEXT) -c $< $(OBJ_OUT_FLAG)$@

# Compile CUDA executable/library sources
$(BUILDPATH)/$(LIBPATH)/$(CUPATH)/%.$(OBJEXT): $(SRCPATH)/%.cu
	@$(call MKDIR, $(dir $@))
	$(NVCC) $(NVCFLAGS) $(DEP_OUT_NVCFLAG)$(basename $@).$(DEPEXT) -c $< -o $@

# Compile C++ executable/library sources
$(BUILDPATH)/$(LIBPATH)/$(CXXPATH)/%.$(OBJEXT): $(SRCPATH)/%.cpp
	@$(call MKDIR, $(dir $@))
	$(CXX) $(CXXFLAGS) $(DEP_OUT_CXXFLAG)$(basename $@).$(DEPEXT) -c $< $(OBJ_OUT_FLAG)$@

# Compile C test sources
$(BUILDPATH)/$(TESTPATH)/$(CPATH)/%.$(OBJEXT): $(TESTPATH)/%.c
	@$(call MKDIR, $(dir $@))
	$(CC) $(CFLAGS) $(DEP_OUT_CFLAG)$(basename $@).$(DEPEXT) -c $< $(OBJ_OUT_FLAG)$@

# Compile CUDA test sources
$(BUILDPATH)/$(TESTPATH)/$(CUPATH)/%.$(OBJEXT): $(TESTPATH)/%.cu
	@$(call MKDIR, $(dir $@))
	$(NVCC) $(NVCFLAGS) $(DEP_OUT_NVCFLAG)$(basename $@).$(DEPEXT) -c $< $(OBJ_OUT_FLAG)$@

# Compile C++ test sources
$(BUILDPATH)/$(TESTPATH)/$(CXXPATH)/%.$(OBJEXT): $(TESTPATH)/%.cpp
	@$(call MKDIR, $(dir $@))
	$(CXX) $(CXXFLAGS) $(DEP_OUT_CXXFLAG)$(basename $@).$(DEPEXT) -c $< $(OBJ_OUT_FLAG)$@

#### END GENERIC SOURCE RECIPES ####

#### BEGIN DEPENDENCY RECIPIES INCLUSION ####
-include $(DEP)
#### END DEPENDENCY RECIPIES INCLUSION ####
